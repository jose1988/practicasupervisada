﻿$(document).ready(
function () {
    $('#Tarjeta').hide();
    $('#Domicilio').hide();
    $('#Domicilio').css("width", "550");
    $('#selDom').hide();
    $('#selTar').hide();
    $('#FechaEnvio').hide();
    $("label[for='FechaEnvio']").hide();
});

$('#FormaDePago').change(function () {
    var seleccionado = $(this).val();
    if (seleccionado == "Efectivo") {
        $('#Tarjeta').attr('readonly', 'true')
        $('#Tarjeta').hide();
        $('#selTar').hide();
    } else {
        $('#Tarjeta').show();
        $('#selTar').show();
    }
});

$('#idDom').change(function () {
    var seleccionado = $(this).val();
    
});

$('#IsEnvio').change(function () {
    var seleccionado = this.checked;
    if (!seleccionado) {
        $('#Domicilio').attr('readonly', 'true')
        $('#Domicilio').hide();
        $('#selDom').hide();
        $('#FechaEnvio').hide();
        $("label[for='FechaEnvio']").hide();
    } else {
        $('#Domicilio').show();
        $('#selDom').show();
        $('#FechaEnvio').show();
        $("label[for='FechaEnvio']").show();
    }
});

$('#selDom').click(function () {
    //window.open();
    window.open('/Compra/DomicilioPopUp', "PopupWindow", 'scrollbars=1,width=600px,height=800px,top=50,left=100');
});

$('#selTar').click(function () {
    //window.open();
    window.open('/Compra/TarjetaPopUp', "PopupWindow", 'scrollbars=1,width=600px,height=800px,top=50,left=100');
});

$('#IdSector').change(function () {
    var opcion_seleccionada = $('#IdSector option:selected').text();
    var cant = $('#Cantidad').val();
    var importe = 0;
    var s = opcion_seleccionada.split('$');

    var precio = s[1].split(' -');

    // para pasarlos a float tienen que tener .
    precio = parseFloat(precio[0].replace(",", "."));
    var servicio = s[2].split(' -');
    servicio = parseFloat(servicio[0].replace(",", "."));

    if (cant == "" || cant == " " || cant == 0) {
        importe = 0;
    } else {
        importe = parseInt(cant) * (precio + servicio);
    }
    $('#importe').val(importe);
});

$('#area1').click(function () 
{
    var id = 1;
      $.ajax({
            url: "/Compra/ObtenerDatosSector",
            cache: false,
            type: "POST",
            data: { "id":id },
            success: function (data) {
                var cant = $('#Cantidad').val();
                $('#importe').val(parseInt(cant) * (data.Precio + data.Servicio));
            }
        });
});

$('#area2').click(function () {
    var id = 2;
      $.ajax({
            url: "/Compra/ObtenerDatosSector",
            cache: false,
            type: "POST",
            data: { "id":id },
            success: function (data) {
                var cant = $('#Cantidad').val();
                $('#importe').val(parseInt(cant) * (data.Precio + data.Servicio));
            }
        });
});

$('#area3').click(function () {
    var id = 3;
      $.ajax({
            url: "/Compra/ObtenerDatosSector",
            cache: false,
            type: "POST",
            data: { "id":id },
            success: function (data) {
                var cant = $('#Cantidad').val();
                $('#importe').val(parseInt(cant) * (data.Precio + data.Servicio));
            }
        });
});

$('#area4').click(function () {
     var id = 4;
      $.ajax({
            url: "/Compra/ObtenerDatosSector",
            cache: false,
            type: "POST",
            data: { "id":id },
            success: function (data) {
                var cant = $('#Cantidad').val();
                $('#importe').val(parseInt(cant) * (data.Precio + data.Servicio));
            }
        });
});

$('#area5').click(function () {
     var id = 5;
      $.ajax({
            url: "/Compra/ObtenerDatosSector",
            cache: false,
            type: "POST",
            data: { "id":id },
            success: function (data) {
                var cant = $('#Cantidad').val();
                $('#importe').val(parseInt(cant) * (data.Precio + data.Servicio));
            }
        });
     });

     $('#area1').mouseover(function () {
         var id = 1;
         $.ajax({
             url: "/Compra/ObtenerDatosSector",
             cache: false,
             type: "POST",
             data: { "id": id },
             success: function (data) {
                 $('#area1').attr('title', "Precio:" + data.Precio + " Servicio: " + data.Servicio);
             }
         });
     });

     $('#area2').mouseover(function () {
         var id = 2;
         $.ajax({
             url: "/Compra/ObtenerDatosSector",
             cache: false,
             type: "POST",
             data: { "id": id },
             success: function (data) {
                 $('#area2').attr('title', "Precio:" + data.Precio + " Servicio: " + data.Servicio);
             }
         });
     });

     $('#area3').mouseover(function () {
         var id = 3;
         $.ajax({
             url: "/Compra/ObtenerDatosSector",
             cache: false,
             type: "POST",
             data: { "id": id },
             success: function (data) {
                 $('#area3').attr('title', "Precio:" + data.Precio + " Servicio: " + data.Servicio);
             }
         });
     });

     $('#area4').mouseover(function () {
         var id = 4;
         $.ajax({
             url: "/Compra/ObtenerDatosSector",
             cache: false,
             type: "POST",
             data: { "id": id },
             success: function (data) {
                 $('#area4').attr('title', "Precio:" + data.Precio + " Servicio: " + data.Servicio);
             }
         });
     });

     $('#area5').mouseover(function () {
         var id = 5;
         $.ajax({
             url: "/Compra/ObtenerDatosSector",
             cache: false,
             type: "POST",
             data: { "id": id },
             success: function (data) {
                 $('#area5').attr('title', "Precio:" + data.Precio + " Servicio: " + data.Servicio);
             }
         });
     });
