﻿$(document).ready(

function () {
    $("#btnUpload").click(function () {
        
        //para saber si voy al Edit o Create con el Ajax
        var url = "";
        if(document.URL.contains("Create"))
            url = "/Show/PostIm";
        else 
            url = "/Show/PostImEdit";
        var files = $("#inputFile").get(0).files;
        var data = new FormData();
        for (i = 0; i < files.length; i++) {
            data.append("file" + i, files[i]);
        }
        $.ajax({
            type: "POST",
            url: url,
            contentType: false,
            processData: false,
            data: data,
            success: function (result) {
                if (result) {
                    $('#img').attr("src", "../../Content/Images/" + files[0].name);
                }
            }
        });
    })
});