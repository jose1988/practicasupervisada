﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcEntradas.Entity;
using MvcEntradas.Models;

namespace MvcEntradas.Controllers
{ 
    public class ZonaEnvioController : Controller
    {
        private VentaEntradasEntities db = new VentaEntradasEntities();

        //
        // GET: /ZonaEnvio/
        [MyAuthorizeAttribute(Roles = "Administrador")]
        public ViewResult Index()
        {
            return View(db.ZonasEnvios.ToList());
        }

        //
        // GET: /ZonaEnvio/Details/5
        [MyAuthorizeAttribute(Roles = "Administrador")]
        public ViewResult Details(long id)
        {
            ZonasEnvios zonasenvios = db.ZonasEnvios.Single(z => z.IdZona == id);
            return View(zonasenvios);
        }

        //
        // GET: /ZonaEnvio/Create
        [MyAuthorizeAttribute(Roles = "Administrador")]
        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /ZonaEnvio/Create

        [HttpPost]
        [MyAuthorizeAttribute(Roles = "Administrador")]
        public ActionResult Create(ZonasEnvios zonasenvios)
        {
            if (ModelState.IsValid)
            {
                db.ZonasEnvios.AddObject(zonasenvios);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            return View(zonasenvios);
        }
        
        //
        // GET: /ZonaEnvio/Edit/5
        [MyAuthorizeAttribute(Roles = "Administrador")]
        public ActionResult Edit(long id)
        {
            ZonasEnvios zonasenvios = db.ZonasEnvios.Single(z => z.IdZona == id);
            return View(zonasenvios);
        }

        //
        // POST: /ZonaEnvio/Edit/5

        [HttpPost]
        [MyAuthorizeAttribute(Roles = "Administrador")]
        public ActionResult Edit(ZonasEnvios zonasenvios)
        {
            if (ModelState.IsValid)
            {
                db.ZonasEnvios.Attach(zonasenvios);
                db.ObjectStateManager.ChangeObjectState(zonasenvios, EntityState.Modified);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(zonasenvios);
        }

        //
        // GET: /ZonaEnvio/Delete/5
        [MyAuthorizeAttribute(Roles = "Administrador")]
        public ActionResult Delete(long id)
        {
            ZonasEnvios zonasenvios = db.ZonasEnvios.Single(z => z.IdZona == id);
            return View(zonasenvios);
        }

        //
        // POST: /ZonaEnvio/Delete/5

        [HttpPost, ActionName("Delete")]
        [MyAuthorizeAttribute(Roles = "Administrador")]
        public ActionResult DeleteConfirmed(long id)
        {            
            ZonasEnvios zonasenvios = db.ZonasEnvios.Single(z => z.IdZona == id);
            db.ZonasEnvios.DeleteObject(zonasenvios);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}