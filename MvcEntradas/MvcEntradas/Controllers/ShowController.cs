﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcEntradas.Entity;
using MvcEntradas.Models;
using System.IO;
using System.Collections;
using System.Net.Mail;

namespace MvcEntradas.Controllers
{ 
    public class ShowController : Controller
    {
        private VentaEntradasEntities db = new VentaEntradasEntities();

        //
        // GET: /Todos/
        public ViewResult ShowsPasados()
        {
            DateTime fechaDesde = DateTime.Today;
            DateTime fechaHasta = fechaDesde.AddMonths(-3);
            var shows = db.Shows.Include("Establecimientos").Where(s => s.IsAprobado && s.FechaHora < fechaDesde && s.FechaHora >= fechaHasta && s.IsCancelado == false);
            return View(shows.ToList());
        }

        //
        // GET: /Browse/
        public ViewResult Browse()
        {
            string texto = Request.QueryString["txtBuscar"];
            var fecha = new DateTime();
            if (DateTime.TryParse(texto, out fecha))
            {
                var fec = fecha.Date;
                var fecHasta = fec.AddDays(1).AddSeconds(-1);
                var shows = db.Shows.Where(s => s.IsAprobado && s.IsCancelado == false && s.FechaHora >= fec && s.FechaHora <= fecHasta);
                return View(shows.ToList());
            }
            else
            {
                var shows = db.Shows.Where(s => s.IsAprobado && s.IsCancelado == false && s.Artista.Contains(texto) || s.Establecimientos.Nombre.Contains(texto));
                return View(shows.ToList());
            }
        }

        //
        // GET: /Todos/
        public ViewResult Todos()
        {
            // Esto son todos los activos para que vea cualquier usuario
            // los de la fecha de hoy a un mes los cargamos
            // en home debajo del carrusel
            DateTime fechaDesde = DateTime.Today;
            var shows = db.Shows.Include("Establecimientos").Where(s => s.IsAprobado && s.FechaHora >= fechaDesde && s.IsCancelado == false); ;
            return View(shows.ToList());
        }

        //
        // GET: /Show/
         [MyAuthorizeAttribute(Roles = "Administrador,Organizador")]
        public ViewResult Index(string error)
        {
            if (error != null && error != "")
                ViewBag.error = error;
            else
                ViewBag.error = "";
            var shows = db.Shows.Include("Establecimientos").Where(s => s.IsCancelado == false);
             Usuarios us = db.Usuarios.Single(u => u.NombreUsuario == User.Identity.Name);
             ViewBag.IdPerfil = us.IdPerfil;
            // Shows activos. Los pasados se muestran en vista Shows pasados
             if(User.IsInRole("Administrador"))
                shows = db.Shows.Include("Establecimientos").Include("Usuarios").Where(s => s.IsCancelado == false && s.FechaHora >= DateTime.Now);
             else if(User.IsInRole("Organizador"))
                 shows = db.Shows.Include("Establecimientos").Include("Usuarios").Where(s => s.IsCancelado == false && s.FechaHora >= DateTime.Now && s.IdUsuario == us.IdUsuario);
            return View(shows.ToList());
        }

        [HttpPost]
        [MyAuthorizeAttribute(Roles = "Administrador,Organizador")]
         public ViewResult Index(string error, string artista, string est, string fecha, string org)
         {
             if (error != null && error != "")
                 ViewBag.error = error;
             else
                 ViewBag.error = "";

             var shows = db.Shows.Include("Establecimientos").Where(s => s.IsCancelado == false);
             Usuarios us = db.Usuarios.Single(u => u.NombreUsuario == User.Identity.Name);
             ViewBag.IdPerfil = us.IdPerfil;
             // Shows activos. Los pasados se muestran en vista Shows pasados
             if (User.IsInRole("Administrador"))
                 shows = db.Shows.Include("Establecimientos").Include("Usuarios").Where(s => s.IsCancelado == false && s.FechaHora >= DateTime.Now);
             else if (User.IsInRole("Organizador"))
                 shows = db.Shows.Include("Establecimientos").Include("Usuarios").Where(s => s.IsCancelado == false && s.FechaHora >= DateTime.Now && s.IdUsuario == us.IdUsuario);
             if (artista != "")
                 shows = shows.Where(s => s.Artista.ToLower().Contains(artista.ToLower()));
            if(est != "")
                shows = shows.Where(s => s.Establecimientos.Nombre.ToLower().Contains(est.ToLower()));
            if (fecha != "")
            {
                var fe = new DateTime();
                if (DateTime.TryParse(fecha, out fe))
                {
                    var fec = fe.Date;
                    var fecHasta = fec.AddDays(1).AddSeconds(-1);
                    shows = shows.Where(s => s.FechaHora >= fec && s.FechaHora <= fecHasta);
                }
            }

            if (org != null && org != "")
                shows = shows.Where(s => s.Usuarios.NombreLegal.ToLower().Contains(org.ToLower()));
            return View(shows.ToList());
         }

        //
        // GET: /Show/Details/5

        public ViewResult Details(long id)
        {
            Shows shows = db.Shows.Single(s => s.IdShow == id);
            return View(shows);
        }

        //
        // GET: /Show/Create
         [MyAuthorizeAttribute(Roles = "Administrador,Organizador")]
        public ActionResult Create()
        {
            ViewBag.IdEstablecimiento = new SelectList(db.Establecimientos, "IdEstablecimiento", "Nombre");
            //ViewBag.IdOrganizador = new SelectList(db.Organizadores, "IdOrganizador", "NombreLegal");
            return View();
        } 

        //
        // POST: /Show/Create

        [HttpPost]
        [MyAuthorizeAttribute(Roles = "Administrador,Organizador")]
        public ActionResult Create(Shows shows)
        {
            if (ModelState.IsValid)
            {
                if (Session["urlImagen"] != null)
                {
                   
                    shows.Imagen = Session["urlImagen"].ToString();

                    //quiero guardar solo ruta relativa
                    string source = "..\\..\\Content\\Images\\";
                    string[] stringSeparators = new string[] { "\\Images\\" };
                    string[] result = shows.Imagen.Split(stringSeparators, StringSplitOptions.None);
                    string srcRelativa = source + result[1];
                    shows.Imagen = srcRelativa;
                }
                string user = User.Identity.Name;
                Usuarios usuarioBd = db.Usuarios.Single(u => u.NombreUsuario == user);
                shows.IdUsuario = usuarioBd.IdUsuario;
                shows.IsCancelado = false;
                db.Shows.AddObject(shows);
                db.SaveChanges();
                Session["urlImagen"] = null;
                return RedirectToAction("Index");  
            }

            ViewBag.IdEstablecimiento = new SelectList(db.Establecimientos, "IdEstablecimiento", "Nombre", shows.IdEstablecimiento);
            //ViewBag.IdOrganizador = new SelectList(db.Organizadores, "IdOrganizador", "NombreLegal", shows.IdOrganizador);
            return View(shows);
        }
        
        //
        // GET: /Show/Edit/5
         [MyAuthorizeAttribute(Roles = "Administrador,Organizador")]
        public ActionResult Edit(long id)
        {
            Usuarios usuario = db.Usuarios.Where(u => u.NombreUsuario == User.Identity.Name).First();
            Shows shows = db.Shows.Single(s => s.IdShow == id);
            ViewBag.IdEstablecimiento = new SelectList(db.Establecimientos, "IdEstablecimiento", "Nombre", shows.IdEstablecimiento);
            ViewBag.IdPerfil = usuario.IdPerfil;
            //ViewBag.IdOrganizador = new SelectList(db.Organizadores, "IdOrganizador", "NombreLegal", shows.IdOrganizador);
            string source = "../../Content/Images/";
            string[] stringSeparators = new string[] { "\\Images\\" };
            if (shows.Imagen != null)
            {
                string[] result = shows.Imagen.Split(stringSeparators, StringSplitOptions.None);
                string srcCompleta = source + result[1];
                Session["srcRelativaEdit"] = srcCompleta;
            }
            return View(shows);
        }

        //
        // POST: /Show/Edit/5

        [HttpPost]
        [MyAuthorizeAttribute(Roles = "Administrador,Organizador")]
        public ActionResult Edit(Shows shows)
        {
            if (ModelState.IsValid)
            {
                if (Session["urlImagenEdit"] != null)
                {

                    shows.Imagen = Session["urlImagenEdit"].ToString();

                    //quiero guardar solo ruta relativa
                    string source = "..\\..\\Content\\Images\\";
                    string[] stringSeparators = new string[] { "\\Images\\" };
                    string[] result = shows.Imagen.Split(stringSeparators, StringSplitOptions.None);
                    string srcRelativa = source + result[1];
                    shows.Imagen = srcRelativa;
                }

                db.Shows.Attach(shows);
                db.ObjectStateManager.ChangeObjectState(shows, EntityState.Modified);
                db.SaveChanges();
                Session["urlImagenEdit"] = null;
                return RedirectToAction("Index");
            }
            ViewBag.IdEstablecimiento = new SelectList(db.Establecimientos, "IdEstablecimiento", "Nombre", shows.IdEstablecimiento);
            //ViewBag.IdOrganizador = new SelectList(db.Organizadores, "IdOrganizador", "NombreLegal", shows.IdOrganizador);
            return View(shows);
        }

        //
        // GET: /Show/Delete/5
         [MyAuthorizeAttribute(Roles = "Administrador,Organizador")]
        public ActionResult Delete(long id)
        {
            Shows shows = db.Shows.Single(s => s.IdShow == id);
            return View(shows);
        }

        //
        // POST: /Show/Delete/5

        [HttpPost, ActionName("Delete")]
        [MyAuthorizeAttribute(Roles = "Administrador")]
        public ActionResult DeleteConfirmed(long id)
        {            
            Shows shows = db.Shows.Single(s => s.IdShow == id);
            db.Shows.DeleteObject(shows);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        //guarda foto en server
        // hay q ver la forma de que al volver a la pagina no se pierdan
        // los datos que pueda haber ingresado el usuario!!
        //[HttpPost]
        //public ActionResult LoadImage(HttpPostedFileBase img, Shows s)
        //{
        //    var data = new byte[img.ContentLength];
        //    img.InputStream.Read(data, 0, img.ContentLength);
        //    var path = ControllerContext.HttpContext.Server.MapPath("~/Content/Images/");
        //    //creando nombre de archivo para evitar conflictos de repeticion
        //    string fileName = Guid.NewGuid().ToString();
        //    //var filename = Path.Combine(path, Path.GetFileName(img.FileName));
        //    System.IO.File.WriteAllBytes(Path.Combine(path, fileName), data);
        //    ViewBag.ImageUploaded = fileName;
        //    ViewBag.IdEstablecimiento = new SelectList(db.Establecimientos, "IdEstablecimiento", "Nombre");
        //    return PartialView("Create");
        //}

        //guarda foto en server, viene llamado por el js con Ajax. 
        //Ya no se pierden los datos ingresados
        public ActionResult PostIm()
        {
            var request = HttpContext.Request;
            if (request.Files.Count > 0)
            {
                foreach (string file in request.Files)
                {
                    var postedFile = request.Files[file];
                    var filePath = HttpContext.Server.MapPath(string.Format("~/Content/Images/{0}", postedFile.FileName));
                    postedFile.SaveAs(filePath);
                    Session["urlImagen"] = filePath;
                }
                
                ViewBag.IdEstablecimiento = new SelectList(db.Establecimientos, "IdEstablecimiento", "Nombre");
                return PartialView("Create");
            }
            else
                return PartialView("Create");
        }

        public ActionResult PostImEdit()
        {
            var request = HttpContext.Request;
            if (request.Files.Count > 0)
            {
                foreach (string file in request.Files)
                {
                    var postedFile = request.Files[file];
                    var filePath = HttpContext.Server.MapPath(string.Format("~/Content/Images/{0}", postedFile.FileName));
                    postedFile.SaveAs(filePath);
                    Session["urlImagenEdit"] = filePath;
                }

                ViewBag.IdEstablecimiento = new SelectList(db.Establecimientos, "IdEstablecimiento", "Nombre");
                return PartialView("Edit");
            }
            else
                return PartialView("Edit");
        }

        ////carga foto en el img. Ahora la cargo en el JS para un alta
        //Para mostrar las de bd uso este
        public ActionResult Preview(string file)
        {
            var path = ControllerContext.HttpContext.Server.MapPath("~/Content/Images/");
            string[] stringSeparators = new string[] { "\\Images\\" };
            string[] result = file.Split(stringSeparators, StringSplitOptions.None);
            string srcAbsoluta = path + result[1];
            if (System.IO.File.Exists(srcAbsoluta))
            {
                return File(srcAbsoluta, "image/jpeg");
            }
            return new HttpNotFoundResult();
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        [MyAuthorizeAttribute(Roles = "Administrador")]
        public ActionResult Carrusel()
        {
            var shows = db.Shows.Where(s => s.IsAprobado && s.FechaHora >= DateTime.Now && s.IsCancelado == false)
                .ToList();
            return View("Carrusel", shows);
        }

        [HttpPost]
        [MyAuthorizeAttribute(Roles = "Administrador")]
        public ActionResult Carrusel(string Ids)
        {
            Session["msjErrorCarrusel"] = "";
            Ids = Ids.Substring(0,Ids.Length - 1);
            List<long> listIds = Ids.Split(',').ToList().Select(i => Convert.ToInt64(i)).ToList();
            var shows = db.Shows.Where(s => s.IsAprobado && s.FechaHora >= DateTime.Now)
                .ToList();
            if (listIds.Count > 5)
            {
                throw new Exception();
            }

            foreach (var show in shows)
            {
                if (listIds.Contains(show.IdShow))
                    show.IsCarrusel = true;
                else
                    show.IsCarrusel = false;
                
            }
            db.SaveChanges();
            return View("Carrusel", shows);
        }

        [MyAuthorizeAttribute(Roles = "Administrador")]
        public ActionResult CancelarShow(long id)
        {
            Shows show = db.Shows.Single(s => s.IdShow == id);
            string err = "";
            TimeSpan dias = show.FechaHora.Subtract(DateTime.Now);
            if (dias.TotalHours >= 24)
            {
                show.IsCancelado = true;

                db.SaveChanges();
                MailSender ms = new MailSender();
                string b = ms.cuerpoMailCancelacionShow(show);
                var compras = db.Compras.Where(c => c.IdShow == show.IdShow);
                foreach (var co in compras)
                {
                    MailMessage mm = new MailMessage();
                    mm.To.Add(co.Usuarios.Email);
                    mm.Subject = "Cancelación de Show";
                    ms.EnviarCorreo(mm, b);
                }
            }
            else
                err = "No se puede cancelar un show con menos de un dia de anticipacion";

            return RedirectToAction("Index", new { error = err });
        }

    }
}