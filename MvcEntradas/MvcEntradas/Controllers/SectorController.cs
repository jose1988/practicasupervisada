﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcEntradas.Entity;

namespace MvcEntradas.Controllers
{ 
    public class SectorController : Controller
    {
        private VentaEntradasEntities db = new VentaEntradasEntities();

        //
        // GET: /Sector/

        public ViewResult Index()
        {
            return View(db.Sectores.ToList());
        }

        //
        // GET: /Sector/Details/5

        public ViewResult Details(long id)
        {
            Sectores sectores = db.Sectores.Single(s => s.IdSector == id);
            return View(sectores);
        }

        //
        // GET: /Sector/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /Sector/Create

        [HttpPost]
        public ActionResult Create(Sectores sectores)
        {
            if (ModelState.IsValid)
            {
                db.Sectores.AddObject(sectores);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            return View(sectores);
        }
        
        //
        // GET: /Sector/Edit/5
 
        public ActionResult Edit(long id)
        {
            Sectores sectores = db.Sectores.Single(s => s.IdSector == id);
            return View(sectores);
        }

        //
        // POST: /Sector/Edit/5

        [HttpPost]
        public ActionResult Edit(Sectores sectores)
        {
            if (ModelState.IsValid)
            {
                db.Sectores.Attach(sectores);
                db.ObjectStateManager.ChangeObjectState(sectores, EntityState.Modified);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(sectores);
        }

        //
        // GET: /Sector/Delete/5
 
        public ActionResult Delete(long id)
        {
            Sectores sectores = db.Sectores.Single(s => s.IdSector == id);
            return View(sectores);
        }

        //
        // POST: /Sector/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(long id)
        {            
            Sectores sectores = db.Sectores.Single(s => s.IdSector == id);
            db.Sectores.DeleteObject(sectores);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}