﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcEntradas.Entity;
using MvcEntradas.Models;
using System.Net.Mail;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;

namespace MvcEntradas.Controllers
{ 
    public class CompraController : Controller
    {
        private VentaEntradasEntities db = new VentaEntradasEntities();
        private decimal costoServicio = 0;

        //
        // GET: /Compra/

        /*
         Detalles del show
         Imagen del show
         Listado de los precios segun sector
         * 
         * 
         Text de cantidad de entradas
         combo de sector
         combo de forma de pago
         check de envio
         
         text de importe total de compra
         
         */

        //Vista de compra exitosa
        [MyAuthorizeAttribute(Roles = "Comprador")]
        public ViewResult Index()
        {
            // Se compara Identity.Name (es el nombre de usuario) con nuestro Usuarios.NombreUsuario 
            //!! ATENCION: no comparar con el Usuarios.Nombre que no tiene corelacion con lo
            //guardado en al base del Membership
            Compras compra = db.Compras.Where(c => c.Usuarios.NombreUsuario == User.Identity.Name).OrderByDescending(c => c.FechaHora).First();
            
            return View(compra);
        }

        //
        // GET: /Compra/Details/5

        public ViewResult Details(long id)
        {
            int idShow = 1;
            var show = db.Shows.Where(s => s.IdShow == idShow).First();
            Compras compras = db.Compras.Single(c => c.IdCompra == id);
            compras.Shows = show;
            return View(compras);
        }

        //
        // GET: /Compra/ListadoPorUsuario
        [MyAuthorizeAttribute(Roles = "Administrador, Comprador")]
        public ViewResult ListadoPorUsuario(long id)
        {
            var shows = db.Shows.Where(s => s.FechaHora > DateTime.Now);
            var compras = db.Compras.Where(c => c.IdUsuario == id).Include("Sectores").Include("ZonasEnvios").Include("Shows");

            compras = compras.Where(c => c.Shows.FechaHora >= DateTime.Now).OrderByDescending(c => c.FechaHora);
            return View(compras.ToList());
        }

        [HttpPost]
        [MyAuthorizeAttribute(Roles = "Administrador, Comprador")]
        public ViewResult ListadoPorUsuario(string fshow, string estado, string fecha, string pago)
        {
            Usuarios us = db.Usuarios.SingleOrDefault(u => u.NombreUsuario == User.Identity.Name);
            var shows = db.Shows.Where(s => s.FechaHora > DateTime.Now);
            var compras = db.Compras.Where(c => c.IdUsuario == us.IdUsuario).Include("Sectores").Include("ZonasEnvios").Include("Shows");

            compras = compras.Where(c => c.Shows.FechaHora >= DateTime.Now).OrderByDescending(c => c.FechaHora);
            long es = 0;

            if (fshow != "")
            {
                compras = compras.Where(c => c.Shows.Artista.ToLower().Contains(fshow.ToLower()));
            }
            if (estado != "")
            {
                es = Convert.ToInt64(estado);
                compras = compras.Where(c => c.IdEstado == es);
            }
            if (fecha != "")
            {
                var fe = new DateTime();
                if (DateTime.TryParse(fecha, out fe))
                {
                    var fec = fe.Date;
                    var fecHasta = fec.AddDays(1).AddSeconds(-1);
                    compras = compras.Where(c => c.Shows.FechaHora >= fec && c.Shows.FechaHora <= fecHasta);
                }
            }
            if (pago != "")
                compras = compras.Where(c => c.FormaDePago.ToLower() == pago.ToLower());
            
            return View(compras.ToList());
        }

        [MyAuthorizeAttribute(Roles = "Administrador, Comprador")]
        public ViewResult MisComprasAnteriores(long id)
        {
            var compras = db.Compras.Where(c => c.IdUsuario == id).Include("Sectores").Include("ZonasEnvios").Include("Shows");
            compras = compras.Where(c => c.Shows.FechaHora < DateTime.Now);
            return View(compras.ToList());
        }

        [HttpPost]
        [MyAuthorizeAttribute(Roles = "Administrador, Comprador")]
        public ViewResult MisComprasAnteriores(string fshow, string estado, string fecha, string pago)
        {
            Usuarios us = db.Usuarios.SingleOrDefault(u => u.NombreUsuario == User.Identity.Name);
            var compras = db.Compras.Where(c => c.IdUsuario == us.IdUsuario).Include("Sectores").Include("ZonasEnvios").Include("Shows");
            compras = compras.Where(c => c.Shows.FechaHora < DateTime.Now);

            long es = 0;

            if (fshow != "")
            {
                compras = compras.Where(c => c.Shows.Artista.ToLower().Contains(fshow.ToLower()));
            }
            if (estado != "")
            {
                es = Convert.ToInt64(estado);
                compras = compras.Where(c => c.IdEstado == es);
            }
            if (fecha != "")
            {
                var fe = new DateTime();
                if (DateTime.TryParse(fecha, out fe))
                {
                    var fec = fe.Date;
                    var fecHasta = fec.AddDays(1).AddSeconds(-1);
                    compras = compras.Where(c => c.Shows.FechaHora >= fec && c.Shows.FechaHora <= fecHasta);
                }
            }
            if (pago != "")
                compras = compras.Where(c => c.FormaDePago.ToLower() == pago.ToLower());

            return View(compras.ToList());
        }

        //
        // GET: /Compra/Create
       
        public ActionResult Create(string error)
        {

            Session["SectorId"] = null;
            if (User.Identity.IsAuthenticated)
            {
                if (error != null)
                    Session["msjErrorCompra"] = error;
                else
                    Session["msjErrorCompra"] = "";
                int idShow = Convert.ToInt32(Request.QueryString["IdShow"]);
                
                var show = db.Shows.Where(s => s.IdShow == idShow).First();
                if (LogicaShow.validarDisponibilidad(show))
                {

                    Usuarios us = db.Usuarios.Single(u => u.NombreUsuario == User.Identity.Name);
                    // Ojo no es la ultima trajeta guardada la que necesitamos sino la ultima q uso para comprar
                    Tarjetas tarjeta = db.Tarjetas.Where(t => t.IdUsuario == us.IdUsuario).OrderByDescending(t => t.FechaAlta).FirstOrDefault();
                    Domicilio dom = db.Domicilio.Where(d => d.IdUsuario == us.IdUsuario).OrderByDescending(d => d.FechaAlta).FirstOrDefault();

                    Compras compra = new Compras();
                    compra.Shows = show;
                    compra.FechaHora = DateTime.Now;
                    if (tarjeta != null)
                    {
                        compra.Tarjeta = tarjeta.NroTarjeta;
                        ViewBag.Tarjeta = tarjeta.NroTarjeta;
                        Session["tarjeta"] = tarjeta.NroTarjeta;
                    }
                    else
                        ViewBag.Tarjeta = "";
                    if (dom != null)
                    {
                        ViewBag.idDom = dom.IdDomicilio;
                        Session["idDom"] = dom.IdDomicilio;
                        compra.IdDomicilio = dom.IdDomicilio;
                        ViewBag.Domicilio = "Calle: " + dom.Calle + " Nro: " + dom.Numero.ToString() + " Loc: " + dom.Localidad + " Zona Envio: " + dom.ZonasEnvios.Nombre + " Precio Envio $" + dom.ZonasEnvios.Precio;
                    }
                    else
                        ViewBag.Domicilio = "";
                    this.GenerarImagen(show.Establecimientos.IdEstablecimiento);
                    // Armo descripcion - precio para mostar como texto de combo
                    var sectores = db.Sectores.Where(s => s.Establecimientos.IdEstablecimiento == show.Establecimientos.IdEstablecimiento);

                    //Cambio para no modificar el sector en bd
                    List<SectorDetallado> secc = new List<SectorDetallado>();
                    foreach (var sec in sectores)
                    {
                        if (sec.Descripcion.ToString().ToLower() == "campo")
                            costoServicio = Convert.ToDecimal(0.10) * sec.Precio;
                        else if (sec.Descripcion.ToString().ToLower() == "platea")
                            costoServicio = Convert.ToDecimal(0.10) * sec.Precio;
                        else if (sec.Descripcion.ToString().ToLower() == "campo vip")
                            costoServicio = Convert.ToDecimal(0.25) * sec.Precio;
                        else
                            costoServicio = Convert.ToDecimal(0.15) * sec.Precio;
                        costoServicio = Decimal.Round(costoServicio, 2);
                        //Cambio para no modificar el sector en bd
                        var secDet = new SectorDetallado
                            {
                                IdSector = sec.IdSector,
                                Descripcion =  sec.Descripcion + " - $" + sec.Precio + " - Costo Servicio $" + costoServicio + " -"
                                
                            };

                        secc.Add(secDet);
                        //sec.Descripcion = sec.Descripcion + " - $" + sec.Precio + " - Costo Servicio $" + costoServicio + " -";
                    }
                    ViewBag.IdSector = new SelectList(secc, "IdSector", "Descripcion");

                    ViewBag.IdZonaEnvio = new SelectList(db.ZonasEnvios, "IdZona", "Nombre");

                    ViewBag.FormaDePago = new SelectList(db.FormasDePago, "Nombre", "Nombre");
                    
                    return View(compra);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }else
                return RedirectToAction("LogOn", "Account");
        } 

        //
        // POST: /Compra/Create

        [HttpPost]
        [MyAuthorizeAttribute(Roles = "Comprador")]
        public ActionResult Create(Compras compras)
        {
            string msjErrorCompra = "";
            var id = Request.Form["idDom"];
            var nro = Request.Form["idTar"];
            if (id != null && id != "")
                compras.IdDomicilio = Convert.ToInt64(id);
            else
                compras.IdDomicilio = Convert.ToInt64(Session["idDom"]);
            if (nro != null && nro != "")
                compras.Tarjeta = nro;
            else if (Session["Tarjeta"] != null)
                compras.Tarjeta = Session["Tarjeta"].ToString();
            //if (ModelState.IsValid)
            //{
           
                Usuarios us = db.Usuarios.Single(u => u.NombreUsuario == User.Identity.Name);
                if (us.IsActivo)
                {
                    Shows show = db.Shows.SingleOrDefault(s => s.IdShow == compras.IdShow);
                    compras.Shows = show;
                    var sectores = db.Sectores.Where(s => s.Establecimientos.IdEstablecimiento == show.IdEstablecimiento);
                    if(compras.Cantidad == 0 || compras.FormaDePago == String.Empty || compras.Cantidad > 6
                        || compras.IdSector == 0)
                    {
                        //Session["msjErrorCompra"] = "";
                        msjErrorCompra = "";
                         if(compras.Cantidad == 0)
                             msjErrorCompra += "Debe elegir cantidad mayor a 0.\n";
                        if (compras.FormaDePago == null)
                            msjErrorCompra += " Debe seleccionar forma de pago.\n";
                        if(compras.Cantidad > 6)
                            msjErrorCompra += " La compra maxima son 6 entradas.\n";
                        if (compras.IdSector == 0)
                        {
                            if (Session["SectorId"] != null)
                            {
                                compras.IdSector = Convert.ToInt64(Session["SectorId"]);
                            }
                            else
                                msjErrorCompra += " Debe seleccionar un sector.\n";
                        }

                        //Cambio para no modificar el sector en bd
                        List<SectorDetallado> secc = new List<SectorDetallado>();
                        foreach (var se in sectores)
                        {
                            if (se.Descripcion.ToString().ToLower() == "campo")
                                costoServicio = Convert.ToDecimal(0.10) * se.Precio;
                            else if (se.Descripcion.ToString().ToLower() == "platea")
                                costoServicio = Convert.ToDecimal(0.10) * se.Precio;
                            else if (se.Descripcion.ToString().ToLower() == "campo vip")
                                costoServicio = Convert.ToDecimal(0.25) * se.Precio;
                            else
                                costoServicio = Convert.ToDecimal(0.15) * se.Precio;

                            //Cambio para no modificar el sector en bd
                            var secDet = new SectorDetallado
                            {
                                IdSector = se.IdSector,
                                Descripcion = se.Descripcion + " - $" + se.Precio + " - Costo Servicio $" + costoServicio + " -"

                            };

                            secc.Add(secDet);
                            //se.Descripcion = se.Descripcion + " - $" + se.Precio + " - Costo Servicio $" + costoServicio + " -";
                        }

                        
                        if (msjErrorCompra != "")
                        {
                            ViewBag.IdSector = new SelectList(secc, "IdSector", "Descripcion");
                            ViewBag.IdZonaEnvio = new SelectList(db.ZonasEnvios, "IdZona", "Nombre");
                            ViewBag.FormaDePago = new SelectList(db.FormasDePago, "Nombre", "Nombre");
                            return RedirectToAction("Create", new { IdShow = show.IdShow, error = msjErrorCompra });
                        }

                    }
                    compras.FechaHora = DateTime.Now;
                    compras.IdUsuario = us.IdUsuario;
                    compras.CantidadEnvios = 0;
                    //Session["msjErrorCompra"] = "";
                    Sectores sec = db.Sectores.SingleOrDefault(s => s.IdSector == compras.IdSector);
                    int disp = LogicaShow.validarDisponibilidadSector(compras.IdShow, sec, compras.Cantidad);
                    if( disp == 1)
                    {
                        if (sec.Descripcion.ToString().ToLower() == "campo")
                            costoServicio = Convert.ToDecimal(0.10) * sec.Precio;
                        else if (sec.Descripcion.ToString().ToLower() == "platea")
                            costoServicio = Convert.ToDecimal(0.10) * sec.Precio;
                        else if (sec.Descripcion.ToString().ToLower() == "campo vip")
                            costoServicio = Convert.ToDecimal(0.25) * sec.Precio;
                        else
                            costoServicio = Convert.ToDecimal(0.15) * sec.Precio;
                        //sec.Descripcion = sec.Descripcion + " - $" + sec.Precio + " - Costo Servicio $" + costoServicio + " -";
                        compras.Importe = compras.Cantidad * (sec.Precio + costoServicio);
                        // Guardo estado de la compra
                        if (compras.FormaDePago.ToLower() == "efectivo")
                            compras.IdEstado = (int)Estados.Reservada;
                        else
                        {
                            //guardo la fecha en la tarjeta usada, para saber ultima tarjeta usada
                            Tarjetas tar = db.Tarjetas.Where(t => t.NroTarjeta == compras.Tarjeta).SingleOrDefault();
                            tar.FechaAlta = DateTime.Now;
                            //db.Tarjetas.Attach(tar);
                            db.SaveChanges();
                            compras.IdEstado = (int)Estados.Vendida;
                        }

                        if (compras.IsEnvio)
                        {
                            //guardo la fecha en el domicilio usado para saber ultimo domicilio usado
                            Domicilio dom = db.Domicilio.Where(d => d.IdDomicilio == compras.IdDomicilio).SingleOrDefault();
                            dom.FechaAlta = DateTime.Now;
                            //db.Domicilio.Attach(dom);
                            db.SaveChanges();
                            compras.IdZonaEnvio = dom.IdZonaEnvio;
                            if(compras.FormaDePago.ToLower() == "efectivo")
                                compras.IdEstado = (int)Estados.Reservada;
                            else
                            compras.IdEstado = (int)Estados.VendidaPendienteDeEntrega;
                        }

                        //db.Compras.AddObject(compras);
                        db.SaveChanges();
                        try
                        {
                            MailSender ms = new MailSender();
                            string b = ms.cuerpoMailCompraExitosa(compras);
                            MailMessage mm = new MailMessage();
                            mm.To.Add(compras.Usuarios.Email);
                            mm.Subject = "Detalles de su Compra";
                            ms.EnviarCorreo(mm, b);
                        }
                        catch (Exception ex)
                        {
                            return RedirectToAction("Index");
                        }
                        return RedirectToAction("Index");
                    }
                    else if(disp == 0)
                        return RedirectToAction("Agotado");
                    else
                        return RedirectToAction("SectorAgotado", new { dispo = disp, idShow = compras.IdShow});
                }
                else
                    return RedirectToAction("MsgInactivo");
            //}

            //ViewBag.IdSector = new SelectList(db.Sectores, "IdSector", "Descripcion");
            //ViewBag.IdZonaEnvio = new SelectList(db.ZonasEnvios, "IdZona", "Nombre");
            //ViewBag.FormaDePago = new SelectList(db.FormasDePago, "Nombre", "Nombre");
            //var show = db.Shows.Where(s => s.IdShow == compras.IdShow).First();
            //compras.Shows = show;
            //return RedirectToAction("Create", new { IdShow = show.IdShow });
        }
        
        //
        // GET: /Compra/Edit/5
        [MyAuthorizeAttribute(Roles = "Administrador,Comprador")]
        public ActionResult Edit(long id)
        {
            Compras compras = db.Compras.Single(c => c.IdCompra == id);
            // Agregar cuando se ponga el IdSector en tabla Compras
            ViewBag.IdSector = new SelectList(db.Sectores, "IdSector", "Descripcion");
            ViewBag.IdZonaEnvio = new SelectList(db.ZonasEnvios, "IdZona", "Nombre");
            ViewBag.FormaDePago = new SelectList(db.FormasDePago, "Nombre", "Nombre");
            return View(compras);
        }

        //
        // POST: /Compra/Edit/5
        [HttpPost]
        [MyAuthorizeAttribute(Roles = "Administrador,Comprador")]
        public ActionResult Edit(Compras compras)
        {
            if (ModelState.IsValid)
            {
                db.Compras.Attach(compras);
                db.ObjectStateManager.ChangeObjectState(compras, EntityState.Modified);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            // Agregar cuando se ponga el IdSector en tabla Compras
            ViewBag.IdSector = new SelectList(db.Sectores, "IdSector", "Descripcion");
            ViewBag.IdZonaEnvio = new SelectList(db.ZonasEnvios, "IdZona", "Nombre");
            ViewBag.FormaDePago = new SelectList(db.FormasDePago, "Nombre", "Nombre");
            return View(compras);
        }

        //
        // GET: /Compra/Delete/5
         [MyAuthorizeAttribute(Roles = "Administrador,Comprador")]
        public ActionResult Delete(long id)
        {
            Compras compras = db.Compras.Single(c => c.IdCompra == id);
            return View(compras);
        }

        //
        // POST: /Compra/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(long id)
        {            
            Compras compras = db.Compras.Single(c => c.IdCompra == id);
            db.Compras.DeleteObject(compras);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult CancelarCompra(long id)
        {
            Compras compras = db.Compras.Single(c => c.IdCompra == id);
            Usuarios usuario = db.Usuarios.Where(u => u.IdUsuario == compras.IdUsuario).First();

            if (usuario.IsActivo)
            {
                compras.IdEstado = (int)Estados.Cancelada;

                usuario.CantidadCancelaciones = Convert.ToInt32(usuario.CantidadCancelaciones) + 1;

                //si llego a 2 inactivo el usuario
                if (usuario.CantidadCancelaciones == 2)
                    usuario.IsActivo = false;
                db.SaveChanges();
            }
            else
                return RedirectToAction("MsgInactivo");

            return RedirectToAction("ListadoPorUsuario", "Compra", new { id = usuario.IdUsuario });
        }

        [MyAuthorizeAttribute(Roles = "Comprador")]
        public ActionResult CambiarFechaEnvio(long id)
        {
            Compras compras = db.Compras.Single(c => c.IdCompra == id);
            Usuarios usuario = db.Usuarios.Where(u => u.IdUsuario == compras.IdUsuario).First();

            if (usuario.IsActivo)
            {
                    ViewBag.precio = (compras.ZonasEnvios.Precio + compras.ZonasEnvios.Precio).ToString();
                    ViewBag.formaPago = compras.FormaDePago.ToLower();
                    Session["precio"] = ViewBag.precio;
                    Session["pago"] = ViewBag.formaPago;
                    return View(compras);
            }
            else
                return RedirectToAction("MsgInactivo");

        }

        [HttpPost]
        [MyAuthorizeAttribute(Roles = "Comprador")]
        public ActionResult CambiarFechaEnvio(Compras compra, string fecha ="")
        {
            if (compra.CambioFechaEnvio != true)
            {
                Usuarios usuario = db.Usuarios.Single(u => u.NombreUsuario == User.Identity.Name);
                compra.CambioFechaEnvio = true;
                db.Compras.Attach(compra);
                db.ObjectStateManager.ChangeObjectState(compra, EntityState.Modified);
                db.SaveChanges();
                return RedirectToAction("ListadoPorUsuario", "Compra", new { id = usuario.IdUsuario });
            }
            else
            {
                Session["msjErrorEnvio"] = "No puede volver a cambiar la fecha de envio, se admite solo un cambio";
                ViewBag.precio = Session["precio"];
                ViewBag.formaPago = Session["pago"];
                return View(compra);
            }

            //ZonasEnvios z = db.ZonasEnvios.Single(zo => zo.IdZona == compra.IdZonaEnvio);
            //ViewBag.precio = (z.Precio + z.Precio).ToString();
            //ViewBag.formaPago = compra.FormaDePago.ToLower();
            //return View(compra);
        }


        public ActionResult DomicilioPopUp()
        {
            Usuarios us = db.Usuarios.Single(u => u.NombreUsuario == User.Identity.Name);
            var dom = db.Domicilio.Where(d => d.IdUsuario == us.IdUsuario).OrderByDescending(d => d.FechaAlta).Include("ZonasEnvios");
            //ViewBag.ListaDom = dom.ToList();
            foreach (var d in dom)
                d.Calle = "Calle: " + d.Calle + " Nro:" + d.Numero + " Loc: " + d.Localidad + " Zona Envio: " + d.ZonasEnvios.Nombre + " Precio Envio $" + d.ZonasEnvios.Precio;
            ViewBag.ListaDom = new SelectList(dom, "IdDomicilio", "Calle");
            var zonas = db.ZonasEnvios;
            foreach (var z in zonas)
                z.Nombre = z.Nombre + " Precio $" + z.Precio;
            ViewBag.IdZonaEnvio = new SelectList(zonas, "IdZona", "Nombre");
            return View();
        }

        [HttpPost]
        public ActionResult DomicilioPopUp(Domicilio domicilio)
        {
            Usuarios us = db.Usuarios.Single(u => u.NombreUsuario == User.Identity.Name);
            if (ModelState.IsValid)
            {
                domicilio.IdUsuario = us.IdUsuario;
                
                db.Domicilio.AddObject(domicilio);
                db.SaveChanges();
            }
            //return RedirectToAction("");
            
            var dom = db.Domicilio.Where(d => d.IdUsuario == us.IdUsuario).OrderByDescending(d => d.FechaAlta);
            var zonas = db.ZonasEnvios;
            foreach (var z in zonas)
                z.Nombre = z.Nombre + " Precio $" + z.Precio;
            ViewBag.IdZonaEnvio = new SelectList(zonas, "IdZona", "Nombre");
            foreach (var d in dom)
                d.Calle = "Calle: " + d.Calle + " Nro: " + d.Numero + " Loc: " + d.Localidad + " Zona Envio: " + d.ZonasEnvios.Nombre + " Precio Envio $" + d.ZonasEnvios.Precio;
            ViewBag.ListaDom = new SelectList(dom, "IdDomicilio", "Calle");
            return View();
        }

        public ActionResult TarjetaPopUp()
        {
            Usuarios us = db.Usuarios.Single(u => u.NombreUsuario == User.Identity.Name);
            var tar = db.Tarjetas .Where(d => d.IdUsuario == us.IdUsuario).OrderByDescending(d => d.FechaAlta);
            ViewBag.ListaTar = new SelectList(tar, "NroTarjeta", "NroTarjeta");
            return View();
        }

        [HttpPost]
        public ActionResult TarjetaPopUp(Tarjetas tarjeta)
        {
            Usuarios us = db.Usuarios.Single(u => u.NombreUsuario == User.Identity.Name);
            if (ModelState.IsValid)
            {
                tarjeta.IdUsuario = us.IdUsuario;

                db.Tarjetas.AddObject(tarjeta);
                db.SaveChanges();
            }
            //return RedirectToAction("");

            var tar = db.Tarjetas.Where(d => d.IdUsuario == us.IdUsuario).OrderByDescending(d => d.FechaAlta);
            ViewBag.ListaTar = new SelectList(tar, "NroTarjeta", "NroTarjeta");
            return View();
        }


        public ActionResult MsgInactivo()
        {
            return View();
        }

        public ActionResult Agotado()
        {
            return View();
        }

        public ActionResult SectorAgotado(int dispo, long idShow)
        {
            ViewBag.disponible = dispo.ToString();
            ViewBag.id = idShow.ToString();
            return View();
        }

        [MyAuthorizeAttribute(Roles = "Administrador")]
        public ActionResult GestionEnvioRetiro(long idShow = 0)
        {
            var shows = db.Shows.Where(s => s.FechaHora > DateTime.Now);
            ViewBag.shows = new SelectList(shows, "IdShow", "Artista");
            var compras = db.Compras.Where(c => c.IdEstado != (int)Estados.Cancelada && c.IdEstado != (int)Estados.Vendida).Include("Sectores").Include("ZonasEnvios").Include("Shows");
            compras = compras.Where(c => c.Shows.FechaHora >= DateTime.Now && c.Shows.IsCancelado == false);
            if(idShow != 0)
                compras = compras.Where(c => c.IdShow == idShow);
            return View(compras.ToList());
        }

        [HttpPost]
        [MyAuthorizeAttribute(Roles = "Administrador")]
        public ActionResult GestionEnvioRetiro(string idShow, string fecha, string ape, string dni)
        {
            var shows = db.Shows.Where(s => s.FechaHora > DateTime.Now);
            ViewBag.shows = new SelectList(shows, "IdShow", "Artista");
            var compras = db.Compras.Where(c => c.IdEstado != (int)Estados.Cancelada && c.IdEstado != (int)Estados.Vendida).Include("Sectores").Include("ZonasEnvios").Include("Shows");
            compras = compras.Where(c => c.Shows.FechaHora >= DateTime.Now && c.Shows.IsCancelado == false);
            if (idShow != "")
            {
                long id2 = Convert.ToInt64(idShow);
                compras = compras.Where(c => c.IdShow == id2);
            }
            if (fecha != "")
            {
                var fe = new DateTime();
                if (DateTime.TryParse(fecha, out fe))
                {
                    var fec = fe.Date;
                    var fecHasta = fec.AddDays(1).AddSeconds(-1);
                    compras = compras.Where(c => c.FechaHora >= fec && c.FechaHora <= fecHasta);
                }
            }
            if (ape != "")
                compras = compras.Where(c => c.Usuarios.Apellido.ToLower().Contains(ape.ToLower()));
            if (dni != "")
                compras = compras.Where(c => c.Usuarios.DNI == dni);
            return View(compras.ToList());
        }

        public ActionResult SumarEnvio(long id)
        {
            Compras compras = db.Compras.Single(c => c.IdCompra == id);
            Usuarios us = db.Usuarios.Single(u => u.IdUsuario == compras.IdUsuario);
            
            compras.CantidadEnvios++;
            if (compras.CantidadEnvios < 2)
            {
                MailSender ms = new MailSender();
                string b = ms.cuerpoMailEnvio(compras);
                MailMessage mm = new MailMessage();
                mm.To.Add(compras.Usuarios.Email);
                mm.Subject = "Envio de Compra";
                ms.EnviarCorreo(mm, b);
            }
            //llego a los 2 envios permitidos
            if (compras.CantidadEnvios == 2 && compras.FormaDePago.ToLower() == "efectivo")
            {
                compras.IdEstado = (int)Estados.Cancelada;
                us.CantidadCancelaciones++;
            }
            if (compras.CantidadEnvios == 2 && compras.FormaDePago.ToLower() != "efectivo")
            {
                compras.IdEstado = (int)Estados.Vendida;
            }
            if (us.CantidadCancelaciones == 2)
                us.IsActivo = false;
            db.SaveChanges();
            
            return RedirectToAction("GestionEnvioRetiro", "Compra");
        }

        public ActionResult EntRet(long id)
        {
            Compras compras = db.Compras.Single(c => c.IdCompra == id);
            //Usuarios us = db.Usuarios.Single(u => u.IdUsuario == compras.IdUsuario);

            compras.IsEntregado = true;
            compras.IdEstado = (int)Estados.Vendida;
            db.SaveChanges();
            
            return RedirectToAction("GestionEnvioRetiro", "Compra");
        }


        [MyAuthorizeAttribute(Roles = "Administrador")]
        public ActionResult Vendidas(long idShow = 0)
        {
            var shows = db.Shows.Where(s => s.FechaHora > DateTime.Now);
            ViewBag.shows = new SelectList(shows, "IdShow", "Artista");
            var compras = db.Compras.Where(c => c.IdEstado != (int)Estados.Cancelada && c.IdEstado == (int)Estados.Vendida).Include("Sectores").Include("ZonasEnvios").Include("Shows");
            compras = compras.Where(c => c.Shows.FechaHora >= DateTime.Now && c.Shows.IsCancelado == false);
            if (idShow != 0)
                compras = compras.Where(c => c.IdShow == idShow);
            return View(compras.ToList());
        }

        [HttpPost]
        [MyAuthorizeAttribute(Roles = "Administrador")]
        public ActionResult Vendidas(string idShow, string fecha, string ape, string dni)
        {
            var shows = db.Shows.Where(s => s.FechaHora > DateTime.Now);
            ViewBag.shows = new SelectList(shows, "IdShow", "Artista");
            var compras = db.Compras.Where(c => c.IdEstado != (int)Estados.Cancelada && c.IdEstado == (int)Estados.Vendida).Include("Sectores").Include("ZonasEnvios").Include("Shows");
            compras = compras.Where(c => c.Shows.FechaHora >= DateTime.Now && c.Shows.IsCancelado == false);
            if (idShow != "")
            {
                long id2 = Convert.ToInt64(idShow);
                compras = compras.Where(c => c.IdShow == id2);
            }
            if (fecha != "")
            {
                var fe = new DateTime();
                if (DateTime.TryParse(fecha, out fe))
                {
                    var fec = fe.Date;
                    var fecHasta = fec.AddDays(1).AddSeconds(-1);
                    compras = compras.Where(c => c.FechaHora >= fec && c.FechaHora <= fecHasta);
                }
            }
            if (ape != "")
                compras = compras.Where(c => c.Usuarios.Apellido.ToLower().Contains(ape.ToLower()));
            if (dni != "")
                compras = compras.Where(c => c.Usuarios.DNI == dni);
            return View(compras.ToList());
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult Preview(long Id)
        {
            var cantidadSectores = db.Shows.Where(s => s.IdShow == Id).First().Establecimientos.Sectores.Count;
            string path = "";
            path = ControllerContext.HttpContext.Server.MapPath("~/Content/Images/Imagen Sector.bmp");
            if (System.IO.File.Exists(path))
            {
                return File(path, "image/jpeg");
            }
            return new HttpNotFoundResult();
        }

        [HttpPost]
        public ActionResult ObtenerDatosSector(long id) 
        {
            var EstablecimientoId = Convert.ToInt64(Session["EstablecimientoId"]);
            var sectores = db.Sectores.Where(s => s.idEstablecimiento == EstablecimientoId).OrderBy(s => s.IdSector).ToList();
            int i = 1;
            foreach (var sector in sectores)
            {
                decimal costoServicio = 0.00M;
                if (sector.Descripcion.ToString().ToLower() == "campo")
                    costoServicio = Convert.ToDecimal(0.10) * sector.Precio;
                else if (sector.Descripcion.ToString().ToLower() == "platea")
                    costoServicio = Convert.ToDecimal(0.10) * sector.Precio;
                else if (sector.Descripcion.ToString().ToLower() == "campo vip")
                    costoServicio = Convert.ToDecimal(0.25) * sector.Precio;
                else
                    costoServicio = Convert.ToDecimal(0.15) * sector.Precio;
                if (i == id)
                {
                    Session["SectorId"] = sector.IdSector;
                    return Json(new { Precio = sector.Precio, Servicio = costoServicio });
                }
                i++;
            }

            return null;
        }

        public void GenerarImagen(long id)
        {
            Establecimientos establecimiento = db.Establecimientos.Where(e => e.IdEstablecimiento == id).First();
            Bitmap establecimientoImagen = new Bitmap(Server.MapPath("~") + "\\Content\\Images\\Ubicaciones.jpg");

            if (establecimiento.Sectores.Count == 1)
                establecimientoImagen = new Bitmap(Server.MapPath("~") + "\\Content\\Images\\Ubicaciones1.jpg");
            if (establecimiento.Sectores.Count == 2)
                establecimientoImagen = new Bitmap(Server.MapPath("~") + "\\Content\\Images\\Ubicaciones2.jpg");
            if (establecimiento.Sectores.Count == 3)
                establecimientoImagen = new Bitmap(Server.MapPath("~") + "\\Content\\Images\\Ubicaciones3.jpg");
            if (establecimiento.Sectores.Count == 4)
                establecimientoImagen = new Bitmap(Server.MapPath("~") + "\\Content\\Images\\Ubicaciones4.jpg");
            if (establecimiento.Sectores.Count == 5)
                establecimientoImagen = new Bitmap(Server.MapPath("~") + "\\Content\\Images\\Ubicaciones5.jpg");

            using (Bitmap b = new Bitmap(establecimientoImagen, establecimientoImagen.Width * establecimiento.Sectores.Count, establecimientoImagen.Height))
            {
                int x = 0;
                int y = 0;
                int i = 0;
                using (Graphics g = Graphics.FromImage(b))
                {
                    g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    if (establecimiento.Sectores.Count <= 3)
                    {
                        int movX = b.Width / establecimiento.Sectores.Count;
                        int mitadMovX = movX / 2;
                        foreach (var item in establecimiento.Sectores)
                        {
                            x += movX;
                            g.DrawString(item.Descripcion, new Font("Verdana", 50, GraphicsUnit.Pixel), new SolidBrush(Color.Red), x - mitadMovX, b.Height / 2);

                            i++;
                        }
                    }
                    else
                    {
                        int movX = b.Width / 3;
                        int mitadMovX = movX / 2;
                        int movY = b.Height / 2;
                        int mitadMovY = movY / 2;
                        foreach (var item in establecimiento.Sectores)
                        {
                            i++;
                            if (i <= 3)
                            {
                                if (i == 1)
                                    y = movY - mitadMovY;
                                x += movX;
                                g.DrawString(item.Descripcion, new Font("Verdana", 50, GraphicsUnit.Pixel), new SolidBrush(Color.Red), x - mitadMovX, y);
                            }
                            if (i > 3)
                            {
                                y = movY + mitadMovY;
                                if (i == 4)
                                {
                                    x = 0;
                                    movX = b.Width / (establecimiento.Sectores.Count - 3);
                                    mitadMovX = movX / 2;
                                }

                                x += movX;
                                g.DrawString(item.Descripcion, new Font("Verdana", 50, GraphicsUnit.Pixel), new SolidBrush(Color.Red), x - mitadMovX, y);

                            }
                        }


                    }
                }

                b.Save(Server.MapPath("~") + "\\Content\\Images\\Imagen Sector2.bmp");
            }

        }

    }
}