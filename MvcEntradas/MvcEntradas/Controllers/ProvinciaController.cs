﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcEntradas.Entity;
using System.Data;
using MvcEntradas.Models;

namespace MvcEntradas.Controllers
{

    public class ProvinciaController : Controller
    {
        //
        // GET: /Provincia/

        private VentaEntradasEntities db = new VentaEntradasEntities();
         [MyAuthorizeAttribute(Roles = "Administrador,Organizador")]
        public ActionResult Index()
        {
            return View(db.Provincias.ToList());
        }

         [MyAuthorizeAttribute(Roles = "Administrador,Organizador")]
        public ViewResult Details(long id)
        {
            Provincias establecimientos = db.Provincias.Single(p => p.IdProvincia == id);
            return View(establecimientos);
        }

         [MyAuthorizeAttribute(Roles = "Administrador,Organizador")]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [MyAuthorizeAttribute(Roles = "Administrador,Organizador")]
        public ActionResult Create(Provincias provincias)
        {
            if (ModelState.IsValid)
            {
                db.Provincias.AddObject(provincias);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(provincias);
        }

         [MyAuthorizeAttribute(Roles = "Administrador,Organizador")]
        public ActionResult Edit(long id)
        {
            Provincias provincias = db.Provincias.Single(p => p.IdProvincia == id);
            return View(provincias);
        }

        //
        // POST: /Sector/Edit/5

        [HttpPost]
        [MyAuthorizeAttribute(Roles = "Administrador,Organizador")]
        public ActionResult Edit(Provincias provincias)
        {
            if (ModelState.IsValid)
            {
                db.Provincias.Attach(provincias);
                db.ObjectStateManager.ChangeObjectState(provincias, EntityState.Modified);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(provincias);
        }

        //
        // GET: /Sector/Delete/5
         [MyAuthorizeAttribute(Roles = "Administrador,Organizador")]
        public ActionResult Delete(long id)
        {
            Provincias provincias = db.Provincias.Single(p => p.IdProvincia == id);
            return View(provincias);
        }

        //
        // POST: /Sector/Delete/5

        [HttpPost, ActionName("Delete")]
        [MyAuthorizeAttribute(Roles = "Administrador,Organizador")]
        public ActionResult DeleteConfirmed(long id)
        {
            Provincias provincias = db.Provincias.Single(p => p.IdProvincia == id);
            db.Provincias.DeleteObject(provincias);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

    }
}
