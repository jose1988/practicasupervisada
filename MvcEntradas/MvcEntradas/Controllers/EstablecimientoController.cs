﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcEntradas.Entity;
using System.Data;
using MvcEntradas.Models;
using System.Drawing;
using System.IO;
using System.Drawing.Drawing2D;

namespace MvcEntradas.Controllers
{
    public class EstablecimientoController : Controller
    {
        //
        // GET: /Establecimiento/

        private VentaEntradasEntities db = new VentaEntradasEntities();

        [MyAuthorizeAttribute(Roles = "Administrador,Organizador")]
        public ActionResult Index()
        {
            var establecimientos = db.Establecimientos.Include("Sectores");
                
            return View(db.Establecimientos.ToList());
        }

        [HttpPost]
        [MyAuthorizeAttribute(Roles = "Administrador,Organizador")]
        public ActionResult Index(string nombre, string cap)
        {
            System.Linq.IQueryable<Establecimientos> establecimientos =  db.Establecimientos.OrderBy(e => e.Nombre);

            long capa = 0;
            if (nombre != "")
                establecimientos = establecimientos.Where(e => e.Nombre.ToLower().Contains(nombre.ToLower()));
            if(cap != "")
            {
                capa = Convert.ToInt64(cap);
                establecimientos = establecimientos.Where(e => e.CapacidadTotal == capa);
            }
            return View(establecimientos);
        }

       [MyAuthorizeAttribute(Roles = "Administrador,Organizador")]
        public ViewResult Details(long id)
        {
            Establecimientos establecimientos = db.Establecimientos.Single(s => s.IdEstablecimiento == id);
            return View(establecimientos);
        }

    [MyAuthorizeAttribute(Roles = "Administrador,Organizador")]
        public ActionResult Create()
        {
            Session["cantSec"] = null;
            return View();
        } 

        [HttpPost]
        [MyAuthorizeAttribute(Roles = "Administrador,Organizador")]
        public ActionResult Create(Establecimientos establecimientos, string operacion = null)
        {
            if (operacion == "generar-imagen")
            {
                Session["cantSec"] = establecimientos.Sectores.Count();
                GenerarImagen(establecimientos);
                Session["generar"] = 1;
            }
            else if (operacion == null)
            {
                if (establecimientos.Sectores != null)
                {
                    if (establecimientos.Sectores.Sum(s => s.capacidad) > establecimientos.CapacidadTotal)
                    {
                        Session["msjErrorEstablecimiento"] = "La capacidad de los sectores supera la capacidad total.";
                        return View(establecimientos);
                    }
                    else
                        Session["msjErrorEstablecimiento"] = "";
                }
                if (CrearEstablecimiento(establecimientos))
                {
                    return RedirectToAction("Index");
                }

            }
            else if (operacion == "agregar-sector")
            {
                establecimientos.Sectores.Add(new Sectores());
                Session["msjErrorEstablecimiento"] = "";
                Session["cantSec"] = establecimientos.Sectores.Count();
                Session["generar"] = 0;
            }
          
           
            return View(establecimientos);
        }
        private static void EliminarDetallePorIndice(Establecimientos establecimientos, int? id)
        {
            // se asume que en el parametro 'operacion' viene el index del detalle a eliminar.
            Sectores item = null; ;
            if (id != null)
            {
                 item = establecimientos.Sectores.Where(i => id == id.Value).FirstOrDefault();
                
                establecimientos.Sectores.Remove(item);
              
            }
        }
                
        private bool CrearEstablecimiento(Establecimientos establecimiento)
        {
            //if (ModelState.IsValid)
            //{
                if (establecimiento.Sectores != null && establecimiento.Sectores.Count > 0)
                {
                    //if (ModelState.IsValid)
                    //{
                        //GenerarImagen(establecimiento);
                        db.Establecimientos.AddObject(establecimiento);
                        db.SaveChanges();
                       
                    //}
                    return true;
                }
                else
                {
                    ModelState.AddModelError("", "No puede guardar establecimientos sin sectores");
                }
            //}
 
            return false;
        }


    [MyAuthorizeAttribute(Roles = "Administrador,Organizador")]
        public ActionResult Edit(long id)
        {
            Establecimientos establecimientos = db.Establecimientos.Single(s => s.IdEstablecimiento == id);
            return View(establecimientos);
        }

        //
        // POST: /Sector/Edit/5

        [HttpPost]
        [MyAuthorizeAttribute(Roles = "Administrador,Organizador")]
        public ActionResult Edit(Establecimientos establecimientos, string operacion = null, int? idSector = null)
        {
            if (operacion == null)
            {
                if (establecimientos.Sectores.Sum(s => s.capacidad) > establecimientos.CapacidadTotal)
                {
                    Session["msjErrorEstablecimiento"] = "La capacidad de los sectores supera la capacidad total.";
                    return View(establecimientos);
                }
                else
                    Session["msjErrorEstablecimiento"] = "";
                if (EditarEstablecimiento(establecimientos))
                {
                    return RedirectToAction("Index");
                }

            }
            else if (operacion == "agregar-sector")
            {
                Session["msjErrorEstablecimiento"] = "";
                Sectores nuevoSector = new Sectores();
                nuevoSector.idEstablecimiento = establecimientos.IdEstablecimiento;
                Random r = new Random();
                nuevoSector.IdSector = r.Next(0, 200) - 500;
                establecimientos.Sectores.Add(nuevoSector);
            }
            else if (operacion.StartsWith("eliminar-detalle"))
            {
                Session["msjErrorEstablecimiento"] = "";
                EliminarDetallePorIndice(establecimientos, idSector);
            }

            return View(establecimientos);
        }

        private bool EditarEstablecimiento(Establecimientos establecimiento)
        {


            //if (ModelState.IsValid)
            //{
                    if (establecimiento.Sectores != null && establecimiento.Sectores.Count > 0)
                    {  
                          var enbase= db.Sectores.Where(x => x.idEstablecimiento == establecimiento.IdEstablecimiento).Select(X=> X.IdSector).ToList();
                          var enCliente = establecimiento.Sectores.Select(x => x.IdSector).ToList();
                          IList<Int64> itemToDelete = enbase.Except(enCliente).ToList();
                          db.Establecimientos.Attach(establecimiento);
                          foreach (var item in itemToDelete)
                          {
                              if (item != 0)
                              {
                                  Sectores aBorrar = db.Sectores.Where(o => o.idEstablecimiento == establecimiento.IdEstablecimiento && o.IdSector == item).Single();
                                  db.ObjectStateManager.ChangeObjectState(aBorrar, EntityState.Deleted);
                              }
                              
                          }
                       
                        foreach(var item in establecimiento.Sectores)
                        {
                            if (item.IdSector == 0 || item.IdSector < 0)
                            {
                                db.ObjectStateManager.ChangeObjectState(item, EntityState.Added);

                            }
                            else
                            {
                                db.ObjectStateManager.ChangeObjectState(item, EntityState.Modified);
                              
                              
                            }
                        }
                        db.ObjectStateManager.ChangeObjectState(establecimiento, EntityState.Modified);

                                
                        db.SaveChanges();
                        return true;
                    }
                    else
                    {
                        ModelState.AddModelError("", "No puede guardar establecimientos sin sectores");
                        return false;
                    }
            //    }
            
            //return false;
        }

        //
        // GET: /Sector/Delete/5
         [MyAuthorizeAttribute(Roles = "Administrador,Organizador")]
        public ActionResult Delete(long id)
        {
            Establecimientos establecimientos = db.Establecimientos.Single(s => s.IdEstablecimiento == id);
            return View(establecimientos);
        }

        //
        // POST: /Sector/Delete/5

        [HttpPost, ActionName("Delete")]
        [MyAuthorizeAttribute(Roles = "Administrador,Organizador")]
        public ActionResult DeleteConfirmed(long id)
        {
            Establecimientos estableciemientos = db.Establecimientos.Single(s => s.IdEstablecimiento == id);
            var idToDelete = estableciemientos.Sectores.Select(x => x.IdSector).ToList();
            foreach(var item in idToDelete)
            {
                Sectores delItem = db.Sectores.Where(x => x.IdSector == item).FirstOrDefault();
                db.ObjectStateManager.ChangeObjectState(delItem, EntityState.Deleted);
            }
            db.Establecimientos.DeleteObject(estableciemientos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }


        //public void GenerarImagen(Establecimientos establecimiento)
        //{
        //    Bitmap sector = new Bitmap(Server.MapPath("~") + "\\Content\\Images\\Imagen Sector.bmp");
      
        //    using (Bitmap b = new Bitmap(sector, sector.Width * establecimiento.Sectores.Count, sector.Height))
        //    {
        //        int x = 0;
        //        int i = 0;
        //        using (Graphics g = Graphics.FromImage(b))
        //        {
        //            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
        //            foreach(var item in establecimiento.Sectores)
        //            {
        //                using (Image Nueva = new Bitmap(Server.MapPath("~") + "\\Content\\Images\\Imagen Sector.bmp"))
        //                {
        //                    g.DrawString(item.Descripcion, new Font("Verdana", 15, GraphicsUnit.Pixel), new SolidBrush(Color.Red), x + (b.Width / 2), b.Height / 2);
        //                    g.DrawImage(Nueva, x, 0);

        //                }
        //                x += sector.Width;
        //                i++;
        //            }
        //        }
        //        using (MemoryStream ms = new MemoryStream())
        //        {
        //            b.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);

        //            byte[] imageBytes = ms.ToArray();
        //            establecimiento.Imagen = imageBytes;
        //        }
        //    }
          
        //}

        public ActionResult Preview(long id)
        {
            Establecimientos establecimiento = db.Establecimientos.Where(e => e.IdEstablecimiento == id).First();
            if (establecimiento.Imagen != null)
            {
                return File(establecimiento.Imagen, "image/jpeg");
            }
            return new HttpNotFoundResult();
        }



        public void GenerarImagen(Establecimientos establecimiento)
        {
            Bitmap establecimientoImagen = new Bitmap(Server.MapPath("~") + "\\Content\\Images\\Ubicaciones.jpg");
            int cant = establecimiento.Sectores.Count();
            Session["cantSec"] = cant;
            if(cant == 0 || cant >5)
                establecimientoImagen = new Bitmap(Server.MapPath("~") + "\\Content\\Images\\imagen-no-disponible.jpg");
            if (cant == 1)
                establecimientoImagen = new Bitmap(Server.MapPath("~") + "\\Content\\Images\\Ubicaciones1.jpg");
            if (cant == 2)
                establecimientoImagen = new Bitmap(Server.MapPath("~") + "\\Content\\Images\\Ubicaciones2.jpg");
            if (cant == 3)
                establecimientoImagen = new Bitmap(Server.MapPath("~") + "\\Content\\Images\\Ubicaciones3.jpg");
            if (cant == 4)
                establecimientoImagen = new Bitmap(Server.MapPath("~") + "\\Content\\Images\\Ubicaciones4.jpg");
            if (cant == 5)
                establecimientoImagen = new Bitmap(Server.MapPath("~") + "\\Content\\Images\\Ubicaciones5.jpg");

            if (cant != 0 && cant <= 5)
            {
                using (Bitmap b = new Bitmap(establecimientoImagen, establecimientoImagen.Width * cant, establecimientoImagen.Height))
                {
                    int x = 0;
                    int y = 0;
                    int i = 0;
                    using (Graphics g = Graphics.FromImage(b))
                    {

                        g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                        if (cant <= 3)
                        {
                            int movX = b.Width / cant;
                            int mitadMovX = movX / 2;
                            foreach (var item in establecimiento.Sectores)
                            {
                                x += movX;
                                g.DrawString(item.Descripcion, new Font("Verdana", 20, GraphicsUnit.Pixel), new SolidBrush(Color.Red), x - mitadMovX, b.Height / 2);

                                i++;
                            }
                        }
                        else
                        {
                            int movX = b.Width / 3;
                            int mitadMovX = movX / 2;
                            int movY = b.Height / 2;
                            int mitadMovY = movY / 2;
                            foreach (var item in establecimiento.Sectores)
                            {
                                i++;
                                if (i <= 3)
                                {
                                    if (i == 1)
                                        y = movY - mitadMovY;
                                    x += movX;
                                    g.DrawString(item.Descripcion, new Font("Verdana", 20, GraphicsUnit.Pixel), new SolidBrush(Color.Red), x - mitadMovX, y);
                                }
                                if (i > 3)
                                {
                                    if (i == 4)
                                    {
                                        x = 0;
                                        y = movY + mitadMovY;
                                    }

                                    if (i == 5)
                                    {
                                        x = 0;
                                        y = movY + mitadMovY + (mitadMovY / 2);
                                    }

                                    x += movX;
                                    g.DrawString(item.Descripcion, new Font("Verdana", 20, GraphicsUnit.Pixel), new SolidBrush(Color.Red), x - mitadMovX, y);

                                }
                            }


                        }
                    }

                    b.Save(Server.MapPath("~") + "\\Content\\Images\\Imagen Sector2.bmp");
                }
            }
            else
            {
                Bitmap b = new Bitmap(establecimientoImagen, establecimientoImagen.Width, establecimientoImagen.Height);
                b.Save(Server.MapPath("~") + "\\Content\\Images\\Imagen Sector2.bmp");
            }

        }
    }
}
