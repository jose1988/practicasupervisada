﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcEntradas.Entity;

namespace MvcEntradas.Controllers
{
    public class HomeController : Controller
    {
        private VentaEntradasEntities db = new VentaEntradasEntities();

        public ActionResult Index()
        {
            // Para mostrar en los div debajo del carrusel
            ViewBag.Message = "";
            DateTime fechaDesde = DateTime.Now;
            DateTime fechaHasta = fechaDesde.AddMonths(2);
            //var shows = db.Shows.Include("Establecimientos").Where(s => s.FechaHora >= fechaDesde && s.FechaHora <= fechaHasta);
            var showsCarrusel = db.Shows.Include("Establecimientos").Where(s => s.IsCarrusel == true && s.FechaHora >= fechaDesde && s.IsCancelado == false);
            var showsDiv = db.Shows.Include("Establecimientos").Where(s => s.FechaHora >= fechaDesde && s.FechaHora <= fechaHasta && s.IsCancelado == false && s.IsAprobado);
            //Session["carrusel"] = shows.ToList();
            ArmarCarrusel(showsCarrusel);
            ArmarDivCentral(showsDiv);
            return View(showsCarrusel.ToList());
            //return View();
        }

        private void ArmarCarrusel(IEnumerable<Shows> lista)
        {
            int index = 0;
            string myCarousel = "<ol class='carousel-indicators'>";
            foreach (var show in lista)
            {
                myCarousel += "<li data-target='#myCarousel' data-slide-to="
                    + "'" + index + "'";
                if (index == 0)
                    myCarousel += "class='active'";
                myCarousel += "></li>";

                index++;
            }

            myCarousel += "</ol>";

            int index2 = 0;
            string source = "../../Content/Images/";
            string[] stringSeparators = new string[] { "\\Images\\" };
            myCarousel += "<div class='carousel-inner'>";
            foreach (var show in lista)
            {

                string[] result = show.Imagen.Split(stringSeparators, StringSplitOptions.None);
                string srcCompleta = source + result[1];
                if (index2 == 0)
                    myCarousel += "<div class='item active'>";
                else
                    myCarousel += "<div class='item'>";

                myCarousel += "<img src='" + srcCompleta + "' alt='" + index2 + "slide'>" +
                                         "<div class='container'>" +
                                         "<div class='carousel-caption'>" +
          "<p><a class='btn btn-lg btn-primary' href='../../Compra/Create?IdShow=" + show.IdShow + "' role='button'>Comprar entradas</a></p>" +
        "</div></div></div>";

                index2++;
            }
            myCarousel += "</div>";
            myCarousel += "<a class='left carousel-control' href='#myCarousel' role='button' data-slide='prev'><span class='glyphicon glyphicon-chevron-left'></span></a>" +
                                    "<a class='right carousel-control' href='#myCarousel' role='button' data-slide='next'><span class='glyphicon glyphicon-chevron-right'></span></a>";
           
            //lo guardo para cargar en  js
            Session["carrusel"] = myCarousel;
        }

        private void ArmarDivCentral(IEnumerable<Shows> lista)
        {
            int index = 0;

            string myDiv = "";
            string source = "../../Content/Images/";
            string[] stringSeparators = new string[] { "\\Images\\" };

            foreach (var show in lista)
            {
                string[] result = show.Imagen.Split(stringSeparators, StringSplitOptions.None);
                string srcCompleta = source + result[1];

                myDiv += "<div class='col-sm-6 col-md-4'>" +
                        "<div class='thumbnail'>" +
                        "<img src='" + srcCompleta + "'" +
                        "style='height: 200px; width: 100%; display: block;' data-src='holder.js/100%x200' alt='100%x200'>" +
                        "<div class='caption'>" +
                        "<h3>" + show.Artista + "</h3>" +
                        " <p>" + show.Descripcion + "</p>" +
                        " <p>" + show.FechaHora + "</p>" +
                        "<p><a href='../../Compra/Create?IdShow=" + show.IdShow + "' class='btn btn-primary' role='button'>Comprar</a></p>" +
                        "</div></div></div>";
            }

            //lo guardo para cargar en  js
            Session["divCentral"] = myDiv;
        }
        public ActionResult About()
        {
            return View();
        }
    }
}
