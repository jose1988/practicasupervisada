﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcEntradas.Entity;
using System.Data;
using MvcEntradas.Models;

namespace MvcEntradas.Controllers
{
    public class ListaNegraController : Controller
    {

        private VentaEntradasEntities db = new VentaEntradasEntities();

        //
        // GET: /ListaNegra/
         [MyAuthorizeAttribute(Roles = "Administrador")]
        public ActionResult Index()
        {
            // Perfil de comprador 3
            return View(db.Usuarios.Where(c => !c.IsActivo && c.IdPerfil == 3).ToList());
        }
            
         [HttpPost]
         [MyAuthorizeAttribute(Roles = "Administrador")]
         public ActionResult Index(string dni, string nombreU, string ape, string dom)
         {
             // Perfil de comprador 3

             var compradores = db.Usuarios.Where(c => !c.IsActivo && c.IdPerfil == 3);

             if (dni != "")
                 compradores = compradores.Where(c => c.DNI.Contains(dni));
             if(nombreU != "")
                 compradores = compradores.Where(c => c.NombreUsuario.ToLower().Contains(nombreU.ToLower()));
             if(ape != "")
                 compradores = compradores.Where(c => c.Apellido.ToLower().Contains(ape.ToLower()));
             if(dom != "")
                 compradores = compradores.Where(c => c.Calle.ToLower().Contains(dom.ToLower()));
             return View(compradores);
         }

         [MyAuthorizeAttribute(Roles = "Administrador")]
        public ActionResult Habilitar(long id)
        {
            Usuarios usuarios = db.Usuarios.Single(u => u.IdUsuario == id);
            return View(usuarios);
        }

        //
        // POST: /Usuario/Delete/5

        [HttpPost, ActionName("Habilitar")]
        [MyAuthorizeAttribute(Roles = "Administrador")]
        public ActionResult HabilitarConfirmed(long id)
        {
            Usuarios usuarios = db.Usuarios.Single(u => u.IdUsuario == id);
            usuarios.IsActivo = true;
            //db.Usuarios.DeleteObject(usuarios);
            db.SaveChanges();
            return RedirectToAction("Index");
        }


    }
}
