﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcEntradas.Entity;

namespace MvcEntradas.Controllers
{ 
    public class DomicilioController : Controller
    {
        private VentaEntradasEntities db = new VentaEntradasEntities();

        //
        // GET: /Domicilio/

        public ViewResult Index()
        {
            var domicilio = db.Domicilio.Include("Usuarios").Include("ZonasEnvios");
            return View(domicilio.ToList());
        }

        //
        // GET: /Domicilio/Details/5

        public ViewResult Details(long id)
        {
            Domicilio domicilio = db.Domicilio.Single(d => d.IdDomicilio == id);
            return View(domicilio);
        }

        //
        // GET: /Domicilio/Create

        public ActionResult Create()
        {
            ViewBag.IdUsuario = new SelectList(db.Usuarios, "IdUsuario", "NombreUsuario");
            ViewBag.IdZonaEnvio = new SelectList(db.ZonasEnvios, "IdZona", "Nombre");
            return View();
        } 

        //
        // POST: /Domicilio/Create

        [HttpPost]
        public ActionResult Create(Domicilio domicilio)
        {
            if (ModelState.IsValid)
            {
                db.Domicilio.AddObject(domicilio);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            ViewBag.IdUsuario = new SelectList(db.Usuarios, "IdUsuario", "NombreUsuario", domicilio.IdUsuario);
            ViewBag.IdZonaEnvio = new SelectList(db.ZonasEnvios, "IdZona", "Nombre", domicilio.IdZonaEnvio);
            return View(domicilio);
        }
        
        //
        // GET: /Domicilio/Edit/5
 
        public ActionResult Edit(long id)
        {
            Domicilio domicilio = db.Domicilio.Single(d => d.IdDomicilio == id);
            ViewBag.IdUsuario = new SelectList(db.Usuarios, "IdUsuario", "NombreUsuario", domicilio.IdUsuario);
            ViewBag.IdZonaEnvio = new SelectList(db.ZonasEnvios, "IdZona", "Nombre", domicilio.IdZonaEnvio);
            return View(domicilio);
        }

        //
        // POST: /Domicilio/Edit/5

        [HttpPost]
        public ActionResult Edit(Domicilio domicilio)
        {
            if (ModelState.IsValid)
            {
                db.Domicilio.Attach(domicilio);
                db.ObjectStateManager.ChangeObjectState(domicilio, EntityState.Modified);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdUsuario = new SelectList(db.Usuarios, "IdUsuario", "NombreUsuario", domicilio.IdUsuario);
            ViewBag.IdZonaEnvio = new SelectList(db.ZonasEnvios, "IdZona", "Nombre", domicilio.IdZonaEnvio);
            return View(domicilio);
        }

        //
        // GET: /Domicilio/Delete/5
 
        public ActionResult Delete(long id)
        {
            Domicilio domicilio = db.Domicilio.Single(d => d.IdDomicilio == id);
            return View(domicilio);
        }

        //
        // POST: /Domicilio/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(long id)
        {            
            Domicilio domicilio = db.Domicilio.Single(d => d.IdDomicilio == id);
            db.Domicilio.DeleteObject(domicilio);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}