﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcEntradas.Entity;
using MvcEntradas.Models;
using System.Web.Security;

namespace MvcEntradas.Controllers
{ 
    public class UsuarioController : Controller
    {
        private VentaEntradasEntities db = new VentaEntradasEntities();

        //
        // GET: /Usuario/CreateOrganizador
       [MyAuthorizeAttribute(Roles = "Administrador")]
        public ActionResult CreateOrganizador()
        {
            ViewBag.IdProvincia = new SelectList(db.Provincias, "IdProvincia", "Descripcion");
            SelectList s = new SelectList(db.Perfiles, "IdPerfil", "Nombre");
            // no quiero que se pongan los compradores en el combo
            ViewBag.IdPerfil = s.Where(p => p.Value != Convert.ToString(3));
            return View();
        }

        //
        // POST: /Usuario/CreateOrganizador
        //Crear Organizador/Administrador
        [HttpPost]
        [MyAuthorizeAttribute(Roles = "Administrador")]
        public ActionResult CreateOrganizador(Usuarios usuario)
        {
            RegisterModel model = new RegisterModel();
            try
            {
                if (ModelState.IsValid)
                {
                    usuario.IsActivo = true;
                    
                    //----------
                    //Creacion de Usuario en base AspMembership de MVC
                    
                    model.UserName = usuario.NombreUsuario;
                    model.Password = usuario.Password;
                    model.Email = usuario.Email;
                    model.ConfirmPassword = usuario.Password;
                    MembershipCreateStatus createStatus;
                    Membership.CreateUser(model.UserName, model.Password, model.Email, null, null, true, null, out createStatus);
                   
                    if (createStatus == MembershipCreateStatus.Success)
                    {
                        // Se llaman igual q en nuestra base los roles
                        if (usuario.IdPerfil == 1)
                            Roles.AddUserToRole(model.UserName, "Administrador");
                        if (usuario.IdPerfil == 2)
                            Roles.AddUserToRole(model.UserName, "Organizador");
                        db.Usuarios.AddObject(usuario);
                        db.SaveChanges();
                        if (usuario.IdPerfil == 1)
                            return RedirectToAction("IndexAdministrador");
                        if (usuario.IdPerfil == 2)
                            return RedirectToAction("IndexOrganizador");
                    }
                    //--------------------------------
                    else
                    {
                        ModelState.AddModelError("", AccountController.ErrorCodeToString(createStatus));
                    }
                }
            }
            catch (Exception e)
            {
                // Borro del AspMembership porque no se pudo crear en nuestra BD
                Membership.DeleteUser(model.UserName);
                ModelState.AddModelError("", "Error de creación de usuario: " + e.Message);
            }

            ViewBag.IdPerfil = new SelectList(db.Perfiles, "IdPerfil", "Nombre", usuario.IdPerfil);
            ViewBag.IdProvincia = new SelectList(db.Provincias, "IdProvincia", "Descripcion", usuario.IdProvincia);
            return View(usuario);
        }

        //
        // GET: /Usuario/IndexOrganizador
        //Lista de usuarios Admin
         [MyAuthorizeAttribute(Roles = "Administrador")]
        public ViewResult IndexAdministrador()
        {
            var usuarios = db.Usuarios.Include("Perfiles").Include("Provincias");
            return View(usuarios.Where(c => c.IdPerfil == 1).ToList());
        }

        //
        // GET: /Usuario/IndexOrganizador
        //Lista de usuarios organizadores
        [MyAuthorizeAttribute(Roles = "Administrador")]
        public ViewResult IndexOrganizador()
        {
            var usuarios = db.Usuarios.Include("Perfiles").Include("Provincias");
           //Suponiendo que 2 es el perfil organizador
            return View(usuarios.Where(c => c.IdPerfil == 2).ToList());
        }

        //
        // GET: /Usuario/
        //Lista de usuarios Compradores
        [MyAuthorizeAttribute(Roles = "Administrador")]
        public ViewResult Index()
        {
            var usuarios = db.Usuarios.Include("Perfiles").Include("Provincias").Where(c => c.IsActivo);
            return View(usuarios.Where(c => c.IdPerfil == 3).ToList());
        }

        [HttpPost]
        [MyAuthorizeAttribute(Roles = "Administrador")]
        public ViewResult Index(string dni, string nombreU, string ape, string dom)
        {
            var usuarios = db.Usuarios.Include("Perfiles").Include("Provincias").Where(c => c.IsActivo && c.IdPerfil == 3);
            if (dni != "")
                usuarios = usuarios.Where(c => c.DNI.Contains(dni));
            if (nombreU != "")
                usuarios = usuarios.Where(c => c.NombreUsuario.ToLower().Contains(nombreU.ToLower()));
            if (ape != "")
                usuarios = usuarios.Where(c => c.Apellido.ToLower().Contains(ape.ToLower()));
            if (dom != "")
                usuarios = usuarios.Where(c => c.Calle.ToLower().Contains(dom.ToLower()));
            
            return View(usuarios);
        }
        
        //
        // GET: /Usuario/Details/5

        public ViewResult Details(long id)
        {
            Usuarios usuarios = db.Usuarios.Single(u => u.IdUsuario == id);
            return View(usuarios);
        }

        //
        // GET: /Usuario/Details/5
        //Detalle para admin y organizador
        [MyAuthorizeAttribute(Roles = "Administrador,Organizador")]
        public ViewResult DetailsOrganizador(long id)
        {
            Usuarios usuarios = db.Usuarios.Single(u => u.IdUsuario == id);
            return View(usuarios);
        }
        //
        // GET: /Usuario/Create

        public ActionResult Create()
        {
            ViewBag.IdPerfil = new SelectList(db.Perfiles, "IdPerfil", "Nombre");
            ViewBag.IdZonaEnvio = new SelectList(db.ZonasEnvios, "IdZona", "Nombre");
            ViewBag.IdProvincia = new SelectList(db.Provincias, "IdProvincia", "Descripcion");
            return View();
        } 

        //
        // POST: /Usuario/Create

        [HttpPost]
        public ActionResult Create(Usuarios usuarios)
        {
            RegisterModel model = new RegisterModel();
            try
            {
                if (ModelState.IsValid)
                {
                    usuarios.IdPerfil = 3; // Perfil de Comprador
                    usuarios.IsActivo = true;

                    //Creacion de Usuario en base AspMembership de MVC
                    model.UserName = usuarios.NombreUsuario;
                    model.Password = usuarios.Password;
                    model.Email = usuarios.Email;
                    model.ConfirmPassword = usuarios.Password;
                    // Valida el membership provider que no se duplique el mail. No hace falta mas
                    //MembershipUser oldUser = Membership.GetUser(model.UserName, false);
                    //if (oldUser != null)
                    //    throw new Exception("Ya existe un usuario con el nombre de usuario ingresado. Por favor elija uno nuevo");
                    MembershipCreateStatus createStatus;
                    Membership.CreateUser(model.UserName, model.Password, model.Email, null, null, true, null, out createStatus);
                    
                    if (createStatus == MembershipCreateStatus.Success)
                    {
                        // Le pongo el rol en la base del AspMembership
                        // Se llaman igual q en nuestra base los roles
                        Roles.AddUserToRole(model.UserName, "Comprador");
                        // lo loguea
                        FormsAuthentication.SetAuthCookie(model.UserName, false /* createPersistentCookie */);
                        db.Usuarios.AddObject(usuarios);
                        // lo guarda en bd VentraEtradas

                        if (usuarios.NroTarjeta != null)
                        {
                            Tarjetas tarjeta = new Tarjetas();
                            tarjeta.FechaVencimiento = usuarios.FechaVencimientoTarjeta.Value;
                            tarjeta.NroTarjeta = usuarios.NroTarjeta;
                            tarjeta.IdUsuario = usuarios.IdUsuario;
                            tarjeta.IsPrincipal = true;
                            tarjeta.FechaAlta = DateTime.Now;

                            db.Tarjetas.AddObject(tarjeta);
                        }
                        if (usuarios.IdZonaEnvio != null)
                        {
                            Domicilio d = new Domicilio();
                            d.Calle = usuarios.Calle;
                            d.CodigoPostal = usuarios.CodigoPostal.Value;
                            d.Numero = usuarios.NumeroCalle.Value;
                            d.Localidad = usuarios.Localidad;
                            d.IdUsuario = usuarios.IdUsuario;
                            d.Departamento = usuarios.Departamento;
                            d.IdZonaEnvio = usuarios.IdZonaEnvio.Value;
                            d.Piso = usuarios.Piso;
                            db.Domicilio.AddObject(d);
                        }

                        db.SaveChanges();
                        Session["perfil"] = usuarios.IdPerfil;
                        Session["idUsuario"] = usuarios.IdUsuario;
                        return RedirectToAction("Index", "Home", new { perfil = usuarios.IdPerfil });
                        //return RedirectToAction("Index");
                    }
                    //--------------------------------
                    else
                    {
                        ModelState.AddModelError("", AccountController.ErrorCodeToString(createStatus));
                      
                    }
                }
            }
            catch (Exception e)
            {
                // Borro del AspMembership porque no se pudo crear en nuestra BD
                Membership.DeleteUser(model.UserName);
                ModelState.AddModelError("", "Error de creación de usuario: "+ e.Message);
            }

            ViewBag.IdPerfil = new SelectList(db.Perfiles, "IdPerfil", "Nombre", usuarios.IdPerfil);
            ViewBag.IdZonaEnvio = new SelectList(db.ZonasEnvios, "IdZona", "Nombre");
            ViewBag.IdProvincia = new SelectList(db.Provincias, "IdProvincia", "Descripcion", usuarios.IdProvincia);
            return View(usuarios);
        }
        
        //
        // GET: /Usuario/Edit/5
 
        public ActionResult Edit(long id)
        {
            Usuarios usuarios = db.Usuarios.Single(u => u.IdUsuario == id);
            ViewBag.IdPerfil = new SelectList(db.Perfiles, "IdPerfil", "Nombre", usuarios.IdPerfil);
            ViewBag.IdProvincia = new SelectList(db.Provincias, "IdProvincia", "Descripcion", usuarios.IdProvincia);
            return View(usuarios);
        }

        //
        // POST: /Usuario/Edit/5
        //Editar para compradores
        [HttpPost]
        [MyAuthorizeAttribute(Roles = "Comprador")]
        public ActionResult Edit(Usuarios usuarios)
        {
            if (ModelState.IsValid)
            {
                db.Usuarios.Attach(usuarios);
                db.ObjectStateManager.ChangeObjectState(usuarios, EntityState.Modified);

                Tarjetas tarjeta = db.Tarjetas.Where(t => t.IdUsuario == usuarios.IdUsuario && t.IsPrincipal.HasValue && t.IsPrincipal.Value).FirstOrDefault();
                if (tarjeta == null)
                    tarjeta = new Tarjetas();
                tarjeta.FechaVencimiento = usuarios.FechaVencimientoTarjeta.Value;
                tarjeta.NroTarjeta = usuarios.NroTarjeta;
                tarjeta.IdUsuario = usuarios.IdUsuario;
                
                db.SaveChanges();
                MembershipUser oldUser = Membership.GetUser(usuarios.NombreUsuario, true);
               //Si cambio el email lo actualizo en el AspMembership
                if (oldUser.Email != usuarios.Email)
                {
                    oldUser.Email = usuarios.Email;
                    Membership.UpdateUser(oldUser);
                }

                return RedirectToAction("Index");
            }
            //ViewBag.IdPerfil = new SelectList(db.Perfiles, "IdPerfil", "Nombre", usuarios.IdPerfil);
            ViewBag.IdProvincia = new SelectList(db.Provincias, "IdProvincia", "Descripcion", usuarios.IdProvincia);
            return View(usuarios);
        }

        //
        // GET: /Usuario/Edit/5
        //Editar para organizador/admin
         [MyAuthorizeAttribute(Roles = "Administrador,Organizador")]
        public ActionResult EditOrganizador(long id)
        {
            Usuarios usuarios = db.Usuarios.Single(u => u.IdUsuario == id);
            ViewBag.IdPerfil = new SelectList(db.Perfiles, "IdPerfil", "Nombre", usuarios.IdPerfil);
            ViewBag.IdProvincia = new SelectList(db.Provincias, "IdProvincia", "Descripcion", usuarios.IdProvincia);
            return View(usuarios);
        }

        //
        // POST: /Usuario/Edit/5
        //Editar para organizador/admin
        [HttpPost]
        [MyAuthorizeAttribute(Roles = "Administrador,Organizador")]
        public ActionResult EditOrganizador(Usuarios usuarios)
        {
            if (ModelState.IsValid)
            {
                db.Usuarios.Attach(usuarios);
                db.ObjectStateManager.ChangeObjectState(usuarios, EntityState.Modified);
                db.SaveChanges();
                MembershipUser oldUser = Membership.GetUser(usuarios.NombreUsuario, true);
                //Si cambio el email lo actualizo en el AspMembership
                if (oldUser.Email != usuarios.Email)
                {
                    oldUser.Email = usuarios.Email;
                    Membership.UpdateUser(oldUser);
                }

                if (usuarios.IdPerfil == 1 && User.IsInRole("Administrador"))
                    return RedirectToAction("IndexAdministrador");
                if (usuarios.IdPerfil == 2 && User.IsInRole("Administrador"))
                    return RedirectToAction("IndexOrganizador");
                if (User.IsInRole("Organizador"))
                    return RedirectToAction("DetailsOrganizador", new { id = usuarios.IdUsuario });
            }
            SelectList s = new SelectList(db.Perfiles, "IdPerfil", "Nombre");
            // no quiero que se pongan los compradores en el combo
            ViewBag.IdPerfil = s.Where(p => p.Value != Convert.ToString(3));
            ViewBag.IdProvincia = new SelectList(db.Provincias, "IdProvincia", "Descripcion", usuarios.IdProvincia);
            return View(usuarios);
        }


        //
        // GET: /Usuario/Delete/5
          [MyAuthorizeAttribute(Roles = "Administrador")]
        public ActionResult Delete(long id)
        {
            Usuarios usuarios = db.Usuarios.Single(u => u.IdUsuario == id);
            return View(usuarios);
        }

        //
        // POST: /Usuario/Delete/5
        //Delete para Compradores, deshabilita
        [HttpPost, ActionName("Delete")]
        [MyAuthorizeAttribute(Roles = "Administrador")]
        public ActionResult DeleteConfirmed(long id)
        {            
            Usuarios usuarios = db.Usuarios.Single(u => u.IdUsuario == id);
            usuarios.IsActivo = false;
            //db.Usuarios.DeleteObject(usuarios);
            db.SaveChanges();
            return RedirectToAction("Index");
        }


        //
        // GET: /Usuario/Delete/5
        //Delete para organizador/Admin borra en serio
          [MyAuthorizeAttribute(Roles = "Administrador,Organizador")]
        public ActionResult DeleteOrganizador(long id)
        {
            Usuarios usuarios = db.Usuarios.Single(u => u.IdUsuario == id);
            return View(usuarios);
        }

        //
        // POST: /Usuario/Delete/5
        //Delete para organizador/Admin borra en serio
        [HttpPost, ActionName("DeleteOrganizador")]
        [MyAuthorizeAttribute(Roles = "Administrador,Organizador")]
        public ActionResult DeleteOrganizadorConfirmed(long id)
        {
            Usuarios usuario = db.Usuarios.Single(u => u.IdUsuario == id);
            db.Usuarios.DeleteObject(usuario);
            db.SaveChanges();
            MembershipUser oldUser = Membership.GetUser(usuario.NombreUsuario, true);
            Membership.DeleteUser(oldUser.UserName);
            if (usuario.IdPerfil == 1 && User.IsInRole("Administrador"))
                return RedirectToAction("IndexAdministrador");
            else if (usuario.IdPerfil == 2 && User.IsInRole("Administrador"))
                return RedirectToAction("IndexOrganizador");
            //Organizador se borra a si mismo
            else
            {
                FormsAuthentication.SignOut();
                Session["perfil"] = 0;
                Session["idUsuario"] = 0;
                return RedirectToAction("Index", "Home");
            }
        }

        [MyAuthorizeAttribute(Roles = "Comprador")]
        public ActionResult UsuarioRegularizacion()
        {
            var monto = db.Compras.Where(x => x.Usuarios.NombreUsuario == User.Identity.Name && x.IdEstado == 1).OrderByDescending(x => x.FechaHora).Take(2).Sum(a => a.Importe);
            ViewBag.Monto = monto.ToString();
            return View();
        
        }

        [HttpPost]
        public ActionResult UsuarioRegularizacion(string tarjeta)
        {
            Usuarios us = db.Usuarios.Where(u => u.NombreUsuario == User.Identity.Name).SingleOrDefault();
            us.IsActivo = true;
            us.CantidadCancelaciones = 0;
            if (tarjeta != "" && tarjeta != null)
            {
                Tarjetas tar = db.Tarjetas.Where(t => t.NroTarjeta == tarjeta).SingleOrDefault();
                tar.FechaAlta = DateTime.Now;
            }
            db.SaveChanges();
            return RedirectToAction("Index","Home");

        }
        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        //
        // GET: /Usuario/Delete/5
    }
}