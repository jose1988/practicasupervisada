﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcEntradas.Entity;
using MvcEntradas.Models;

namespace MvcEntradas.Controllers
{ 
    public class PerfilController : Controller
    {
        private VentaEntradasEntities db = new VentaEntradasEntities();

        //
        // GET: /Perfil/
         [MyAuthorizeAttribute(Roles = "Administrador")]
        public ViewResult Index()
        {
            return View(db.Perfiles.ToList());
        }

        //
        // GET: /Perfil/Details/5

         [MyAuthorizeAttribute(Roles = "Administrador")]
        public ViewResult Details(long id)
        {
            Perfiles perfiles = db.Perfiles.Single(p => p.IdPerfil == id);
            return View(perfiles);
        }

        //
        // GET: /Perfil/Create
         [MyAuthorizeAttribute(Roles = "Administrador")]
        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /Perfil/Create

        [HttpPost]
        [MyAuthorizeAttribute(Roles = "Administrador")]
        public ActionResult Create(Perfiles perfiles)
        {
            if (ModelState.IsValid)
            {
                db.Perfiles.AddObject(perfiles);
                db.SaveChanges();
                return RedirectToAction("Index");  
            }

            return View(perfiles);
        }
        
        //
        // GET: /Perfil/Edit/5
         [MyAuthorizeAttribute(Roles = "Administrador")]
        public ActionResult Edit(long id)
        {
            Perfiles perfiles = db.Perfiles.Single(p => p.IdPerfil == id);
            return View(perfiles);
        }

        //
        // POST: /Perfil/Edit/5

        [HttpPost]
        [MyAuthorizeAttribute(Roles = "Administrador")]
        public ActionResult Edit(Perfiles perfiles)
        {
            if (ModelState.IsValid)
            {
                db.Perfiles.Attach(perfiles);
                db.ObjectStateManager.ChangeObjectState(perfiles, EntityState.Modified);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(perfiles);
        }

        //
        // GET: /Perfil/Delete/5
         [MyAuthorizeAttribute(Roles = "Administrador")]
        public ActionResult Delete(long id)
        {
            Perfiles perfiles = db.Perfiles.Single(p => p.IdPerfil == id);
            return View(perfiles);
        }

        //
        // POST: /Perfil/Delete/5

        [HttpPost, ActionName("Delete")]
        [MyAuthorizeAttribute(Roles = "Administrador")]
        public ActionResult DeleteConfirmed(long id)
        {            
            Perfiles perfiles = db.Perfiles.Single(p => p.IdPerfil == id);
            db.Perfiles.DeleteObject(perfiles);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}