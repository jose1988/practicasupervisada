﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcEntradas.Entity;
using MvcEntradas.Models;

namespace MvcEntradas.Controllers
{
    public class ShowListadoController : Controller
    {
        private VentaEntradasEntities db = new VentaEntradasEntities();
        //
        // GET: /ShowListado/
        [MyAuthorizeAttribute(Roles = "Administrador,Organizador")]
        public ActionResult Index(long id)
        {
            //var shows = db.Shows.Include("Establecimientos").Include("Organizadores").Where(s => s.FechaHora >= DateTime.Now);
            
            var shows = db.Shows.Include("Establecimientos").Where(s => s.FechaHora >= DateTime.Now && s.IsCancelado == false);

            var usuario = db.Usuarios.Where(u => u.NombreUsuario == User.Identity.Name).First();
            if (usuario.IdPerfil == 2)
                shows = shows.Where(s => s.IdUsuario == usuario.IdUsuario);

            var idsShows = db.Shows.Select(s => s.IdShow).ToList();
            var compras = db.Compras.Where(c => idsShows.Contains(c.IdShow)).ToList();

            List<ShowDetallado> lista = new List<ShowDetallado>();
            foreach (var s in shows)
            {
                var showDetallado = new ShowDetallado
                {
                    Artista = s.Artista,
                    CapacidadTotal = s.Establecimientos.CapacidadTotal,
                    Descripcion = s.Descripcion,
                    Establecimiento = s.Establecimientos.Nombre,
                    FechaHora = s.FechaHora,
                    IdShow = s.IdShow,
                    IsAprobado = s.IsAprobado,
                    //Uso SUM(cc => cc.Cantidad) en vez de Count por necesito sumar la cantidad de todas las compras
                    // (eso es las entradas vendidas) en vez de contar las compras
                    EntradasVendidas = (int)compras.Where(c => c.IdShow == s.IdShow && (c.IdEstado == (long)Estados.Vendida || c.IdEstado == (long)Estados.VendidaPendienteDeEntrega)).Sum(cc => cc.Cantidad),
                    EntradasReservadas = (int)compras.Where(c => c.IdShow == s.IdShow && c.IdEstado == (long)Estados.Reservada).Sum(cc => cc.Cantidad),
                    EntradasDisponibles = (int)s.Establecimientos.CapacidadTotal - compras.Where(c => c.IdShow == s.IdShow && (c.IdEstado == (long)Estados.Vendida || c.IdEstado == (long)Estados.Reservada || c.IdEstado == (long)Estados.VendidaPendienteDeEntrega)).Sum(cc => cc.Cantidad)
                };
                lista.Add(showDetallado);
            }

            Session["listaShowDetallado"] = lista;
            return View(lista);
        }

        [HttpPost]
        [MyAuthorizeAttribute(Roles = "Administrador,Organizador")]
        public ActionResult Index(string artista, string est, string fecha)
        {
            IEnumerable<ShowDetallado> shows = (IEnumerable<ShowDetallado>)Session["listaShowDetallado"];
  
            if (artista != "")
                shows = shows.Where(s => s.Artista.ToLower().Contains(artista.ToLower()));
            if (est != "")
                shows = shows.Where(s => s.Establecimiento.ToLower().Contains(est.ToLower()));
            if (fecha != "")
            {
                var fe = new DateTime();
                if (DateTime.TryParse(fecha, out fe))
                {
                    var fec = fe.Date;
                    var fecHasta = fec.AddDays(1).AddSeconds(-1);
                    shows = shows.Where(s => s.FechaHora >= fec && s.FechaHora <= fecHasta);
                }
            }

            return View(shows);
        }

        public ActionResult Details(long id)
        {
            return RedirectToAction("Details", "Show", new { id = id });
        }

        //Para el organizador
        [MyAuthorizeAttribute(Roles = "Administrador,Organizador")]
        public ActionResult ShowsPasados(long id)
        {
            //var shows = db.Shows.Include("Establecimientos").Include("Organizadores").Where(s => s.FechaHora < DateTime.Now);
            var shows = db.Shows.Include("Establecimientos").Where(s => s.FechaHora < DateTime.Now && s.IdUsuario == id);

            var usuario = db.Usuarios.Where(u => u.NombreUsuario == User.Identity.Name).First();
            if (usuario.IdPerfil == 2)
                shows = shows.Where(s => s.IdUsuario == usuario.IdUsuario);

            var idsShows = db.Shows.Select(s => s.IdShow).ToList();
            var compras = db.Compras.Where(c => idsShows.Contains(c.IdShow)).ToList();

            List<ShowDetallado> lista = new List<ShowDetallado>();
            foreach (var s in shows)
            {
                var showDetallado = new ShowDetallado
                {
                    Artista = s.Artista,
                    CapacidadTotal = s.Establecimientos.CapacidadTotal,
                    Descripcion = s.Descripcion,
                    Establecimiento = s.Establecimientos.Nombre,
                    FechaHora = s.FechaHora,
                    IdShow = s.IdShow,
                    IsAprobado = s.IsAprobado,
                    EntradasVendidas = (int)compras.Where(c => c.IdShow == s.IdShow && c.IdEstado == (long)Estados.Vendida).Sum(cc => cc.Cantidad),
                    EntradasNoVendidas = (int)s.Establecimientos.CapacidadTotal - (compras.Where(c => c.IdShow == s.IdShow && c.IdEstado == (long)Estados.Vendida).Sum(cc => cc.Cantidad))
                };
                lista.Add(showDetallado);
            }

            Session["listaShowPasado"] = lista;
            return View(lista);
        }

        [HttpPost]
        [MyAuthorizeAttribute(Roles = "Administrador,Organizador")]
        public ActionResult ShowsPasados(string artista, string est, string fecha)
        {
            IEnumerable<ShowDetallado> shows = (IEnumerable<ShowDetallado>)Session["listaShowPasado"];

            if (artista != "")
                shows = shows.Where(s => s.Artista.ToLower().Contains(artista.ToLower()));
            if (est != "")
                shows = shows.Where(s => s.Establecimiento.ToLower().Contains(est.ToLower()));
            if (fecha != "")
            {
                var fe = new DateTime();
                if (DateTime.TryParse(fecha, out fe))
                {
                    var fec = fe.Date;
                    var fecHasta = fec.AddDays(1).AddSeconds(-1);
                    shows = shows.Where(s => s.FechaHora >= fec && s.FechaHora <= fecHasta);
                }
            }

            return View(shows);
        }
    }
}
