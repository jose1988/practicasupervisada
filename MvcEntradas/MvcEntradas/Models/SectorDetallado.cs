﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcEntradas.Models
{
    public class SectorDetallado
    {
        public long IdSector { get; set; }

        public string Descripcion { get; set; }
    }
}