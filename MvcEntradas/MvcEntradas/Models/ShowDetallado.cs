﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcEntradas.Models
{
    public class ShowDetallado
    {
        public long IdShow { get; set; }

        public string Establecimiento { get; set; }

        public string Artista { get; set; }

        public string Descripcion { get; set; }

        public DateTime FechaHora { get; set; }

        public bool IsAprobado { get; set; }

        public long CapacidadTotal { get; set; }
        
        public int EntradasVendidas { get; set; }

        public int EntradasDisponibles { get; set; }

        public int EntradasReservadas { get; set; }

        public int EntradasNoVendidas { get; set; }
    }
}