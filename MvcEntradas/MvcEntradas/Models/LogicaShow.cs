﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MvcEntradas.Entity;
using MvcEntradas.Controllers;

namespace MvcEntradas.Models
{
    public class LogicaShow
    {
        private static VentaEntradasEntities db = new VentaEntradasEntities();

        public static bool validarDisponibilidad(Shows show)
        {
            bool disp = true;
            var compras = db.Compras.Where(c => c.IdShow == show.IdShow && c.IdEstado != 1);
            int totalVendido = 0;
            foreach (var c in compras)
            {
                totalVendido += c.Cantidad;
            }

            if (totalVendido >= show.Establecimientos.CapacidadTotal)
                disp = false;
            return disp;
        }

        public static int validarDisponibilidadSector(long IdShow, Sectores sec, int cant)
        {
            int disp = 1;
            Shows show = db.Shows.SingleOrDefault(s => s.IdShow == IdShow);
            var compras = db.Compras.Where(c => c.IdShow == show.IdShow && c.IdEstado != 1 && c.IdSector == sec.IdSector);
            if (compras.ToList().Count == 0)
            {
                var sector = db.Sectores.Where(s => s.IdSector == sec.IdSector).FirstOrDefault();
                if(sector != null && sector.capacidad >= cant)
                    return disp;
            }
            int totalVendido = 0;
            foreach (var c in compras)
            {
                totalVendido += c.Cantidad;
            }
            if (totalVendido >= sec.capacidad)
                disp = -1;
            if (totalVendido < sec.capacidad)
            {
                int restantes = sec.capacidad - totalVendido;
                if (cant > restantes)
                    disp = restantes;
            }
            return disp;
        }
    }
}