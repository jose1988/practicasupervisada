﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using MvcEntradas.Entity;
using System.Text;

namespace MvcEntradas.Models
{
    public class MailSender
    {
        public void EnviarCorreo(MailMessage mmsg, string body)
        {
            /*-------------------------MENSAJE DE CORREO----------------------*/

            //Nota: La propiedad To es una colección que permite enviar el mensaje a más de un destinatario

            mmsg.SubjectEncoding = System.Text.Encoding.UTF8;

         
            //Cuerpo del Mensaje
            mmsg.Body = body;
            mmsg.BodyEncoding = System.Text.Encoding.UTF8;
            mmsg.IsBodyHtml = true; //Si no queremos que se envíe como HTML

            //Correo electronico desde la que enviamos el mensaje

            mmsg.From = new System.Net.Mail.MailAddress("altaentrada@gmail.com");


            /*-------------------------CLIENTE DE CORREO----------------------*/

            //Creamos un objeto de cliente de correo
            SmtpClient cliente = new System.Net.Mail.SmtpClient();

            //Hay que crear las credenciales del correo emisor
            cliente.Credentials =
                new System.Net.NetworkCredential("altaentrada@gmail.com", "altaalta");

            //Lo siguiente es obligatorio si enviamos el mensaje desde Gmail

            cliente.Port = 587;
            cliente.EnableSsl = true;


            cliente.Host = "smtp.gmail.com"; 
            //Para Gmail "smtp.gmail.com";
            //Para Outlook (hotmail) "smtp-mail.outlook.com"; 


            /*-------------------------ENVIO DE CORREO----------------------*/

            try
            {
                //Enviamos el mensaje      
                cliente.Send(mmsg);
            }
            catch (SmtpException ex)
            {
                //Aquí gestionamos los errores al intentar enviar el correo
            }
        }

        public string cuerpoMailCancelacionShow(Shows show)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<h3>Atención: Cancelacion de Show</h3>");
            sb.AppendLine("<h4> Se ha cancelado el show del artista: " + show.Artista);
            sb.AppendLine("<br/>De la fecha: " + show.FechaHora +"</h4>");
            sb.AppendLine("<h4>Disculpe las molestias.</h4>");
            sb.AppendLine("<h4>Está a su disposición el acceso a devolución de importe de compra en punto de venta de Corrientes 2454, Bs As. </h4>");
            sb.AppendLine("<footer> Todo los derechos reservados. AltaEntrada 2014</footer>");

            return sb.ToString();
        }

        public string cuerpoMailEnvio(Compras compra)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<h3>Atención: Se ha realizado un envio</h3>");
            sb.AppendLine("<h4> Se ha realizado un envio de su compra de entradas del show: " + compra.Shows.Artista);
            sb.AppendLine("<br/>Fecha de envio: " + compra.FechaEnvio);
            sb.AppendLine("<br/>Forma de pago: " + compra.FormaDePago + "</h4>");
            sb.AppendLine("<h4>Por favor ingrese a su cuenta de AltaEntrada y edite su compra para elegir una nueva fecha de envío.</h4>");
            sb.AppendLine("<h4>Recuerde que solo cuenta con una oportunidad más de envío y debe abonar doble envío. </h4>");
            if(compra.FormaDePago.ToLower() == "efectivo")
                sb.AppendLine("<h4>Al tratarse de compra en efectivo luego del siguiente envio fallido su compra será cancelada.</h4>");
            else
                sb.AppendLine("<h4>Al tratarse de compra con tarjeta luego del siguiente envio fallido su compra quedara para retiro personal en punto de venta o boletería el mismo día del show.</h4>");
            sb.AppendLine("<h4>Atentamente. </h4>");
            sb.AppendLine("<footer> Todo los derechos reservados. AltaEntrada 2014</footer>");

            return sb.ToString();
        }

        public string cuerpoMailCompraExitosa(Compras compra)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<h3>Felicitaciones: Su compra se ha realizado con éxito</h3>");
            sb.AppendLine("<h4 style='font-weight: bold'>Detalles de compra</h4>");
            sb.AppendLine("<h4>Show: " + compra.Shows.Artista);
            sb.AppendLine("<br/>Fecha: " + compra.Shows.FechaHora);
            sb.AppendLine("<br/>Lugar: " + compra.Shows.Establecimientos.Nombre);
            sb.AppendLine("<br/>Forma de pago: " + compra.FormaDePago);
            if (compra.FormaDePago.ToLower() != "efectivo")
                sb.AppendLine("<br/>Tarjeta Nro: " + compra.Tarjeta);
            sb.AppendLine("<br/>Importe: " + compra.Importe + "</h4>");
            sb.AppendLine("<h4>¡Muchas Gracias por su compra!.</h4>");
            sb.AppendLine("<h4>Que disfute el show. </h4>");
            sb.AppendLine("<footer> Todo los derechos reservados. AltaEntrada 2014</footer>");

            return sb.ToString();
        }
    }
}