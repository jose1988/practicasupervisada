﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.master" Inherits="System.Web.Mvc.ViewPage<MvcEntradas.Entity.Sectores>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Details
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Detalles</h2>

<fieldset>
    <legend>Sectores</legend>

    <div class="display-label">Descripcion</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Descripcion) %>
    </div>
</fieldset>
<p>

    <%: Html.ActionLink("Editar", "Edit", new { id=Model.IdSector }) %> |
    <%: Html.ActionLink("Volver", "Index") %>
</p>

</asp:Content>
