﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<MvcEntradas.Entity.Tarjetas>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Elegir Tarjeta de Credito
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Elegir Tarjeta de Credito</h2>

 <div class="editor-field">
            <%: Html.DropDownList("ListaTar")%>
            <input id="sel" type="button" value="Seleccionar"/>
        </div>


<% using (Html.BeginForm()) { %>
    <%: Html.ValidationSummary(true) %>
    <fieldset>
        <legend>Nueva Tarjeta</legend>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.NroTarjeta) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.NroTarjeta)%>
            <%: Html.ValidationMessageFor(model => model.NroTarjeta)%>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.FechaVencimiento) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.FechaVencimiento)%>
            <%: Html.ValidationMessageFor(model => model.FechaVencimiento)%>
        </div>
        <p>
            <input type="submit" value="Guardar" />
        </p>
    </fieldset>
<% } %>

<script type="text/javascript" src="../../Scripts/tar.js">

</script>
</asp:Content>
