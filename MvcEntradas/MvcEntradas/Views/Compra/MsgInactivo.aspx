﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Inactivo
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Inactivo</h2>

<h3> Usted ha llegado a las 2 cancelaciones de compra por lo que se encuentra inhabilitado como comprador</h3>
<h3> Para volver a estar habilitado ingrese a <a href="../Usuario/UsuarioRegularizacion"> Regularizacion</a></h3>
<%--<%Html.ActionLink() %>--%>

</asp:Content>
