﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<MvcEntradas.Entity.Compras>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Cambiar Fecha Envio
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Elija una nueva fecha de envio <img src="../../Content/Images/envio.jpg" alt="Imagen no disponible" width="50px" height="50px"/>
</h2>

<% string msj = "";%>
    <%if (Session["msjErrorEnvio"] != null)
      { %>
    <% msj = Session["msjErrorEnvio"].ToString();%>
    <%} %>
<label id="errores" style="color:Red;font-weight:bold; font-size:larger"><%=msj %></label>

<h3>Recuerde que solo cuenta con 2 instancias de envio para sus compras</h3>


<% using (Html.BeginForm()) { %>
   <%: Html.ValidationSummary(true) %>

<fieldset>
 
<% if((string) ViewBag.formaPago == "efectivo"){ %>
<h3>Su compra será cancelada luego de 2 envios fallidos.</h3>
<%} %>
<% else{ %>
<h3>Luego de 2 envios fallidos, su compra quedara para retiro puntos de venta o boleteria el mismo dia del show.</h3>
<%} %>
     <div class="editor-label">
        <%: Html.LabelFor(model => model.ZonasEnvios.Precio, "Doble precio de envio: ") %>
        <% string l = ViewBag.precio;%> <%: Html.Label(l) %>
        
     </div>

    <br />
   <%: Html.HiddenFor(model => model.IdCompra) %>
   <%: Html.HiddenFor(model => model.IdShow) %>
    <%: Html.HiddenFor(model => model.IdDomicilio) %>
    <%: Html.HiddenFor(model => model.IdEstado) %>
    <%: Html.HiddenFor(model => model.IdSector) %>
    <%: Html.HiddenFor(model => model.IdUsuario) %>
    <%: Html.HiddenFor(model => model.IdZonaEnvio) %>
    <%: Html.HiddenFor(model => model.Importe) %>
    <%: Html.HiddenFor(model => model.IsEntregado) %>
    <%: Html.HiddenFor(model => model.IsEnvio) %>
    <%: Html.HiddenFor(model => model.Sectores) %>
    <%: Html.HiddenFor(model => model.Shows) %>
<%: Html.HiddenFor(model => model.Tarjeta) %>
<%: Html.HiddenFor(model => model.Cantidad) %>
<%: Html.HiddenFor(model => model.CantidadEnvios) %>
    <%: Html.HiddenFor(model => model.Domicilio) %>
    <%: Html.HiddenFor(model => model.FechaHora) %>
    <%: Html.HiddenFor(model => model.FormaDePago) %>
    <%: Html.HiddenFor(model => model.CambioFechaEnvio) %>
   
   <div class="editor-label">
            <%: Html.LabelFor(model => model.FechaEnvio, "Fecha de envio") %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.FechaEnvio) %>
            <%: Html.ValidationMessageFor(model => model.FechaEnvio)%>
        </div>
      <p>
         <input type="submit" value="Guardar" />
      </p>

 </fieldset>
<% } %>
  
</asp:Content>
