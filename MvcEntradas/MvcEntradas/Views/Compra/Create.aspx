﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<MvcEntradas.Entity.Compras>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Compra de Entradas
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

  <h2><span class="label label-default">Compra de Entradas</span></h2>

<script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>" type="text/javascript"></script>
<script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>" type="text/javascript"></script>
<script type="text/javascript" src="../../Scripts/FixDecimalBug.js">

    $.validator.methods.number = function (value, element) {
        value = floatValue(value);
        return this.optional(element) || !isNaN(value);
    }
    $.validator.methods.range = function (value, element, param) {
        value = floatValue(value);
        return this.optional(element) || (value >= param[0] && value <= param[1]);
    }

    function floatValue(value) {
        return parseFloat(value.replace(",", "."));
    }  
</script>
<% using (Html.BeginForm()) { %>
    <%: Html.ValidationSummary(true) %>
    <% string msj = "";%>
    <%if (Session["msjErrorCompra"] != null)
      { %>
    <% msj = Session["msjErrorCompra"].ToString();%>
    <%} %>
    <fieldset>
        <legend><%: Html.DisplayFor(model  => model.Shows.Artista)%></legend>
<%--
        <div class="editor-label">
            <%: Html.LabelFor(model => model.IdUsuario) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.IdUsuario) %>
            <%: Html.ValidationMessageFor(model => model.IdUsuario) %>
        </div>--%>

       <%-- <div class="editor-label">
            <%: Html.LabelFor(model => model.IdShow) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.IdShow) %>
            <%: Html.ValidationMessageFor(model => model.IdShow) %>
        </div>--%>
         <%: Html.Hidden("idDom") %>
         <%: Html.Hidden("idTar") %>
        <div class="editor-field">
             <img width="280" height="250" style="margin-left:200px;" src="<%:Url.Action("Preview","Show" , new {file=Model.Shows.Imagen})%>" /> 
        </div>
        <label id="errores" style="color:Red;font-weight:bold; font-size:larger"><%=msj %></label>
        <div class="display-field" style="border-style:groove">
        <%: Html.Label("Descripcion del show") %>
       
            <div class="display-field">
                 <%: Html.DisplayFor(model => model.Shows.Descripcion) %>
            </div>
             <div class="display-field">
                   <asp:Label ID="Label2" runat="server" Text="Fecha y Hora: "></asp:Label>
                 <%: Html.DisplayFor(model => model.Shows.FechaHora) %>
            </div>
              <div class="display-field">
                <asp:Label ID="Label1" runat="server" Text="Lugar: "></asp:Label>
                 <%: Html.DisplayFor(model => model.Shows.Establecimientos.Nombre) %>
            </div>
         </div>
        <div class="editor-label">
            <%: Html.LabelFor(model => model.FormaDePago,"Forma de Pago") %>
        </div>
 
       <div class="editor-field">
            <%: Html.DropDownList("FormaDePago", String.Empty)%>
            <%: Html.ValidationMessageFor(model => model.FormaDePago)%>
             <input id="selTar" type="button" value="Elegir Tarjeta" />
            <%: Html.Editor("Tarjeta")%>
             <%: Html.ValidationMessageFor(model => model.Tarjeta)%>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.IsEnvio, "Envio a domicilio") %>
            <input id="selDom" type="button" value="Elegir Domicilio" />
        </div>
         <div class="editor-field"">
         <%: Html.Editor("Domicilio")%>
          </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.IsEnvio) %>
            <%: Html.ValidationMessageFor(model => model.IsEnvio) %>
        </div>

         <div class="editor-label">
            <%: Html.LabelFor(model => model.FechaEnvio, "Fecha de envio") %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.FechaEnvio) %>
            <%: Html.ValidationMessageFor(model => model.FechaEnvio)%>
        </div>


      <%--  <div class="editor-label">
            <%: Html.LabelFor(model => model.CantidadEnvios) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.CantidadEnvios) %>
            <%: Html.ValidationMessageFor(model => model.CantidadEnvios) %>
        </div>--%>


        <div class="editor-label">
            <%: Html.LabelFor(model => model.Cantidad) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Cantidad) %>
            <%: Html.ValidationMessageFor(model => model.Cantidad) %>
        </div>

       <%-- <div class="editor-label">
            <%: Html.LabelFor(model => model.IdEstado) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.IdEstado) %>
            <%: Html.ValidationMessageFor(model => model.IdEstado) %>
        </div>--%>

        <%--<div class="editor-label">
            <%: Html.LabelFor(model => model.FechaHora) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.FechaHora) %>
            <%: Html.ValidationMessageFor(model => model.FechaHora) %>
        </div>--%>

       <%-- <div class="editor-label">
            <%: Html.LabelFor(model => model.Importe) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Importe) %>
            <%: Html.ValidationMessageFor(model => model.Importe) %>
        </div>--%>

        <%--<div class="editor-label">
            <%: Html.LabelFor(model => model.IsEntregado) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.IsEntregado) %>
            <%: Html.ValidationMessageFor(model => model.IsEntregado) %>
        </div>--%>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.IdSector,"Sector y Precio") %>
        </div>
     <%--   <div class="editor-field">
            <%: Html.EditorFor(model => model.Shows.Establecimientos.Sectores) %>
            <%: Html.ValidationMessageFor(model => model.Shows.Establecimientos.Sectores)%>
        </div>--%>
        <div class="editor-field">
            <%: Html.DropDownList("IdSector", String.Empty)%>
            <%: Html.ValidationMessageFor(model => model.Shows.Establecimientos.Sectores)%>
            
        </div>
       <div class="editor-label">
            <%: Html.LabelFor(model => model.IdSector,"Sector y Precio en Imagen") %>
        </div>
        <div>
            <%  var cantidad = Model.Shows.Establecimientos.Sectores.Count;
                Session["EstablecimientoId"] = Model.Shows.Establecimientos.IdEstablecimiento;
                if (cantidad == 1)
                {
                    %>
                    <img width="280" height="250" style="margin-left:200px;" alt="Imagen no disponible" src="../../Content/Images/Imagen Sector2.bmp" usemap="#mapa"/> 
                    <map name="mapa" id="mapa">
                        <area id="area1" shape="rect" coords="0,0,400,400" style="cursor: pointer;" alt="ACCESO"/>
                    </map>
                <%}
               if (cantidad == 2)
               {
                   %>
                    <img width="280" height="250" style="margin-left:200px;" alt="Imagen no disponible" src="../../Content/Images/Imagen Sector2.bmp" usemap="#mapa"/> 
                    
                    <map name="mapa" id="map1">
                        <area id="area1" shape="rect" coords="0,0,150,400" style="cursor: pointer;" alt="ACCESO"/>
                        <area id="area2" shape="rect" coords="150,0,400,400" style="cursor: pointer;" alt="ACCESO"/>
                    </map>
                <%}
               if (cantidad == 3)
               {
                   %>
                    <img width="280" height="250" style="margin-left:200px;" alt="Imagen no disponible" src="../../Content/Images/Imagen Sector2.bmp" usemap="#mapa"/> 
                    
                    <map name="mapa" id="map2">
                        <area id="area1" shape="rect" coords="0,0,100,400" style="cursor: pointer;" alt="ACCESO"/>
                        <area id="area2" shape="rect" coords="100,0,200,400" style="cursor: pointer;" alt="ACCESO"/>
                        <area id="area3" shape="rect" coords="200,0,300,400" style="cursor: pointer;" alt="ACCESO"/>
                    </map>
                <%}
               if (cantidad == 4)
               {
                   %>
                    <img width="280" height="250" style="margin-left:200px;" alt="Imagen no disponible" src="../../Content/Images/Imagen Sector2.bmp" usemap="#mapa"/> 
                    
                    <map name="mapa" id="map3">
                        <area id="area1" shape="rect" coords="0,0,100,150" style="cursor: pointer;" alt="ACCESO"/>
                        <area id="area2" shape="rect" coords="100,0,200,150" style="cursor: pointer;" alt="ACCESO"/>
                        <area id="area3" shape="rect" coords="200,0,300,150" style="cursor: pointer;" alt="ACCESO"/>
                        <area id="area4" shape="rect" coords="0,150,300,300" style="cursor: pointer;" alt="ACCESO"/>
                    </map>
                <%}
               if (cantidad == 5)
               {
                   %>
                    <img width="280" height="250" style="margin-left:200px;" alt="Imagen no disponible" src="../../Content/Images/Imagen Sector2.bmp" usemap="#mapa"/> 
                    
                    <map name="mapa" id="map4">
                        <area id="area1" shape="rect" coords="0,0,100,150" style="cursor: pointer;" alt="ACCESO"/>
                        <area id="area2" shape="rect" coords="100,0,200,150" style="cursor: pointer;" alt="ACCESO"/>
                        <area id="area3" shape="rect" coords="200,0,300,150" style="cursor: pointer;" alt="ACCESO"/>
                        <area id="area4" shape="rect" coords="0,150,150,300" style="cursor: pointer;" alt="ACCESO"/>
                        <area id="area5" shape="rect" coords="150,150,300,300" style="cursor: pointer;" alt="ACCESO"/>
                    </map>
                <%}%>
        </div>
        <div class="editor-label">
            <label>Importe</label>
        </div>
        <div class="editor-field">
            <input id="importe" type="text" readonly = "readonly" />
        </div>
        
        <br />
        <p>
            <input type="submit" value="Comprar" style ='width: 120px;height: 50px;-webkit-border-radius: 10px;
    -moz-border-radius: 10px;border-radius: 10px;font-weight:bold;'/>
        </p>
    </fieldset>
<% } %>

<div>
    <%: Html.ActionLink("Volver", "ListadoPorUsuario") %>
</div>

<script type="text/javascript" src = "../../Scripts/compra.js">
</script>
</asp:Content>
