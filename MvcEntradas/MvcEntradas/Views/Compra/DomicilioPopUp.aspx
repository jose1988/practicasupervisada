﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<MvcEntradas.Entity.Domicilio>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Elegir Domicilio
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Elegir Domicilio de Envío</h2>

       <div class="editor-field">
            <%: Html.DropDownList("ListaDom", String.Empty)%>
            <input id="sel" type="button" value="Seleccionar"/>
        </div>
<%--
<table>
    <tr>
        <th>
            Calle
        </th>
        <th>
            Número
        </th>
        <th>
            Localidad
        </th>
        <th>
            CP
        </th>
        <th>
            Piso
        </th>
        <th>
            Dpto
        </th>
        <th></th>
    </tr>

       <% long index = 0;%>
<% foreach (var item in ViewBag.ListaDom)
   { %>
 
    <tr> 
        <td>
            <% string nom = "id" + index%>
            <% string id = item.IdDomicilio%>
            <%: Html.HiddenFor(modelItem => id)%>
            <% string dire = item.Calle;%>
            <%: Html.DisplayFor(modelItem => dire) %>
        </td>
      
        <td>
            <% long n = item.Numero;%>
            <%: Html.DisplayFor(modelItem => n) %>
        </td>
        <td>
            <% string loc = item.Localidad;%>
            <%: Html.DisplayFor(modelItem => loc) %>
        </td>
        <td>
            <% int cod = item.CodigoPostal;%>
            <%: Html.DisplayFor(modelItem => cod) %>
        </td>
        <td>
            <% var p = (item.Piso != null ? item.Piso : String.Empty);%>
            <%: Html.DisplayFor(modelItem => p) %>
        </td>

        <td>
            <% string d = (item.Departamento != null ? item.Departamento : String.Empty);%>
            <%: Html.DisplayFor(modelItem => d) %>
        </td>
        <td>
            <% string nom = "elegir" + index.ToString(); %>
            <input id="elegir" name=<%= nom %> type="button" value="Elegir"/>
           <%-- <%: Html.ActionLink("Elegir", "Elegir", new { id=item.IdDomicilio }) %>--%>
<%--        </td>
    </tr>
     <% index++;%>
<% } %>

</table>
--%>
<% using (Html.BeginForm()) { %>
    <%: Html.ValidationSummary(true) %>
    <fieldset>
        <legend>Nuevo Domicilio</legend>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Calle) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Calle)%>
            <%: Html.ValidationMessageFor(model => model.Calle)%>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Numero) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Numero)%>
            <%: Html.ValidationMessageFor(model => model.Numero)%>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Localidad) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Localidad)%>
            <%: Html.ValidationMessageFor(model => model.Localidad)%>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.CodigoPostal) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.CodigoPostal)%>
            <%: Html.ValidationMessageFor(model => model.CodigoPostal)%>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Piso) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Piso)%>
            <%: Html.ValidationMessageFor(model => model.Piso)%>
        </div>

         <div class="editor-label">
            <%: Html.LabelFor(model => model.Departamento) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Departamento)%>
            <%: Html.ValidationMessageFor(model => model.Departamento)%>
        </div>
        <h5>Recuerde que el envio es solo para Capital, La Plata, zona sur, oeste y norte de GBA.
        No hay otras zonas de envio disponibles. Si vive en otra zona debe retirar en punto de venta.</h5>
        <div class="editor-label">
            <%: Html.LabelFor(model => model.IdZonaEnvio, "Zona de Envio") %>
        </div>
       <div class="editor-field">
            <%: Html.DropDownList("IdZonaEnvio", String.Empty)%>
            <%: Html.ValidationMessageFor(model => model.IdZonaEnvio)%>
        </div>

        <p>
            <input type="submit" value="Guardar" />
        </p>
    </fieldset>
<% } %>

<script type="text/javascript" src="../../Scripts/dom.js"></script>
</asp:Content>
