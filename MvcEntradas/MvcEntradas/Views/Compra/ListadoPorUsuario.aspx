﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<MvcEntradas.Entity.Compras>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    ListadoPorUsuario
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2><span class="label label-default">Mis Compras</span>
 <img src="../../Content/Images/ticket.jpg" alt="Imagen no disponible" width="50px" height="50px"/>
</h2>

<% using (Html.BeginForm()) { %>
 <div class="editor-field">
    <label style="font-weight:bolder">Filtros:</label>
    <br />
    <table>
    <tr>
    <td><label>Por show</label></td>
    <td> <input id="fshow" type="text" name="fshow" /></td>
    <td>
    <label>Por Estado</label></td>
    <td>
    <select id="estados" name ="estados">
        <option value = ""></option>
        <option value = "0">Reservada</option>
        <option value = "1">Cancelada</option>
        <option value = "2">Comprada</option>
        <option value = "3">Comprada Pendiente de Entrega</option>
    </select>
    </td>
    </tr>
    <tr>
    <td>
    <label>Por fecha Show</label></td>
      <td><input id="fecha" type="text" name="fecha" />
    </td>
    <td> 
    <label>Por forma de Pago</label></td>
    <td>
    <select id="formasPago" name ="formasPago">
        <option value = ""></option>
        <option value = "Efectivo">Efectivo</option>
        <option value = "Tarjeta de credito">Tarjeta de credito</option>
    </select>
    </td>
    </tr>
    </table>

     <input type="hidden" id="estado" name="estado" />
     <input type="hidden" id="pago" name="pago" />
     <input type="submit" value="Filtrar" style ='-webkit-border-radius: 5px;
    -moz-border-radius: 5px;border-radius: 5px;'/>
 </div>
 <% } %>


<table>
    <tr>
        <th>
            Show
        </th>
         <th>
            Fecha Show
        </th>
        <th>
            Forma De Pago
        </th>
        <th>
            Tiene Envio
        </th>
        <th>
            Cantidad
        </th>
        <th>
            Sector
        </th>
        <th>
            Fecha y Hora De compra
        </th>
        <th>
            Importe
        </th>
        <th>
            Estado
        </th>
        <th>
            Fecha de Envio
        </th>
        <th>
            Cancelar
        </th>
        <th>
            Elegir Fecha
        </th>
        <th></th>
    </tr>

<% foreach (var item in Model) { %>
    <tr>
        <td>
            <%: Html.DisplayFor(modelItem => item.Shows.Artista) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Shows.FechaHora) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.FormaDePago) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.IsEnvio) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Cantidad) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Sectores.Descripcion) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.FechaHora) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Importe) %>
        </td>
        <td>
             <% string est =""; %>
           <%-- <% var e = (Estados)item.IdEstado; %>--%>
            <% if (item.IdEstado == 0) est = "Reservada"; %>
            <% if (item.IdEstado == 1) est = "Cancelada"; %>
            <% if (item.IdEstado == 2) est = "Comprada"; %>
            <% if (item.IdEstado == 3) est = "Pendiente de Entrega"; %>
           <%: Html.DisplayFor(modelItem => est) %>
        </td>
        <td>
            <%if(item.IsEnvio) {%>
            <%: Html.DisplayFor(modelItem => item.FechaEnvio) %>
            <%} %>
        </td>
         <td>
         <% if ((item.IdEstado == 0 || item.IdEstado == 3) && item.FormaDePago == "Efectivo")
         { %>
            <%: Html.ActionLink("Cancelar", "CancelarCompra", new { id = item.IdCompra })%>
         <%} %>
        </td>
         <td>
         <% if (item.IsEnvio && item.CantidadEnvios == 1 && item.CambioFechaEnvio != true)
         { %>
            <%: Html.ActionLink("Elegir nueva fecha envio", "CambiarFechaEnvio", new { id = item.IdCompra })%>
         <%} %>
        </td>
        
     <%--   <td>
        <form runat="server">
            <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" Height="34px" Width="87px" />
        </form>
        </td>--%>
       <%-- <td>
           
            <%: Html.ActionLink("Details", "Details", new { id=item.IdCompra }) %> |
            <%: Html.ActionLink("Delete", "Delete", new { id=item.IdCompra }) %>
        </td>--%>
    </tr>
<% } %>

</table>

<script type="text/javascript">

    $('#estados').change(function () {
        var sel = $(this).val();
        $('#estado').val(sel);

    });

    $('#formasPago').change(function () {
        var sel = $(this).val();
        $('#pago').val(sel);

    });
</script>

</asp:Content>
