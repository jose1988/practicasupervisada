﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.master" Inherits="System.Web.Mvc.ViewPage<MvcEntradas.Entity.Compras>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Index
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Compra de Entradas
    <img src="../../Content/Images/ticket.jpg" alt="Imagen no disponible" width="50px" height="50px"/>
    </h2>
<h3> Su Compra ha sido exitosa </h3>


<div class="display-field" style="border-style:groove">
        <%: Html.Label("Descripcion del show") %>
</div>

<%--         Detalles del show
         Imagen del show
         Listado de los precios segun sector
         * 
         * 
         Text de cantidad de entradas
         combo de sector
         combo de forma de pago
         check de envio

         importe total de compra
         text de --%>
         

<div class="display-field">
     <%: Html.DisplayFor(model => model.Shows.Artista) %>
</div>

<div class="display-field">
     <%: Html.DisplayFor(model => model.Shows.Descripcion) %>
</div>

<div class="display-field">
     <%: Html.DisplayFor(model => model.Shows.Establecimientos.Nombre) %>
</div>

<div class="display-field">
     <%: Html.DisplayFor(model => model.Shows.FechaHora) %>
</div>

<div class="display-field" >
        <%: Html.Label("Envio a domicilio") %>
</div>

<div class="display-field">
     <%: Html.DisplayFor(model => model.IsEnvio) %>
</div>

</asp:Content>
