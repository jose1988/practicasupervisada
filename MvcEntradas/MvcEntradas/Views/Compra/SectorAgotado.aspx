﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Sector Agotado
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Sector Agotado</h2>

<% using (Html.BeginForm()) { %>
<% string d = (string)ViewBag.disponible;%>
<% string l = (string)ViewBag.id;%>
<% string url = "../Compra/Create?IdShow="+ l;%>

<% if(d != "-1") { %>
<h3>El sector elegido no cuenta con la cantidad de entradas solicitadas</h3>
<h3> Se cuenta solo con  <%: Html.Label(d) %> entradas disponibles</h3>
<h3><a href= <%=url %>>Volver a la compra</a></h3>
 <% } %>
 <%else{ %>
 <h3>El sector elegido se encuentra agotado</h3>
<h3><a href= <%=url %>>Volver a la compra</a></h3>
  <% } %>
 <% } %>
</asp:Content>
