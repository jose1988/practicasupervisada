﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<MvcEntradas.Entity.Compras>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Vendidas
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


<h2>Entradas Vendidas
<img src="../../Content/Images/vendido.jpg" alt="Imagen no disponible" width="100px" height="50px"/>
</h2>

<% using (Html.BeginForm()) { %>
 <div class="editor-field">
    <label style="font-weight:bolder">Filtros:</label>
    <br />
    <table>
    <tr>
    <td><label>Por show</label></td>
    <td> <%: Html.DropDownList("shows", String.Empty)%>
    </td>
    <td>
     <label>Por Fecha Compra</label></td>
    <td> <input id="fecha" type="text" name="fecha" />
    </td>
    </tr>
    <tr>
    <td>
    <label>Por Apellido</label></td>
      <td><input id="ape" type="text" name="ape" />
    </td>
    <td> 
    <label>Por Dni</label></td>
      <td><input id="dni" type="text" name="dni" />
    </td>
    </tr>
    </table>

     <input type="hidden" id="idShow" name="idShow" />
     <input type="submit" value="Filtrar" style ='-webkit-border-radius: 5px;
    -moz-border-radius: 5px;border-radius: 5px;'/>
 </div>
 <% } %>
 <br />
<table>
    <tr>
        <th>
            Show
        </th>
         <th>
            Fecha Show
        </th>
        <th>
            Forma De Pago
        </th>
       <th>
            Apellido y Nombre
        </th>
        <th>
           DNI
        </th>
        <th>
            Tiene Envio
        </th>
        <th>
            Fecha Envio
        </th>
        <th>
            Cantidad Entradas
        </th>
        <th>
            Sector
        </th>
        <th>
            Fecha y Hora De compra
        </th>
        <th>
            Importe
        </th>
        <th>
            Estado
        </th>
    </tr>

<% foreach (var item in Model) { %>
    <tr>
        <td>
            <%: Html.DisplayFor(modelItem => item.Shows.Artista) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Shows.FechaHora) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.FormaDePago) %>
        </td>
        <td>
            <%string n = item.Usuarios.Apellido + " " + item.Usuarios.Nombre; %>
            <%: Html.DisplayFor(modelItem => n) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Usuarios.DNI) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.IsEnvio) %>
        </td>
        <td>
        <%if (item.IsEnvio)
          { %>
            <%: Html.DisplayFor(modelItem => item.FechaEnvio)%>
            <%} %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Cantidad) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Sectores.Descripcion) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.FechaHora) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Importe) %>
        </td>
        <td>
             <% string est =""; %>
           <%-- <% var e = (Estados)item.IdEstado; %>--%>
            <% if (item.IdEstado == 0) est = "Reservada"; %>
            <% if (item.IdEstado == 1) est = "Cancelada"; %>
            <% if (item.IdEstado == 2) est = "Vendida"; %>
            <% if (item.IdEstado == 3) est = "Pendiente de Entrega"; %>
           <%: Html.DisplayFor(modelItem => est) %>
        </td>
    </tr>
<% } %>

</table>

<script type="text/javascript">

    $('#shows').change(function () {
        var sel = $(this).val();
        $('#idShow').val(sel);

    });
</script>
</asp:Content>
