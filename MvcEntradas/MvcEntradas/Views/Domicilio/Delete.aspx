﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.master" Inherits="System.Web.Mvc.ViewPage<MvcEntradas.Entity.Domicilio>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Delete
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Delete</h2>

<h3>Are you sure you want to delete this?</h3>
<fieldset>
    <legend>Domicilio</legend>

    <div class="display-label">Calle</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Calle) %>
    </div>

    <div class="display-label">Numero</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Numero) %>
    </div>

    <div class="display-label">Localidad</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Localidad) %>
    </div>

    <div class="display-label">ZonasEnvios</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.ZonasEnvios.Nombre) %>
    </div>

    <div class="display-label">Usuarios</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Usuarios.NombreUsuario) %>
    </div>

    <div class="display-label">CodigoPostal</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.CodigoPostal) %>
    </div>

    <div class="display-label">Piso</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Piso) %>
    </div>

    <div class="display-label">Departamento</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Departamento) %>
    </div>
</fieldset>
<% using (Html.BeginForm()) { %>
    <p>
        <input type="submit" value="Delete" /> |
        <%: Html.ActionLink("Back to List", "Index") %>
    </p>
<% } %>

</asp:Content>
