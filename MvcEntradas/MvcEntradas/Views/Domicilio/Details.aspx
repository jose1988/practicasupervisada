﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.master" Inherits="System.Web.Mvc.ViewPage<MvcEntradas.Entity.Domicilio>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Details
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Details</h2>

<fieldset>
    <legend>Domicilio</legend>

    <div class="display-label">Calle</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Calle) %>
    </div>

    <div class="display-label">Numero</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Numero) %>
    </div>

    <div class="display-label">Localidad</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Localidad) %>
    </div>

    <div class="display-label">ZonasEnvios</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.ZonasEnvios.Nombre) %>
    </div>

    <div class="display-label">Usuarios</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Usuarios.NombreUsuario) %>
    </div>

    <div class="display-label">CodigoPostal</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.CodigoPostal) %>
    </div>

    <div class="display-label">Piso</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Piso) %>
    </div>

    <div class="display-label">Departamento</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Departamento) %>
    </div>
</fieldset>
<p>

    <%: Html.ActionLink("Edit", "Edit", new { id=Model.IdDomicilio }) %> |
    <%: Html.ActionLink("Back to List", "Index") %>
</p>

</asp:Content>
