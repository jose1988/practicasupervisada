﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.master" Inherits="System.Web.Mvc.ViewPage<MvcEntradas.Entity.Domicilio>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Create
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Create</h2>

<script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>" type="text/javascript"></script>
<script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>" type="text/javascript"></script>

<% using (Html.BeginForm()) { %>
    <%: Html.ValidationSummary(true) %>
    <fieldset>
        <legend>Domicilio</legend>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Calle) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Calle) %>
            <%: Html.ValidationMessageFor(model => model.Calle) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Numero) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Numero) %>
            <%: Html.ValidationMessageFor(model => model.Numero) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Localidad) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Localidad) %>
            <%: Html.ValidationMessageFor(model => model.Localidad) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.IdZonaEnvio, "ZonasEnvios") %>
        </div>
        <div class="editor-field">
            <%: Html.DropDownList("IdZonaEnvio", String.Empty) %>
            <%: Html.ValidationMessageFor(model => model.IdZonaEnvio) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.IdUsuario, "Usuarios") %>
        </div>
        <div class="editor-field">
            <%: Html.DropDownList("IdUsuario", String.Empty) %>
            <%: Html.ValidationMessageFor(model => model.IdUsuario) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.CodigoPostal) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.CodigoPostal) %>
            <%: Html.ValidationMessageFor(model => model.CodigoPostal) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Piso) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Piso) %>
            <%: Html.ValidationMessageFor(model => model.Piso) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Departamento) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Departamento) %>
            <%: Html.ValidationMessageFor(model => model.Departamento) %>
        </div>

        <p>
            <input type="submit" value="Create" />
        </p>
    </fieldset>
<% } %>

<div>
    <%: Html.ActionLink("Back to List", "Index") %>
</div>

</asp:Content>
