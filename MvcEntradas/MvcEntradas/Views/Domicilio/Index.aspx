﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<MvcEntradas.Entity.Domicilio>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Index
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Index</h2>

<p>
    <%: Html.ActionLink("Create New", "Create") %>
</p>
<table>
    <tr>
        <th>
            Calle
        </th>
        <th>
            Numero
        </th>
        <th>
            Localidad
        </th>
        <th>
            ZonasEnvios
        </th>
        <th>
            Usuarios
        </th>
        <th>
            CodigoPostal
        </th>
        <th>
            Piso
        </th>
        <th>
            Departamento
        </th>
        <th></th>
    </tr>

<% foreach (var item in Model) { %>
    <tr>
        <td>
            <%: Html.DisplayFor(modelItem => item.Calle) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Numero) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Localidad) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.ZonasEnvios.Nombre) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Usuarios.NombreUsuario) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.CodigoPostal) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Piso) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Departamento) %>
        </td>
        <td>
            <%: Html.ActionLink("Edit", "Edit", new { id=item.IdDomicilio }) %> |
            <%: Html.ActionLink("Details", "Details", new { id=item.IdDomicilio }) %> |
            <%: Html.ActionLink("Delete", "Delete", new { id=item.IdDomicilio }) %>
        </td>
    </tr>
<% } %>

</table>

</asp:Content>
