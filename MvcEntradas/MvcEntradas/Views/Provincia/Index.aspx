﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<MvcEntradas.Entity.Provincias>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Index
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Listado de provincias</h2>

<p>
    <%: Html.ActionLink("Nuevo", "Create") %>
</p>
<table>
    <tr>
        <th></th>
        <th>
            Descripcion
        </th>
    </tr>

<% foreach (var item in Model) { %>
    <tr>
        <td>
            <%: Html.ActionLink("Editar", "Edit", new { id=item.IdProvincia }) %> |
            <%: Html.ActionLink("Detalles", "Details", new { id=item.IdProvincia }) %> |
            <%: Html.ActionLink("Eliminar", "Delete", new { id=item.IdProvincia }) %>
        </td>
        <td>
            <%: item.Descripcion %>
        </td>
    </tr>  
<% } %>

</table>

</asp:Content>

