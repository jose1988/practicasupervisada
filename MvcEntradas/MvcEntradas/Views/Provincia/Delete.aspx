﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<MvcEntradas.Entity.Provincias>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Delete
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Eliminar</h2>

<h3>Esta seguro de eliminar esta provincia?</h3>
<fieldset>
    <legend>Provincias</legend>

    <div class="display-label">Descripcion</div>
    <div class="display-field"><%: Model.Descripcion %></div>
</fieldset>
<% using (Html.BeginForm()) { %>
    <p>
        <input type="submit" value="Eliminar" /> |
        <%: Html.ActionLink("Volver", "Index") %>
    </p>
<% } %>

</asp:Content>

