﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<MvcEntradas.Entity.Provincias>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Details
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Detalles</h2>

<fieldset>
    <legend>Provincias</legend>

    <div class="display-label">Descripcion</div>
    <div class="display-field"><%: Model.Descripcion %></div>
</fieldset>
<p>

    <%: Html.ActionLink("Editar", "Edit", new { id=Model.IdProvincia }) %> |
    <%: Html.ActionLink("Volver", "Index") %>
</p>

</asp:Content>

