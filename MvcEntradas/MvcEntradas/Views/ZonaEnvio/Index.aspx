﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<MvcEntradas.Entity.ZonasEnvios>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Index
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Listado de zonas de envios</h2>

<p>
    <%: Html.ActionLink("Nuevo", "Create")%>
</p>
<table>
    <tr>
        <th>
            Nombre
        </th>
        <th>
            Precio
        </th>
        <th></th>
    </tr>

<% foreach (var item in Model) { %>
    <tr>
        <td>
            <%: Html.DisplayFor(modelItem => item.Nombre) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Precio) %>
        </td>
        <td>
            <%: Html.ActionLink("Editar", "Edit", new { id=item.IdZona }) %> |
            <%: Html.ActionLink("Detalles", "Details", new { id=item.IdZona }) %> |
            <%: Html.ActionLink("Eliminar", "Delete", new { id=item.IdZona }) %>
        </td>
    </tr>
<% } %>

</table>

</asp:Content>
