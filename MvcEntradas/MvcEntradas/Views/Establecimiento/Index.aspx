﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<MvcEntradas.Entity.Establecimientos>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Index
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Listado de Establecimientos</h2>

<% using (Html.BeginForm()) { %>
 <div class="editor-field">
    <label style="font-weight:bolder">Filtros:</label>
    <br />
    <table>
    <tr>
    <td><label>Por Nombre</label></td>
    <td> <input id="nombre" type="text" name="nombre" /></td>
    <td>
    <label>Por Capacidad</label></td>
    <td> <input id="cap" type="text" name="cap" /></td>
    </tr>
    </table>

     <input type="submit" value="Filtrar" style ='-webkit-border-radius: 5px;
    -moz-border-radius: 5px;border-radius: 5px;'/>
 </div>
 <% } %>

<p>
    <%: Html.ActionLink("Nuevo", "Create") %>
</p>
<table>
    <tr>
        <th></th>
        <th>
            Nombre
        </th>
        <th>
            PrecioAlquiler
        </th>
        <th>
            CapacidadTotal
        </th>
    </tr>

<% foreach (var item in Model) { %>
    <tr>
        <td>
            <%: Html.ActionLink("Editar", "Edit", new { id=item.IdEstablecimiento }) %> |
            <%: Html.ActionLink("Detalles", "Details", new { id=item.IdEstablecimiento }) %> |
            <%: Html.ActionLink("Eliminar", "Delete", new { id=item.IdEstablecimiento }) %>
        </td>
        <td>
            <%: item.Nombre %>
        </td>
        <td>
            <%: String.Format("{0:F}", item.PrecioAlquiler) %>
        </td>
        <td>
            <%: item.CapacidadTotal %>
        </td>
    </tr>  
<% } %>

</table>

</asp:Content>

