﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<MvcEntradas.Entity.Establecimientos>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Edit
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Editar</h2>

<script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>" type="text/javascript"></script>
<script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>" type="text/javascript"></script>
<script type="text/javascript">
    $.validator.methods.number = function (value, element) {
        value = floatValue(value);
        return this.optional(element) || !isNaN(value);
    }
    $.validator.methods.range = function (value, element, param) {
        value = floatValue(value);
        return this.optional(element) || (value >= param[0] && value <= param[1]);
    }

    function floatValue(value) {
        return parseFloat(value.replace(",", "."));
    }  
</script>
<% using (Html.BeginForm()) { %>
    <%: Html.ValidationSummary(true) %>
    <% string msj = "";%>
     <%if (Session["msjErrorEstablecimiento"] != null)
      { %>
    <% msj = Session["msjErrorEstablecimiento"].ToString();%>
    <%} %>
    <fieldset>
        <legend>Establecimientos</legend>

        <%: Html.HiddenFor(model => model.IdEstablecimiento) %>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Nombre) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Nombre) %>
            <%: Html.ValidationMessageFor(model => model.Nombre) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.PrecioAlquiler, "Precio Alquiler") %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.PrecioAlquiler)%>
            <%: Html.ValidationMessageFor(model => model.PrecioAlquiler, "Precio alquiler") %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.CapacidadTotal, "Capacidad Total") %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.CapacidadTotal) %>
            <%: Html.ValidationMessageFor(model => model.CapacidadTotal, "Capacidad total") %>
        </div>
         <legend>Sectores</legend>
 
          <table id="miTabla"  border="1" >
          <thead>
          <tr>
            <th>
                Descripcion
            </th>
            <th>
                capacidad
            </th>
        
            <th>
                Precio
            </th>
            <th><button type="submit" name="operacion" data-val="false" value="agregar-sector">Agregar Sector</button></th>
        </tr>
          
          </thead>
       <tbody>
            
            <% var i = 0; %>
            <%
                 foreach (var item in Model.Sectores)
                {%>
                     <%:Html.Hidden("Sectores[" + i + "].IdSector", item.IdSector)%>
                     <%:Html.Hidden("Sectores[" + i + "].idEstablecimiento", item.idEstablecimiento)%>
                     <%:Html.Hidden("IdSector", item.IdSector)%>
                 <tr>
                <td>
                 <%:Html.TextBox("Sectores[" + i + "].Descripcion", item.Descripcion)%>
                </td>
                  <td>
                 <%:Html.TextBox("Sectores[" + i + "].capacidad", item.capacidad)%>
                </td>
                  <td>
                 <%:Html.TextBox("Sectores[" + i + "].Precio", item.Precio)%>
                </td>
                <td><button  type="submit" name="operacion" value="eliminar-detalle">Eliminar</button></td>
                  </tr>
            <%  i++; %>
                
            <% } %>
          
            </tbody>
       </table>

        <p>
            <input type="submit" value="Guardar" />
        </p>
    </fieldset>
    <label id="errores" style="color:Red;font-weight:bold; font-size:larger"><%=msj %></label>
<% } %>

<div>
    <%: Html.ActionLink("Volver", "Index") %>
</div>

</asp:Content>

