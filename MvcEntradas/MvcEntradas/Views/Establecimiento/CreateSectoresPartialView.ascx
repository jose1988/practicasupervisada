﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<MvcEntradas.Entity.Sectores>>" %>

<script src="<%: Url.Content("~/Scripts/jquery-1.5.1.min.js") %>" type="text/javascript"></script>
<script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>" type="text/javascript"></script>
<script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>" type="text/javascript"></script>



<script type="text/javascript">
    $(document).ready(function () {

        $("#btnAgregar").click(function () {
            $("#miTabla tbody tr:eq(0)").clone().removeClass('fila-base').appendTo("#miTabla tbody");
        });

        // Evento que selecciona la fila y la elimina 
        $(document).on("click", ".eliminar", function () {
            $(this).parents("tr").remove();
        });

    });    
</script>
<style type="text/css">
 
#miTabla{	border: solid 1px #333;	width: 300px; }
#miTabla tbody tr{ background: #999; }
.fila-base{ display: none; } /* fila base oculta */
.eliminar{ cursor: pointer; color: #000; }
input[type="text"]{ width: 80px; } /* ancho a los elementos input="text" */
 
</style>


<% using (Html.BeginForm()) { %>
    <%: Html.ValidationSummary(false) %>
    <fieldset>
        <legend>Sectores</legend>
 
          <table id="miTabla"  border="1" >
          <thead>
                 <tr>
            <th>
                Descripcion
            </th>
            <th>
                capacidad
            </th>
        
            <th>
                Precio
            </th>
            <th></th>
        </tr>
          
          </thead>
       <tbody>
            <tr class="fila-base" > 
            <%foreach(var item in Model) {%>
           
                <td><%--<input type="text"/>--%>
                 <%:Html.EditorFor(mo => item.Descripcion)%>
                </td>
                <td><input type="text"/></td>
                <td><input type="text"/></td>
                <% } %>
                <td><button class="eliminar" type="button" >Eliminar</button></td>
            </tr>          
       
       
       </tbody>
       </table>
        <input type="button" id="btnAgregar" value="Agregar Sector" />
 

    
  

    </fieldset>
<% } %>

