﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<MvcEntradas.Entity.Establecimientos>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Create
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<style type="text/css">
.eliminar{ cursor: pointer; color: #000; }
 
</style>
<h2>Nuevo Establecimiento</h2>

<script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>" type="text/javascript"></script>
<script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>" type="text/javascript"></script>
  <script type="text/javascript">
      $.validator.methods.number = function (value, element) {
          value = floatValue(value);
          return this.optional(element) || !isNaN(value);
      }
      $.validator.methods.range = function (value, element, param) {
          value = floatValue(value);
          return this.optional(element) || (value >= param[0] && value <= param[1]);
      }

      function floatValue(value) {
          return parseFloat(value.replace(",", "."));
      }  
</script>
 
<% using (Html.BeginForm()) { %>
    <%:Html.ValidationSummary(true)%>
     <% string msj = "";%>
     <%if (Session["msjErrorEstablecimiento"] != null)
      { %>
    <% msj = Session["msjErrorEstablecimiento"].ToString();%>
    <%} %>
    <fieldset>
        <legend>Establecimientos</legend>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Nombre) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Nombre) %>
            <%: Html.ValidationMessageFor(model => model.Nombre) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.PrecioAlquiler, "Precio Alquiler") %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.PrecioAlquiler) %>
            <%: Html.ValidationMessageFor(model => model.PrecioAlquiler) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.CapacidadTotal, "Capacidad Total") %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.CapacidadTotal) %>
            <%: Html.ValidationMessageFor(model => model.CapacidadTotal) %>
        </div>
        
        <legend>Sectores</legend>
 
          <table id="miTabla"  border="1" >
          <thead>
          <tr>
            <th>
                Descripcion
            </th>
            <th>
                capacidad
            </th>
        
            <th>
                Precio
            </th>
            <th><button type="submit" name="operacion" data-val="false" value="agregar-sector">Agregar Sector</button></th>
        </tr>
          
          </thead>
       <tbody>
            
            <% var i = 0; %>
            <%
                if(Model!= null && Model.Sectores != null && Model.Sectores.Count>0)
                {
                    foreach (var item in Model.Sectores)
                    {%>
                 <tr>
                <td>
                 <%:Html.TextBox("Sectores[" + i + "].Descripcion", item.Descripcion)%>
                </td>
                  <td>
                 <%:Html.TextBox("Sectores[" + i + "].capacidad", item.capacidad)%>
                </td>
                  <td>
                 <%:Html.TextBox("Sectores[" + i + "].Precio", item.Precio)%>
                </td>
                <td><button name="operacion" class="eliminar">Eliminar</button></td>
                  </tr>
            <%  i++;
                    } %>
                
            <% } %>
          
            </tbody>
       </table>

       <br />
      <%-- <select id="cboImagen" name = "cboImagen">
        <option value= "1">Un sector</option>
        <option value= "2">Dos sectores</option>
        <option value= "3">Tres sectores</option>
        <option value= "4">Cuatro sectores</option>
        <option value= "5">Cinco sectores</option>
       </select>--%>
       <button type="submit" name="operacion" data-val="false" value="generar-imagen">Generar Imagen</button>
       <br />

        <div>
            <%  
               if (Session["cantSec"] != null && Session["generar"] != null && Convert.ToInt32(Session["generar"]) == 1)
                {
                    %>
                    <img width="280" height="250" style="margin-left:200px;" alt="Imagen no disponible" src="../../Content/Images/Imagen Sector2.bmp"/> 
                    
                <%}%>
        </div>


       <label id="errores" style="color:Red;font-weight:bold; font-size:larger"><%=msj %></label>
        <p>
            <input type="submit" value="Crear" />
        </p>
    </fieldset>
<% } %>

<div>
    <%: Html.ActionLink("Volver", "Index") %>
</div>

</asp:Content>

