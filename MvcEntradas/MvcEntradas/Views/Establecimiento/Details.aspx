﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<MvcEntradas.Entity.Establecimientos>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Details
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Detalles</h2>

<fieldset>
    <legend>Establecimientos</legend>

    <div class="display-label">Nombre</div>
    <div class="display-field"><%: Model.Nombre %></div>

    <div class="display-label">PrecioAlquiler</div>
    <div class="display-field"><%: String.Format("{0:F}", Model.PrecioAlquiler) %></div>

    <div class="display-label">CapacidadTotal</div>
    <div class="display-field"><%: Model.CapacidadTotal %></div>
</fieldset>
<legend>Sectores</legend>
 
          <table id="miTabla"  border="1" >
          <thead>
          <tr>
            <th>
                Descripcion
            </th>
            <th>
                capacidad
            </th>
        
            <th>
                Precio
            </th>
          
        </tr>
          
          </thead>
       <tbody>
            
            <% var i = 0; %>
            <%
                 foreach (var item in Model.Sectores)
                {%>
                     <%:Html.Hidden("Sectores[" + i + "].IdSector", item.IdSector)%>
                     <%:Html.Hidden("Sectores[" + i + "].idEstablecimiento", item.idEstablecimiento)%>
                     <%:Html.Hidden("IdSector", item.IdSector)%>
                 <tr>
                <td>
                 <%:Html.TextBox("Sectores[" + i + "].Descripcion", item.Descripcion)%>
                </td>
                  <td>
                 <%:Html.TextBox("Sectores[" + i + "].capacidad", item.capacidad)%>
                </td>
                  <td>
                 <%:Html.TextBox("Sectores[" + i + "].Precio", item.Precio)%>
                </td>
               
                  </tr>
            <%  i++; %>
                
            <% } %>
          
            </tbody>
       </table>
<p>

    <%: Html.ActionLink("Modificar", "Edit", new { id=Model.IdEstablecimiento }) %> |
    <%: Html.ActionLink("Volver", "Index") %>
</p>

</asp:Content>

