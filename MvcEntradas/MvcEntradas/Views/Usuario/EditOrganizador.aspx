﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<MvcEntradas.Entity.Usuarios>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Editar
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2><span class="label label-default">Editar</span></h2>

<script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>" type="text/javascript"></script>
<script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>" type="text/javascript"></script>

<% using (Html.BeginForm()) { %>
    <%: Html.ValidationSummary(true) %>
    <fieldset>
        <legend>Usuarios</legend>

        <%: Html.HiddenFor(model => model.IdUsuario) %>

         <%: Html.HiddenFor(model => model.NombreUsuario) %>
         <%: Html.HiddenFor(model => model.IdPerfil) %>
         <%: Html.HiddenFor(model => model.Password) %>
         <%: Html.HiddenFor(model => model.IsActivo) %>
        <div class="editor-label">
            <%: Html.LabelFor(model => model.Email) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Email) %>
            <%: Html.ValidationMessageFor(model => model.Email) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Nombre) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Nombre) %>
            <%: Html.ValidationMessageFor(model => model.Nombre) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Apellido) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Apellido) %>
            <%: Html.ValidationMessageFor(model => model.Apellido) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.DNI) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.DNI) %>
            <%: Html.ValidationMessageFor(model => model.DNI) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.NombreLegal, "Nombre Legal") %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.NombreLegal) %>
            <%: Html.ValidationMessageFor(model => model.NombreLegal) %>
        </div>

        <p>
            <input type="submit" value="Guardar" />
        </p>
    </fieldset>
<% } %>

<div>
   <%-- <%: Html.ActionLink("Volver", "Index") %>--%>
    <a href="javascript:history.back()">Volver</a>
</div>

</asp:Content>
