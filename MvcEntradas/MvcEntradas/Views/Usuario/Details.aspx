﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.master" Inherits="System.Web.Mvc.ViewPage<MvcEntradas.Entity.Usuarios>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Details
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2><span class="label label-default">Mis Datos</span></h2>

<%--<fieldset>
    <legend>Usuario</legend>

    <%--<div class="display-label">Perfiles</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Perfiles.Nombre) %>
    </div>--%>

    <div class="display-label">NombreUsuario: 
   <%-- <div class="display-field">--%>
        <%: Html.DisplayFor(model => model.NombreUsuario) %>
    </div>


    <div class="display-label">Nombre: 
  <%--  <div class="display-field">--%>
        <%: Html.DisplayFor(model => model.Nombre) %>
    </div>

    <div class="display-label">Apellido: </div>
<%--    <div class="display-field">--%>
        <%: Html.DisplayFor(model => model.Apellido) %>
   <%-- </div>--%>

    <div class="display-label">Nro DNI: 
 <%--   <div class="display-field">--%>
        <%: Html.DisplayFor(model => model.DNI) %>
    </div>

    <div class="display-label">Domicilio: 
<%--    <div class="display-field">--%>
        <%: Html.DisplayFor(model => model.Domicilio) %>
    </div>

    <div class="display-label">Provincia: 
 <%--   <div class="display-field">--%>
        <%: Html.DisplayTextFor(model => model.Provincias.Descripcion)%>
    </div>

    <div class="display-label">Codigo Postal:
<%--    <div class="display-field">--%>
        <%: Html.DisplayFor(model => model.CP) %>
    </div>

    <div class="display-label">Nro Tarjeta: 
<%--    <div class="display-field">--%>
        <%: Html.DisplayFor(model => model.NroTarjeta) %>
    </div>

    <div class="display-label">Fecha Vencimiento Tarjeta: 
<%--    <div class="display-field">--%>
        <%: Html.DisplayFor(model => model.FechaVencimientoTarjeta) %>
   </div>

   <%-- <div class="display-label">IsActivo</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.IsActivo) %>
    </div>

    <div class="display-label">NombreLegal</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.NombreLegal) %>
    </div>--%>
</fieldset>
<p>

    <%: Html.ActionLink("Editar", "Edit", new { id=Model.IdUsuario }) %> |
   <%-- <%: Html.ActionLink("Back to List", "Index") %>--%>
 <a href="javascript:history.back()">Volver</a>
</p>

</asp:Content>
