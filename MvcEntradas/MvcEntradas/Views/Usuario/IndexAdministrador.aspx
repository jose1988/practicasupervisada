﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<MvcEntradas.Entity.Usuarios>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Listado Administradores
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2><span class="label label-default">Listado de Administradores</span></h2>

<p>
    <%: Html.ActionLink("Nuevo", "CreateOrganizador") %>
</p>
<table>
    <tr>
        <th>
            Id Perfil
        </th>
        <th>
            Nombre Usuario
        </th>
       <%-- <th>
            Password
        </th>--%>
        <th>
            Email
        </th>
        <th>
            Nombre
        </th>
        <th>
            Apellido
        </th>
        <th></th>
    </tr>

<% foreach (var item in Model) { %>
    <tr>
        <td>
            <%: Html.DisplayFor(modelItem => item.IdPerfil) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.NombreUsuario) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Email) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Nombre) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Apellido) %>
        </td>
        
        <td>
            <%: Html.ActionLink("Editar", "EditOrganizador", new { id=item.IdUsuario }) %> |
            <%: Html.ActionLink("Detalles", "DetailsOrganizador", new { id=item.IdUsuario }) %> |
            <%: Html.ActionLink("Borrar", "DeleteOrganizador", new { id=item.IdUsuario }) %>
        </td>
    </tr>
<% } %>

</table>

</asp:Content>
