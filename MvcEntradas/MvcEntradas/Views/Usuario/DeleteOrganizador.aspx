﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<MvcEntradas.Entity.Usuarios>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Eliminar
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Eliminar</h2>

<h3>¿Esta seguro de eliminar este usuario?</h3>
<fieldset>
    <legend>Usuarios</legend>

    <div class="display-label">IdPerfil</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Perfiles.Nombre) %>
    </div>

    <div class="display-label">Nombre Usuario</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.NombreUsuario) %>
    </div>

    <div class="display-label">Email</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Email) %>
    </div>

    <div class="display-label">Nombre</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Nombre) %>
    </div>

    <div class="display-label">Apellido</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Apellido) %>
    </div>

    <div class="display-label">DNI</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.DNI) %>
    </div>

    <div class="display-label">Nombre Legal</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.NombreLegal) %>
    </div>
</fieldset>
<% using (Html.BeginForm()) { %>
    <p>
        <input type="submit" value="Delete" /> |
        <%: Html.ActionLink("Volver", "Index") %>
    </p>
<% } %>

</asp:Content>
