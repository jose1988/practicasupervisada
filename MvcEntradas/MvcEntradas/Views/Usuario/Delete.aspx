﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.master" Inherits="System.Web.Mvc.ViewPage<MvcEntradas.Entity.Usuarios>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Deshabilitar
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Deshabilitar</h2>

<h3>¿Seguro de deshabilitar este usuario?</h3>
<fieldset>
    <legend>Usuarios</legend>


    <div class="display-label">Nombre Usuario</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.NombreUsuario) %>
    </div>

    <div class="display-label">Nombre</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Nombre) %>
    </div>

    <div class="display-label">Apellido</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Apellido) %>
    </div>

    <div class="display-label">DNI</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.DNI) %>
    </div>

    <div class="display-label">Domicilio</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Domicilio) %>
    </div>

    <div class="display-label">Provincia</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Provincias.Descripcion) %>
    </div>

    <div class="display-label">CP</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.CP) %>
    </div>

    <div class="display-label">NroTarjeta</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.NroTarjeta) %>
    </div>

    <div class="display-label">Fecha de Vencimiento Tarjeta</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.FechaVencimientoTarjeta) %>
    </div>

    <div class="display-label">Activo</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.IsActivo) %>
    </div>

</fieldset>
<% using (Html.BeginForm()) { %>
    <p>
        <input type="submit" value="Deshabilitar" /> |
        <%: Html.ActionLink("Volver", "Index") %>
    </p>
<% } %>

</asp:Content>
