﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<MvcEntradas.Entity.Usuarios>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Listado Usuarios
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2><span class="label label-default">Listado de Usuarios</span></h2>

<% using (Html.BeginForm()) { %>
 <div class="editor-field">
    <label style="font-weight:bolder">Filtros:</label>
    <br />
    <table>
    <tr>
    <td><label>Por DNI</label></td>
    <td> <input id="dni" type="text" name="dni" />
    </td>
    <td>
     <label>Por Nombre De Usuario</label></td>
    <td> <input id="nombreU" type="text" name="nombreU" />
    </td>
    </tr>
    <tr>
    <td>
    <label>Por Apellido</label></td>
      <td><input id="ape" type="text" name="ape" />
    </td>
    <td> 
    <label>Por Domicilio</label></td>
      <td><input id="dom" type="text" name="dom" />
    </td>
    </tr>
    </table>

     <input type="submit" value="Filtrar" style ='-webkit-border-radius: 5px;
    -moz-border-radius: 5px;border-radius: 5px;'/>
 </div>
 <% } %>
 <br />

<table>
    <tr>
        <th>
            Nombre De Usuario
        </th>
        <th>
            Nombre
        </th>
        <th>
            Apellido
        </th>
        <th>
            DNI
        </th>
        <th>
            Domicilio
        </th>
        <th>
            Provincia
        </th>
        <th>
            CP
        </th>
        <th>
            NroTarjeta
        </th>
        <th>
            Fecha de Vencimiento Tarjeta
        </th>
        <th>
            Activo
        </th>
        <th></th>
    </tr>

<% foreach (var item in Model) { %>
    <tr>
        <td>
            <%: Html.DisplayFor(modelItem => item.NombreUsuario) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Nombre) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Apellido) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.DNI) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Calle) %>
            <%: Html.DisplayFor(modelItem => item.NumeroCalle) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Provincias.Descripcion) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.CP) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.NroTarjeta) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.FechaVencimientoTarjeta) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.IsActivo) %>
        </td>
        <td>
           <%-- <%: Html.ActionLink("Editar", "Edit", new { id=item.IdUsuario }) %>--%> |
            <%: Html.ActionLink("Detalles", "Details", new { id=item.IdUsuario }) %> |
            <%: Html.ActionLink("Deshabilitar", "Delete", new { id=item.IdUsuario }) %>
        </td>
    </tr>
<% } %>

</table>

</asp:Content>
