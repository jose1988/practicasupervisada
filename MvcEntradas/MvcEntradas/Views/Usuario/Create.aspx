﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.master" Inherits="System.Web.Mvc.ViewPage<MvcEntradas.Entity.Usuarios>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
     Registrarse
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

 <h2>Crear una nueva cuenta</h2>
    <p>
        Use el formulario siguiente para crear una cuenta nueva. 
    </p>
    <p>
        Es necesario que las contraseñas tengan al menos <%: Membership.MinRequiredPasswordLength %> caracteres.
    </p>

<script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>" type="text/javascript"></script>
<script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>" type="text/javascript"></script>

<% using (Html.BeginForm()) { %>
    <%: Html.ValidationSummary(true,"No se creó la cuenta. Corrija los errores e inténtelo de nuevo.") %>
    <fieldset>
        <legend>Usuarios</legend>

        <%--<div class="editor-label">
            <%: Html.LabelFor(model => model.IdPerfil, "Perfiles") %>
        </div>
        <div class="editor-field">
            <%: Html.DropDownList("IdPerfil", String.Empty) %>
            <%: Html.ValidationMessageFor(model => model.IdPerfil) %>
        </div>--%>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.NombreUsuario, "Nombre de Usuario") %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.NombreUsuario) %>
            <%: Html.ValidationMessageFor(model => model.NombreUsuario) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Password) %>
        </div>
        <div class="editor-field">
            <%: Html.PasswordFor(model => model.Password) %>
            <%: Html.ValidationMessageFor(model => model.Password) %>
        </div>
        <%--<input id="Password1" type="password" />--%>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Email) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Email) %>
            <%: Html.ValidationMessageFor(model => model.Email) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Nombre) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Nombre) %>
            <%: Html.ValidationMessageFor(model => model.Nombre) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Apellido) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Apellido) %>
            <%: Html.ValidationMessageFor(model => model.Apellido) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.DNI) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.DNI) %>
            <%: Html.ValidationMessageFor(model => model.DNI) %>
        </div>

         <div class="editor-label">
            <%: Html.Label("Domicilio")%>
        </div>
        <div class="editor-label">
            <%: Html.LabelFor(model => model.Calle)%>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Calle)%>
            <%: Html.ValidationMessageFor(model => model.Calle) %>
        </div>
        <div class="editor-label">
            <%: Html.LabelFor(model => model.NumeroCalle, "Numero")%>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.NumeroCalle)%>
            <%: Html.ValidationMessageFor(model => model.NumeroCalle)%>
        </div>
          <div class="editor-label">
            <%: Html.LabelFor(model => model.Piso)%>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Piso)%>
            <%: Html.ValidationMessageFor(model => model.Piso)%>
        </div>

         <div class="editor-label">
            <%: Html.LabelFor(model => model.Departamento)%>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Departamento)%>
            <%: Html.ValidationMessageFor(model => model.Departamento)%>
        </div>
        <div class="editor-label">
            <%: Html.LabelFor(model => model.IdProvincia, "Provincia") %>
        </div>
       <div class="editor-field">
            <%: Html.DropDownList("IdProvincia", String.Empty)%>
            <%: Html.ValidationMessageFor(model => model.IdProvincia) %>
        </div>

        <h5>Si es domicilio de envio complete zona, recuerde que el envio es solo
        para Capital, La Plata, zona sur, oeste y norte de GBA.</h5>
         <div class="editor-label">
            <%: Html.LabelFor(model => model.IdZonaEnvio, "Zona de envio") %>
        </div>
       <div class="editor-field">
            <%: Html.DropDownList("IdZonaEnvio", String.Empty)%>
            <%: Html.ValidationMessageFor(model => model.IdZonaEnvio) %>
        </div>

         <div class="editor-label">
            <%: Html.LabelFor(model => model.Localidad, "Localidad")%>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Localidad) %>
            <%: Html.ValidationMessageFor(model => model.Localidad)%>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.CodigoPostal, "Codigo Postal")%>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.CodigoPostal) %>
            <%: Html.ValidationMessageFor(model => model.CodigoPostal)%>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.NroTarjeta) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.NroTarjeta) %>
            <%: Html.ValidationMessageFor(model => model.NroTarjeta) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.FechaVencimientoTarjeta, "Fecha de Vencimiento Tarjeta") %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.FechaVencimientoTarjeta) %>
            <%: Html.ValidationMessageFor(model => model.FechaVencimientoTarjeta) %>
        </div>

      <%--  <div class="editor-label">
            <%: Html.LabelFor(model => model.IsActivo) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.IsActivo) %>
            <%: Html.ValidationMessageFor(model => model.IsActivo) %>
        </div>--%>

       <%-- <div class="editor-label">
            <%: Html.LabelFor(model => model.NombreLegal) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.NombreLegal) %>
            <%: Html.ValidationMessageFor(model => model.NombreLegal) %>
        </div>
--%>
        <p>
            <input type="submit" value="Registrarse" />
        </p>
    </fieldset>
<% } %>

<div>
    <%: Html.ActionLink("Volver", "Index") %>
</div>

</asp:Content>
