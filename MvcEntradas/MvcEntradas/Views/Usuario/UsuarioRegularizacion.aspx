﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Usuario Regularizacion
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Usuario Regularizacion</h2>

<h3> Para regularizarse debe abonar el siguiente importe con tarjeta de crédito</h3>
<% using (Html.BeginForm()) { %>
<div>

  <%: Html.Label((string)ViewBag.Monto) %>
 <input id="selTar" type="button" value="Elegir Tarjeta" />
 <input type="hidden" id="idTar"/>
 <input type="text" id="Tarjeta" readonly="readonly" name="Tarjeta" />

 <br />
 <input type="submit" id="aceptar" value="Aceptar Pago"/>
           
 </div>
 <% } %>

 <script type="text/javascript" src="../../Scripts/regu.js"></script>
</asp:Content>
