﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<MvcEntradas.Entity.Usuarios>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Detalles y Datos
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2><span class="label label-default">Datos</span></h2>

<fieldset>
    <legend>Detalles y Datos</legend>

    <div class="display-label">Perfil</div>
    <div class="display-field">
        <%: Html.DisplayTextFor(model => model.Perfiles.Nombre) %>
    </div>

    <div class="display-label">Nombre Usuario</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.NombreUsuario) %>
    </div>


    <div class="display-label">Email</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Email) %>
    </div>

    <div class="display-label">Nombre</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Nombre) %>
    </div>

    <div class="display-label">Apellido</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Apellido) %>
    </div>

    <div class="display-label">DNI</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.DNI) %>
    </div>

    <div class="display-label">Nombre Legal</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.NombreLegal) %>
    </div>
</fieldset>
<p>

    <%: Html.ActionLink("Editar", "EditOrganizador", new { id=Model.IdUsuario }) %> |
     <%: Html.ActionLink("Eliminar", "DeleteOrganizador", new { id=Model.IdUsuario }) %> |
    <a href="javascript:history.back()">Volver</a>
</p>

</asp:Content>
