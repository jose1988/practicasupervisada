﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<MvcEntradas.Entity.Usuarios>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Crear Organizador/Administrador
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Nuevo Organizador/Administrador</h2>

<script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>" type="text/javascript"></script>
<script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>" type="text/javascript"></script>

<% using (Html.BeginForm()) { %>
    <%: Html.ValidationSummary(true,"No se creó la cuenta. Corrija los errores e inténtelo de nuevo.") %>
    <fieldset>
        <legend>Usuarios</legend>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.IdPerfil, "Perfil") %>
        </div>
         <div class="editor-field">
            <%: Html.DropDownList("IdPerfil", String.Empty) %>
            <%: Html.ValidationMessageFor(model => model.IdPerfil) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.NombreUsuario, "Nombre de Usuario")%>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.NombreUsuario) %>
            <%: Html.ValidationMessageFor(model => model.NombreUsuario) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Password) %>
        </div>
        <div class="editor-field">
            <%: Html.PasswordFor(model => model.Password) %>
            <%: Html.ValidationMessageFor(model => model.Password) %>
        </div>

         <div class="editor-label">
            <%: Html.LabelFor(model => model.Email) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Email) %>
            <%: Html.ValidationMessageFor(model => model.Email) %>
        </div>
        <div class="editor-label">
            <%: Html.LabelFor(model => model.Nombre) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Nombre) %>
            <%: Html.ValidationMessageFor(model => model.Nombre) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Apellido) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Apellido) %>
            <%: Html.ValidationMessageFor(model => model.Apellido) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.DNI) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.DNI) %>
            <%: Html.ValidationMessageFor(model => model.DNI) %>
        </div>

       <%-- <div class="editor-label">
            <%: Html.LabelFor(model => model.Domicilio) %>
        </div>--%>
       <%-- <div class="editor-field">
            <%: Html.EditorFor(model => model.Domicilio) %>
            <%: Html.ValidationMessageFor(model => model.Domicilio) %>
        </div>--%>
<%--
        <div class="editor-label">
            <%: Html.LabelFor(model => model.IdProvincia) %>
        </div>
        <div class="editor-field">
            <%: Html.DropDownList("IdProvincia", String.Empty)%>
            <%: Html.ValidationMessageFor(model => model.IdProvincia) %>
        </div>--%>

      <%--  <div class="editor-label">
            <%: Html.LabelFor(model => model.CP) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.CP) %>
            <%: Html.ValidationMessageFor(model => model.CP) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.NroTarjeta) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.NroTarjeta) %>
            <%: Html.ValidationMessageFor(model => model.NroTarjeta) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.FechaVencimientoTarjeta) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.FechaVencimientoTarjeta) %>
            <%: Html.ValidationMessageFor(model => model.FechaVencimientoTarjeta) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.IsActivo) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.IsActivo) %>
            <%: Html.ValidationMessageFor(model => model.IsActivo) %>
        </div>
--%>
        <div class="editor-label">
            <%: Html.LabelFor(model => model.NombreLegal, "Nombre Legal") %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.NombreLegal) %>
            <%: Html.ValidationMessageFor(model => model.NombreLegal) %>
        </div>


        <p>
            <input type="submit" value="Crear" />
        </p>
    </fieldset>
<% } %>

<div>
    <%: Html.ActionLink("Volver", "Index") %>
</div>

</asp:Content>
