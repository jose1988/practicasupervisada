﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.master" Inherits="System.Web.Mvc.ViewPage<MvcEntradas.Entity.Usuarios>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Editar
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2><span class="label label-default">Editar</span></h2>

<script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>" type="text/javascript"></script>
<script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>" type="text/javascript"></script>

<% using (Html.BeginForm()) { %>
    <%: Html.ValidationSummary(true) %>
    <fieldset>
        <legend>Usuarios</legend>


        <%: Html.HiddenFor(model => model.IdUsuario) %>
        <%: Html.HiddenFor(model => model.IsActivo) %>
        <%: Html.HiddenFor(model => model.IdPerfil) %>
        <%: Html.HiddenFor(model => model.NombreUsuario) %>
         <%: Html.HiddenFor(model => model.Password) %>

<%--        <div class="editor-label">
            <%: Html.LabelFor(model => model.IdPerfil, "Perfiles") %>
        </div>
        <div class="editor-field">
            <%: Html.DropDownList("IdPerfil", String.Empty) %>
            <%: Html.ValidationMessageFor(model => model.IdPerfil) %>
        </div>--%>
<%--
        <div class="editor-label">
            <%: Html.LabelFor(model => model.NombreUsuario) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.NombreUsuario) %>
            <%: Html.ValidationMessageFor(model => model.NombreUsuario) %>
        </div>--%>

      <%--  <div class="editor-label">
            <%: Html.LabelFor(model => model.Password) %>
        </div>
        <div class="editor-field">
            <%: Html.PasswordFor(model => model.Password) %>
            <%: Html.ValidationMessageFor(model => model.Password) %>
        </div>

         <div class="editor-label">
            <%: Html.Label("Nueva Contraseña") %>--%>
       <%-- </div>
        <input id="newPass" name="newPass" type="password" />--%>

         <div class="editor-label">
            <%: Html.LabelFor(model => model.Email) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Email) %>
            <%: Html.ValidationMessageFor(model => model.Email) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Nombre) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Nombre) %>
            <%: Html.ValidationMessageFor(model => model.Nombre) %>
        </div>
        <div class="editor-label">
            <%: Html.LabelFor(model => model.Apellido) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Apellido) %>
            <%: Html.ValidationMessageFor(model => model.Apellido) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.DNI) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.DNI) %>
            <%: Html.ValidationMessageFor(model => model.DNI) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Domicilio) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Domicilio) %>
            <%: Html.ValidationMessageFor(model => model.Domicilio) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.IdProvincia, "Provincia") %>
        </div>
        <div class="editor-field">
            <%: Html.DropDownList("IdProvincia", String.Empty)%>
            <%: Html.ValidationMessageFor(model => model.IdProvincia) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.CP) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.CP) %>
            <%: Html.ValidationMessageFor(model => model.CP) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.NroTarjeta) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.NroTarjeta) %>
            <%: Html.ValidationMessageFor(model => model.NroTarjeta) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.FechaVencimientoTarjeta) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.FechaVencimientoTarjeta) %>
            <%: Html.ValidationMessageFor(model => model.FechaVencimientoTarjeta) %>
        </div>
<%--
        <div class="editor-label">
            <%: Html.LabelFor(model => model.IsActivo) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.IsActivo) %>
            <%: Html.ValidationMessageFor(model => model.IsActivo) %>
        </div>--%>

<%--        <div class="editor-label">
            <%: Html.LabelFor(model => model.NombreLegal) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.NombreLegal) %>
            <%: Html.ValidationMessageFor(model => model.NombreLegal) %>
        </div>
--%>
        <p>
            <input type="submit" value="Guardar" />
        </p>
    </fieldset>
<% } %>

<div>
    <a href="javascript:history.back()">Volver</a>
</div>

</asp:Content>
