﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<MvcEntradas.Models.ShowDetallado>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Shows Pasados
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2><span class="label label-default">Listado de Shows</span></h2>

<% using (Html.BeginForm()) { %>
 <div class="editor-field">
    <label style="font-weight:bolder">Filtros:</label>
    <br />
    <table>
    <tr>
    <td><label>Por Artista</label></td>
    <td><input id="artista" type="text" name="artista" />
    </td>
    <td>
     <label>Por Establecimiento</label></td>
    <td> <input id="est" type="text" name="est" />
    </td>
    </tr>
    <tr>
    <td>
    <label>Por Fecha</label></td>
      <td><input id="fecha" type="text" name="fecha" />
    </td>
    </tr>
    </table>

     <input type="hidden" id="idShow" name="idShow" />
     <input type="submit" value="Filtrar" style ='-webkit-border-radius: 5px;
    -moz-border-radius: 5px;border-radius: 5px;'/>
 </div>
 <% } %>
 <br />

<table>
    <tr>
        <th>
            Establecimientos
        </th>
        <th>
            Artista
        </th>
        <th>
            Descripcion
        </th>
        <th>
            Fecha/Hora
        </th>
        <th>
            Aprobado
        </th>
         <th>
            Capacidad total
        </th>
        <th>
            Entradas vendidas
        </th>
        <th>
            Entradas no vendidas
        </th>
    </tr>

<% foreach (var item in Model) { %>
    <tr>
        <td>
            <%: Html.DisplayFor(modelItem => item.Establecimiento) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Artista) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Descripcion) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.FechaHora) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.IsAprobado) %>
        </td>
         <td>
            <%: Html.DisplayFor(modelItem => item.CapacidadTotal) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.EntradasVendidas) %>
        </td>
         <td>
             <%: Html.DisplayFor(modelItem => item.EntradasNoVendidas) %>
        </td>
    </tr>
<% } %>

</table>
</asp:Content>
