﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<MvcEntradas.Entity.Shows>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    AltaEntrada Home
</asp:Content>

 
<%-- <script type="text/C#" language="C#" runat="server">

     //protected void Page_Load(Object Sender, EventArgs e)
     //{
     //    if (!IsPostBack)
     //    {
     //        IEnumerable<MvcEntradas.Entity.Shows> lista = (IEnumerable<MvcEntradas.Entity.Shows>)Session["carrusel"];
     //        int index = 0;
     //        myCarousel.InnerHtml = "<ol class='carousel-indicators'>";
     //        foreach (var show in lista)
     //        {
     //            myCarousel.InnerHtml += "<li data-target='#myCarousel' data-slide-to="
     //                + "'" + index + "'";
     //            if (index == 0)
     //                myCarousel.InnerHtml += "class='active'";
     //            myCarousel.InnerHtml += "></li>";

     //            index++;
     //        }

     //        myCarousel.InnerHtml += "</ol>";

     //        int index2 = 0;
     //        string source = "../../Content/Images/";
     //        string[] stringSeparators = new string[] { "\\Images\\" };
     //        myCarousel.InnerHtml += "<div class='carousel-inner'>";
     //        foreach (var show in lista)
     //        {

     //            string[] result = show.Imagen.Split(stringSeparators, StringSplitOptions.None);
     //            string srcCompleta = source + result[1];
     //            if (index2 == 0)
     //                myCarousel.InnerHtml += "<div class='item active'>";
     //            else
     //                myCarousel.InnerHtml += "<div class='item'>";

     //            myCarousel.InnerHtml += "<img src='" + srcCompleta + "' alt='" + index2 + "slide'>" +
     //                                     "<div class='container'>" +
     //                                     "<div class='carousel-caption'>" +
     //      "<p><a class='btn btn-lg btn-primary' href='../Compra/Create?IdShow=" + show.IdShow + "' role='button'>Comprar entradas</a></p>" +
     //    "</div></div></div>";

     //            index2++;
     //        }
     //        myCarousel.InnerHtml += "</div>";
     //        myCarousel.InnerHtml += "<a class='left carousel-control' href='#myCarousel' role='button' data-slide='prev'><span class='glyphicon glyphicon-chevron-left'></span></a>" +
     //                                "<a class='right carousel-control' href='#myCarousel' role='button' data-slide='next'><span class='glyphicon glyphicon-chevron-right'></span></a>";
     //    }
     //}
        
</script>--%>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel" style="margin-left:5px;">
      
     
      <%--<!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>


      <div class="carousel-inner">
        <div class="item active">
          <img src="../../Content/Images/eruca.jpg" alt="First slide">
          <div class="container">
            <div class="carousel-caption">

              <p><a class="btn btn-lg btn-primary" href="../Compra/Create" role="button">Comprar entradas</a></p>
             
            </div>
          </div>
        </div>
        <div class="item">
          <img src="../../Content/Images/queens-of-the-stone-age.jpg" alt="Second slide">
          <div class="container">
            <div class="carousel-caption">
              <p><a class="btn btn-lg btn-primary" href="../Compra/Create" role="button">Comprar entradas</a></p>
            </div>
          </div>
        </div>
        <div class="item">
          <img src="../../Content/Images/salta-la-banca.jpg" alt="Third slide">
          <div class="container">
            <div class="carousel-caption">
              <p><a class="btn btn-lg btn-primary" href="../Compra/Create" role="button">Comprar entradas</a></p>
            </div>
          </div>
        </div>
      </div>
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
--%>
    </div><!-- /.carousel -->

    <h2><span class="label label-default">Próximos Shows</span></h2>

    <div class="row" id= "myDiv">
    <%--  <div class="col-sm-6 col-md-4">
        <div class="thumbnail">
          <img src="../../Content/Images/pearl_jam_ten_front.jpg" style="height: 200px; width: 100%; display: block;" data-src="holder.js/100%x200" alt="100%x200">
          <div class="caption">
            <h3>PJ de vuelta en vivo</h3>
            <p>Repasando sus 20 años</p>
            <p><a href="#" class="btn btn-primary" role="button">Comprar</a></p>
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-md-4">
        <div class="thumbnail">
          <img src="../../Content/Images/Muse.jpg" style="height: 200px; width: 100%; display: block;" data-src="holder.js/100%x200" alt="100%x200">
          <div class="caption">
            <h3>La musa de Muse</h3>
            <p>Cuarta visita al país</p>
            <p><a href="#" class="btn btn-primary" role="button">Comprar</a></p>
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-md-4">
        <div class="thumbnail">
          <img src="../../Content/Images/white stripes800.jpg" style="height: 200px; width: 100%; display: block;" data-src="holder.js/100%x200" alt="100%x200">
          <div class="caption">
            <h3>White Stripes reunidos</h3>
            <p></p>
            <p><a href="#" class="btn btn-primary" role="button">Comprar</a></p>
          </div>
        </div>
      </div>
       <div class="col-sm-6 col-md-4">
        <div class="thumbnail">
          <img src="../../Content/Images/queens-of-the-stone-age.jpg" style="height: 200px; width: 100%; display: block;" data-src="holder.js/100%x200" alt="100%x200">
          <div class="caption">
            <h3>Quees Of The Stone Age</h3>
            <p></p>
            <p><a href="#" class="btn btn-primary" role="button">Comprar</a></p>
          </div>
        </div>
      </div>--%>
   </div>
    
    <script type="text/javascript">


        window.onload = function () {
            //            var variable1 = '<%= Session["carrusel"] %>';
            myCarousel.innerHTML = "<%= Session["carrusel"] %>";
            myDiv.innerHTML = "<%= Session["divCentral"] %>";


        }
</script>


</asp:Content>
