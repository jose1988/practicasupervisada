﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<MvcEntradas.Entity.Usuarios>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Habilitar
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Habilitar</h2>

<h3>¿Esta Seguro de Habilitar este usuario?</h3>
<fieldset>
    <legend>Usuarios</legend>
<%--
    <div class="display-label">IdPerfil</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.IdPerfil) %>
    </div>--%>

    <div class="display-label">Nombre Usuario</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.NombreUsuario) %>
    </div>

  <%--  <div class="display-label">Password</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Password) %>
    </div>--%>

    <div class="display-label">Email</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Email) %>
    </div>

    <div class="display-label">Nombre</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Nombre) %>
    </div>

    <div class="display-label">Apellido</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Apellido) %>
    </div>

    <div class="display-label">DNI</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.DNI) %>
    </div>

    <div class="display-label">Domicilio</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Domicilio) %>
    </div>

   <%-- <div class="display-label">IdProvincia</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.IdProvincia) %>
    </div>--%>

    <div class="display-label">CP</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.CP) %>
    </div>

    <div class="display-label">Nro Tarjeta</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.NroTarjeta) %>
    </div>

    <div class="display-label">Fecha de Vencimiento Tarjeta</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.FechaVencimientoTarjeta) %>
    </div>

    <div class="display-label">Activo</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.IsActivo) %>
    </div>

   <%-- <div class="display-label">NombreLegal</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.NombreLegal) %>
    </div>--%>
</fieldset>
<% using (Html.BeginForm()) { %>
    <p>
        <input type="submit" value="Habilitar" /> |
        <%: Html.ActionLink("Volver", "Index") %>
    </p>
<% } %>

</asp:Content>
