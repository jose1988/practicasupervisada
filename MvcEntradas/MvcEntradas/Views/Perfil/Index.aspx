﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<MvcEntradas.Entity.Perfiles>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Index
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Listado de perfiles</h2>

<p>
    <%: Html.ActionLink("Nuevo", "Create") %>
</p>
<table>
    <tr>
        <th>
            Nombre
        </th>
        <th></th>
    </tr>

<% foreach (var item in Model) { %>
    <tr>
        <td>
            <%: Html.DisplayFor(modelItem => item.Nombre) %>
        </td>
        <td>
            <%: Html.ActionLink("Editar", "Edit", new { id=item.IdPerfil }) %> |
            <%: Html.ActionLink("Detalles", "Details", new { id=item.IdPerfil }) %> |
            <%: Html.ActionLink("Eliminar", "Delete", new { id=item.IdPerfil }) %>
        </td>
    </tr>
<% } %>

</table>

</asp:Content>
