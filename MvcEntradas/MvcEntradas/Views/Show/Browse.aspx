﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<MvcEntradas.Entity.Shows>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Busqueda
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2><span class="label label-default">Búsqueda</span></h2>

<h4>Resultados</h4>

<% foreach (var item in Model)
   { %>
  <!-- Example row of columns -->
        <div class="col-lg-4">
        <div>
            <h3><%: Html.DisplayFor(modelItem => item.Artista) %></h3>   
        </div>
        <div>
            <p>Descripcion</p>
            <%: Html.DisplayFor(modelItem => item.Descripcion) %>
        </div>
        <div> 
            <%:Html.DisplayFor(modelItem => item.FechaHora) %>
        </div>
        <div> 
            <%:Html.DisplayFor(modelItem => item.Establecimientos.Nombre) %>
        </div>
        <div>
            <img width="280" height="250" src="<%:Url.Action("Preview","Show" , new {file=item.Imagen})%>" /> 
        </div>
        <form class="navbar-form navbar-left" role="search" action="../Compra/Create" method="get">
            <%: Html.Hidden("IdShow",item.IdShow) %>
            <button type="submit" class="btn btn-default">Comprar</button>
        </form>
        
     </div>
     <br />
     <br />
<%} %>
</asp:Content>
