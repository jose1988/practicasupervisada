﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.master" Inherits="System.Web.Mvc.ViewPage<MvcEntradas.Entity.Shows>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Details
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<%--
 <script type="text/C#" language="C#" runat="server">

        protected void Page_Load(Object Sender, EventArgs e)
        {
            Ima
        }
</script>--%>
  <h2><span class="label label-default">Detalles</span></h2>

<fieldset>
    <legend>Shows</legend>

    <div class="display-label">Establecimiento</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Establecimientos.Nombre) %>
    </div>

    <div class="display-label">Organizador</div>
    <%--<div class="display-field">
        <%: Html.DisplayFor(model => model.Organizadores.NombreLegal) %>
    </div>--%>

    <div class="display-label">Artista</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Artista) %>
    </div>

    <div class="display-label">Descripcion</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Descripcion) %>
    </div>

    <div class="display-label">FechaHora</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.FechaHora) %>
    </div>

    <div class="display-label">IsAprobado</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.IsAprobado) %>
    </div>

    <div class="display-label">Imagen</div>
    <div class="display-field">
     <img width="280" height="250" src="<%:Url.Action("Preview","Show" , new {file=Model.Imagen})%>" />
    </div>
</fieldset>
<p>

    <%: Html.ActionLink("Editar", "Edit", new { id=Model.IdShow }) %> |
    <%: Html.ActionLink("Volver", "Index") %>
</p>

</asp:Content>
