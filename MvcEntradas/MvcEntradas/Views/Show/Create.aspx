﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.master" Inherits="System.Web.Mvc.ViewPage<MvcEntradas.Entity.Shows>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Nuevo
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2><span class="label label-default">Nuevo Show</span></h2>

<script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>" type="text/javascript"></script>
<script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>" type="text/javascript"></script>
<script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>" type="text/javascript"></script>

<% using (Html.BeginForm()) { %>
    <%: Html.ValidationSummary(true) %>
    <fieldset>
        <legend>Shows</legend>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.IdEstablecimiento, "Establecimientos") %>
        </div>
        <div class="editor-field">
            <%: Html.DropDownListFor(model=> model.IdEstablecimiento, (SelectList)ViewBag.IdEstablecimiento) %>
            <%: Html.ValidationMessageFor(model => model.IdEstablecimiento) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Artista) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Artista) %>
            <%: Html.ValidationMessageFor(model => model.Artista) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Descripcion) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Descripcion) %>
            <%: Html.ValidationMessageFor(model => model.Descripcion) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.FechaHora, "Fecha y Hora") %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.FechaHora) %>
            <%: Html.ValidationMessageFor(model => model.FechaHora,"La fecha no es valida") %>
        </div>
        <div class="editor-label">
            <%: Html.LabelFor(model => model.Imagen) %>
        </div>

         <div class="editor-field">
            <img width="280" height="250" name = "imagen" id = "img" src="">
        </div>
  
        <p>
            <input type="submit" value="Guardar" />
        </p>
    </fieldset>
<% } %>

<% Html.RenderPartial("FotoShow"); %>

<div>
    <%: Html.ActionLink("Volver", "Index") %>
</div>

</asp:Content>
