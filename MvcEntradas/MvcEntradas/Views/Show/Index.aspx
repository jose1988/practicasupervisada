﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<MvcEntradas.Entity.Shows>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Listado
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2><span class="label label-default">Listado de Shows</span></h2>

<p>
    <%: Html.ActionLink("Nuevo", "Create") %>
</p>
<% string er = (string)ViewBag.error; %>
<label style="color:Red;font-weight:bold; font-size:larger"><%=er %></label>

<% using (Html.BeginForm()) { %>
 <div class="editor-field">
    <label style="font-weight:bolder">Filtros:</label>
    <br />
    <table>
    <tr>
    <td><label>Por Artista</label></td>
    <td><input id="artista" type="text" name="artista" />
    </td>
    <td>
     <label>Por Establecimiento</label></td>
    <td> <input id="est" type="text" name="est" />
    </td>
    </tr>
    <tr>
    <td>
    <label>Por Fecha</label></td>
      <td><input id="fecha" type="text" name="fecha" />
    </td>
    <%if (ViewBag.IdPerfil == 1)
          {%>
    <td> 
    <label>Por Organizador</label></td>
      <td><input id="org" type="text" name="org" />
    </td>
      <%} %>
    </tr>
    </table>

     <input type="hidden" id="idShow" name="idShow" />
     <input type="submit" value="Filtrar" style ='-webkit-border-radius: 5px;
    -moz-border-radius: 5px;border-radius: 5px;'/>
 </div>
 <% } %>
 <br />

<table>
    <tr>
        <th>
            Establecimientos
        </th>
        <%if (ViewBag.IdPerfil == 1)
          {%>
        <th>
            Organizador
        </th>
        <%} %>
        <th>
            Artista
        </th>
        <th>
            Descripcion
        </th>
        <th>
            Fecha y Hora
        </th>
        <th>
            Aprobado
        </th>
        <th>
            Imagen
        </th>
        <th></th>
         <%if (ViewBag.IdPerfil == 1)
           {%>
        <th>
            Cancelar
        </th>
        <%} %>
    </tr>

<% foreach (var item in Model) { %>
    <tr>
        <td>
            <%: Html.DisplayFor(modelItem => item.Establecimientos.Nombre) %>
        </td>
           <%if (ViewBag.IdPerfil == 1)
             {%>
        <td>
            <%: Html.DisplayFor(modelItem => item.Usuarios.NombreLegal)%>
        </td>
        <%} %>
        <td>
            <%: Html.DisplayFor(modelItem => item.Artista) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Descripcion) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.FechaHora) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.IsAprobado) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Imagen) %>
        </td>
        <td>
            <%: Html.ActionLink("Editar", "Edit", new { id=item.IdShow }) %> |
            <%: Html.ActionLink("Detalles", "Details", new { id=item.IdShow }) %> |
            <%--<%: Html.ActionLink("Eliminar", "Delete", new { id=item.IdShow }) %>--%>
        </td>
         <td>
         <% if (item.IsAprobado && ViewBag.IdPerfil == 1)
         { %>
            <%: Html.ActionLink("Cancelar", "CancelarShow", new { id = item.IdShow })%>
         <%} %>
        </td>
    </tr>
<% } %>

</table>

</asp:Content>
