﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.master" Inherits="System.Web.Mvc.ViewPage<MvcEntradas.Entity.Shows>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Eliminar Show
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Eliminar</h2>

<h3>¿Esta seguro de borrar?</h3>
<fieldset>
    <legend>Shows</legend>

    <div class="display-label">Establecimientos</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Establecimientos.Nombre) %>
    </div>

    <%--<div class="display-label">Organizadores</div>--%>
    <%--<div class="display-field">
        <%: Html.DisplayFor(model => model.Organizadores.NombreLegal) %>
    </div>--%>

    <div class="display-label">Artista</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Artista) %>
    </div>

    <div class="display-label">Descripcion</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Descripcion) %>
    </div>

    <div class="display-label">FechaHora</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.FechaHora) %>
    </div>

    <div class="display-label">IsAprobado</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.IsAprobado) %>
    </div>

    <div class="display-label">Imagen</div>
    <div class="display-field">
        <%: Html.DisplayFor(model => model.Imagen) %>
    </div>
</fieldset>
<% using (Html.BeginForm()) { %>
    <p>
        <input type="submit" value="Eliminar" /> |
        <%: Html.ActionLink("Volver", "Index") %>
    </p>
<% } %>

</asp:Content>
