﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<MvcEntradas.Entity.Shows>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Carrusel
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2><span class="label label-default">Carrusel</span></h2>

<% using (Html.BeginForm()) { %>
<table id="Tabla">
    <tr>
        <th>
        
        </th>
        <th>
            Establecimientos
        </th>
        <th>
            Organizadores
        </th>
        <th>
            Artista
        </th>
        <th>
            Descripcion
        </th>
        <th>
            Fecha y Hora
        </th>
        <th>
            Aprobado
        </th>
        <th>
            Es Carrusel
        </th>
        <th></th>
    </tr>

<% foreach (var item in Model) { %>
    <tr>
        <td  class="dataId">
            <%: Html.HiddenFor(modelItem => item.IdShow)%>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Establecimientos.Nombre) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Establecimientos.Nombre) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Artista) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.Descripcion) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.FechaHora) %>
        </td>
        <td>
            <%: Html.DisplayFor(modelItem => item.IsAprobado) %>
        </td>
        <td class="Carrusel">
            <%: Html.CheckBoxFor(modelItem => item.IsCarrusel) %>
        </td>
    </tr>
<% } %>

</table>
 <label id="errores" style="color:Red;font-weight:bold; font-size:larger"></label>
<p>
   <input type="button" id="Guardar" value="Guardar" />
</p>
<% } %>

<script type="text/javascript">

    $('#Guardar').click(function () {
        var Ids = "";
        $("#Tabla .Carrusel input[type=checkbox]").each(function (index) {
            var id = "0";
            if ($(this)[0].checked) {
                $("#Tabla .dataId input[type=hidden]").each(function (index2) {
                    if (index == index2)
                        Ids += $(this)[0].value + ",";
                });
            }

        });

        $.ajax({
            url: "/Show/Carrusel",
            cache: false,
            type: "POST",
            data: { "Ids": Ids },
            success: function (data) {
                $("#errores").text("");
            }, error: function (data) {
                $("#errores").text("Solo se pueden ingresar 5 shows al carrusel.");
            }
        });

    });

    
</script>

</asp:Content>
