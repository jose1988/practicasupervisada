﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<MvcEntradas.Entity.Shows>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Todos
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2><span class="label label-default">Shows</span></h2>

<h4>A-Z</h4>
 <% foreach (var item in Model) { %>
  <div class="col-lg-4">
          <div><h3><%:Html.DisplayFor(modelItem => item.Artista) %></h3></div>
          <div><h4><%:Html.DisplayFor(modelItem => item.Establecimientos.Nombre)%></h4></div>
          <div> <%:Html.DisplayFor(modelItem => item.Descripcion) %></div>
          <div> <%:Html.DisplayFor(modelItem => item.FechaHora) %></div>
          <form class="navbar-form navbar-left" role="search" action="../Compra/Create" method="get">
            <%: Html.Hidden("IdShow",item.IdShow) %>
            <button type="submit" class="btn btn-default">Comprar</button>
        </form>
  </div>
<% } %>
</asp:Content>
