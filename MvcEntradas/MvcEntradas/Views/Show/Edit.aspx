﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.master" Inherits="System.Web.Mvc.ViewPage<MvcEntradas.Entity.Shows>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Editar Show
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2><span class="label label-default">Editar Show</span></h2>

<script src="<%: Url.Content("~/Scripts/jquery.validate.min.js") %>" type="text/javascript"></script>
<script src="<%: Url.Content("~/Scripts/jquery.validate.unobtrusive.min.js") %>" type="text/javascript"></script>

<% using (Html.BeginForm()) { %>
    <%: Html.ValidationSummary(true) %>
    <fieldset>
        <legend>Shows</legend>

       <%-- Van en hidden todos los que no se dejan modificar porq sino no mantienen el valor--%>
        <%: Html.HiddenFor(model => model.IdShow) %>
        <%: Html.HiddenFor(model => model.Imagen) %>
        <%: Html.HiddenFor(model => model.IdUsuario) %>
        <%: Html.HiddenFor(model => model.IsCancelado) %>
        <%: Html.HiddenFor(model => model.IsCarrusel) %>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.IdEstablecimiento, "Establecimientos") %>
        </div>
        <div class="editor-field">
            <%: Html.DropDownList("IdEstablecimiento", String.Empty) %>
            <%: Html.ValidationMessageFor(model => model.IdEstablecimiento) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Artista) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Artista) %>
            <%: Html.ValidationMessageFor(model => model.Artista) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.Descripcion) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.Descripcion) %>
            <%: Html.ValidationMessageFor(model => model.Descripcion) %>
        </div>

        <div class="editor-label">
            <%: Html.LabelFor(model => model.FechaHora) %>
        </div>
        <div class="editor-field">
            <%: Html.EditorFor(model => model.FechaHora) %>
            <%: Html.ValidationMessageFor(model => model.FechaHora) %>
        </div>
        <% if (ViewBag.IdPerfil == 1)
        {%>
            <div class="editor-label">
                <%: Html.LabelFor(model => model.IsAprobado)%>
            </div>
            <div class="editor-field">
                <%: Html.EditorFor(model => model.IsAprobado) %>
                <%: Html.ValidationMessageFor(model => model.IsAprobado) %>
            </div>
        <%}else{ %>  
              <div class="editor-field">
                <%: Html.HiddenFor(model => model.IsAprobado) %>
            </div>
        <%}%>
         <div class="editor-field">
            <img width="280" height="250" name = "imagen" id = "img" src="<%: Session["srcRelativaEdit"]%>">
        </div>

        <p>
            <input type="submit" value="Guardar" />
        </p>
    </fieldset>
<% } %>
<% Html.RenderPartial("FotoShow"); %>
<div>
    <%: Html.ActionLink("Volver", "Index") %>
</div>

</asp:Content>
