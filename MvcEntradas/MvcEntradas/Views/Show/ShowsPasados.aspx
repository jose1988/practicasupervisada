﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<MvcEntradas.Entity.Shows>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Shows Pasados
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2><span class="label label-default">Shows Anteriores</span></h2>
<h4>A-Z</h4>
 <% foreach (var item in Model) { %>
  <div class="col-lg-4">
          <h3><%: Html.DisplayFor(modelItem => item.Artista) %></h3>
          <div><h4><%:Html.DisplayFor(modelItem => item.Establecimientos.Nombre)%></h4></div>
          <div> <%:Html.DisplayFor(modelItem => item.Descripcion) %></div>
          <div> <%:Html.DisplayFor(modelItem => item.FechaHora) %></div>
  </div>
  
<% } %>
</asp:Content>
