USE [VentaEntradas]
GO
/****** Object:  Table [dbo].[Provincias]    Script Date: 10/25/2014 02:53:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Provincias](
	[IdProvincia] [bigint] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Provincias] PRIMARY KEY CLUSTERED 
(
	[IdProvincia] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Perfiles]    Script Date: 10/25/2014 02:53:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Perfiles](
	[IdPerfil] [bigint] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Perfiles] PRIMARY KEY CLUSTERED 
(
	[IdPerfil] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Estados]    Script Date: 10/25/2014 02:53:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Estados](
	[IdEstado] [bigint] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Estados] PRIMARY KEY CLUSTERED 
(
	[IdEstado] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Establecimientos]    Script Date: 10/25/2014 02:53:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Establecimientos](
	[IdEstablecimiento] [bigint] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[PrecioAlquiler] [decimal](18, 2) NOT NULL,
	[CapacidadTotal] [bigint] NOT NULL,
 CONSTRAINT [PK_Establecimientos] PRIMARY KEY CLUSTERED 
(
	[IdEstablecimiento] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ZonasEnvios]    Script Date: 10/25/2014 02:53:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ZonasEnvios](
	[IdZona] [bigint] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Precio] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_ZonasEnvios] PRIMARY KEY CLUSTERED 
(
	[IdZona] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sectores]    Script Date: 10/25/2014 02:53:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sectores](
	[IdSector] [bigint] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[capacidad] [int] NOT NULL,
	[idEstablecimiento] [bigint] NOT NULL,
	[Precio] [numeric](18, 2) NOT NULL,
 CONSTRAINT [PK_Sectores] PRIMARY KEY CLUSTERED 
(
	[IdSector] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Usuarios]    Script Date: 10/25/2014 02:53:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuarios](
	[IdUsuario] [bigint] IDENTITY(1,1) NOT NULL,
	[IdPerfil] [bigint] NOT NULL,
	[NombreUsuario] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[Email] [nvarchar](100) NOT NULL,
	[Nombre] [nvarchar](100) NOT NULL,
	[Apellido] [nvarchar](100) NOT NULL,
	[DNI] [nvarchar](13) NOT NULL,
	[Domicilio] [nvarchar](50) NULL,
	[IdProvincia] [bigint] NULL,
	[CP] [bigint] NULL,
	[NroTarjeta] [nvarchar](30) NULL,
	[FechaVencimientoTarjeta] [smalldatetime] NULL,
	[IsActivo] [bit] NOT NULL,
	[NombreLegal] [nvarchar](100) NULL,
 CONSTRAINT [PK_Usuarios] PRIMARY KEY CLUSTERED 
(
	[IdUsuario] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Shows]    Script Date: 10/25/2014 02:53:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Shows](
	[IdShow] [bigint] IDENTITY(1,1) NOT NULL,
	[IdEstablecimiento] [bigint] NOT NULL,
	[IdUsuario] [bigint] NULL,
	[Artista] [varchar](50) NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[FechaHora] [smalldatetime] NOT NULL,
	[IsAprobado] [bit] NOT NULL,
	[Imagen] [varchar](500) NULL,
 CONSTRAINT [PK_Shows] PRIMARY KEY CLUSTERED 
(
	[IdShow] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Compras]    Script Date: 10/25/2014 02:53:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Compras](
	[IdCompra] [bigint] IDENTITY(1,1) NOT NULL,
	[IdUsuario] [bigint] NOT NULL,
	[IdShow] [bigint] NOT NULL,
	[FormaDePago] [varchar](50) NOT NULL,
	[IsEnvio] [bit] NOT NULL,
	[CantidadEnvios] [int] NOT NULL,
	[IdZonaEnvio] [bigint] NOT NULL,
	[Cantidad] [int] NOT NULL,
	[IdEstado] [bigint] NOT NULL,
	[FechaHora] [smalldatetime] NOT NULL,
	[Importe] [decimal](18, 2) NOT NULL,
	[IsEntregado] [bit] NOT NULL,
	[IdSector] [bigint] NOT NULL,
 CONSTRAINT [PK_Compras] PRIMARY KEY CLUSTERED 
(
	[IdCompra] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  ForeignKey [FK_Compras_Estado]    Script Date: 10/25/2014 02:53:13 ******/
ALTER TABLE [dbo].[Compras]  WITH CHECK ADD  CONSTRAINT [FK_Compras_Estado] FOREIGN KEY([IdEstado])
REFERENCES [dbo].[Estados] ([IdEstado])
GO
ALTER TABLE [dbo].[Compras] CHECK CONSTRAINT [FK_Compras_Estado]
GO
/****** Object:  ForeignKey [FK_Compras_Sectores]    Script Date: 10/25/2014 02:53:13 ******/
ALTER TABLE [dbo].[Compras]  WITH CHECK ADD  CONSTRAINT [FK_Compras_Sectores] FOREIGN KEY([IdSector])
REFERENCES [dbo].[Sectores] ([IdSector])
GO
ALTER TABLE [dbo].[Compras] CHECK CONSTRAINT [FK_Compras_Sectores]
GO
/****** Object:  ForeignKey [FK_Compras_Show]    Script Date: 10/25/2014 02:53:13 ******/
ALTER TABLE [dbo].[Compras]  WITH CHECK ADD  CONSTRAINT [FK_Compras_Show] FOREIGN KEY([IdShow])
REFERENCES [dbo].[Shows] ([IdShow])
GO
ALTER TABLE [dbo].[Compras] CHECK CONSTRAINT [FK_Compras_Show]
GO
/****** Object:  ForeignKey [FK_Compras_Usuario]    Script Date: 10/25/2014 02:53:13 ******/
ALTER TABLE [dbo].[Compras]  WITH CHECK ADD  CONSTRAINT [FK_Compras_Usuario] FOREIGN KEY([IdUsuario])
REFERENCES [dbo].[Usuarios] ([IdUsuario])
GO
ALTER TABLE [dbo].[Compras] CHECK CONSTRAINT [FK_Compras_Usuario]
GO
/****** Object:  ForeignKey [FK_Compras_ZonaEnvio]    Script Date: 10/25/2014 02:53:13 ******/
ALTER TABLE [dbo].[Compras]  WITH CHECK ADD  CONSTRAINT [FK_Compras_ZonaEnvio] FOREIGN KEY([IdZonaEnvio])
REFERENCES [dbo].[ZonasEnvios] ([IdZona])
GO
ALTER TABLE [dbo].[Compras] CHECK CONSTRAINT [FK_Compras_ZonaEnvio]
GO
/****** Object:  ForeignKey [FK_Sectores_Establecimientos]    Script Date: 10/25/2014 02:53:13 ******/
ALTER TABLE [dbo].[Sectores]  WITH CHECK ADD  CONSTRAINT [FK_Sectores_Establecimientos] FOREIGN KEY([idEstablecimiento])
REFERENCES [dbo].[Establecimientos] ([IdEstablecimiento])
GO
ALTER TABLE [dbo].[Sectores] CHECK CONSTRAINT [FK_Sectores_Establecimientos]
GO
/****** Object:  ForeignKey [FK_Show_Establecimiento]    Script Date: 10/25/2014 02:53:13 ******/
ALTER TABLE [dbo].[Shows]  WITH CHECK ADD  CONSTRAINT [FK_Show_Establecimiento] FOREIGN KEY([IdEstablecimiento])
REFERENCES [dbo].[Establecimientos] ([IdEstablecimiento])
GO
ALTER TABLE [dbo].[Shows] CHECK CONSTRAINT [FK_Show_Establecimiento]
GO
/****** Object:  ForeignKey [FK_Show_Usuario]    Script Date: 10/25/2014 02:53:13 ******/
ALTER TABLE [dbo].[Shows]  WITH CHECK ADD  CONSTRAINT [FK_Show_Usuario] FOREIGN KEY([IdUsuario])
REFERENCES [dbo].[Usuarios] ([IdUsuario])
GO
ALTER TABLE [dbo].[Shows] CHECK CONSTRAINT [FK_Show_Usuario]
GO
/****** Object:  ForeignKey [FK_Usuario_Perfil]    Script Date: 10/25/2014 02:53:13 ******/
ALTER TABLE [dbo].[Usuarios]  WITH CHECK ADD  CONSTRAINT [FK_Usuario_Perfil] FOREIGN KEY([IdPerfil])
REFERENCES [dbo].[Perfiles] ([IdPerfil])
GO
ALTER TABLE [dbo].[Usuarios] CHECK CONSTRAINT [FK_Usuario_Perfil]
GO
/****** Object:  ForeignKey [FK_Usuario_Provincia]    Script Date: 10/25/2014 02:53:13 ******/
ALTER TABLE [dbo].[Usuarios]  WITH CHECK ADD  CONSTRAINT [FK_Usuario_Provincia] FOREIGN KEY([IdProvincia])
REFERENCES [dbo].[Provincias] ([IdProvincia])
GO
ALTER TABLE [dbo].[Usuarios] CHECK CONSTRAINT [FK_Usuario_Provincia]
GO
