
use [VentaEntradas] 
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE CancelarCompras

AS
BEGIN
	
	SET NOCOUNT ON;
	
	Update Compras Set IdEstado = 1
WHERE IdCompra IN
(Select c.IdCompra from Compras c
INNER JOIN Shows s
On c.IdShow = s.IdShow
Where c.FormaDePago = 'Efectivo' AND (c.IdEstado = 0 OR c.IdEstado = 3)
AND DATEDIFF(hour, GETDATE(), s.FechaHora) <= 2)--Porq son 2 horas

--Le agrego la cancelacion al usuario
UPDATE Usuarios Set CantidadCancelaciones = CantidadCancelaciones +1
WHERE IdUsuario IN
    (Select c.IdUsuario from Compras c
INNER JOIN Shows s
On c.IdShow = s.IdShow
Where c.FormaDePago = 'Efectivo' AND (c.IdEstado = 0 OR c.IdEstado = 3)
AND DATEDIFF(hour, GETDATE(), s.FechaHora) <= 2)

    
END
GO
