USE [VentaEntradas]
GO
/****** Object:  Table [dbo].[Provincias]    Script Date: 11/24/2014 16:16:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Provincias](
	[IdProvincia] [bigint] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Provincias] PRIMARY KEY CLUSTERED 
(
	[IdProvincia] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Provincias] ON
INSERT [dbo].[Provincias] ([IdProvincia], [Descripcion]) VALUES (1, N'Bs As')
SET IDENTITY_INSERT [dbo].[Provincias] OFF
/****** Object:  Table [dbo].[Perfiles]    Script Date: 11/24/2014 16:16:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Perfiles](
	[IdPerfil] [bigint] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Perfiles] PRIMARY KEY CLUSTERED 
(
	[IdPerfil] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Perfiles] ON
INSERT [dbo].[Perfiles] ([IdPerfil], [Nombre]) VALUES (1, N'Administrador')
INSERT [dbo].[Perfiles] ([IdPerfil], [Nombre]) VALUES (2, N'Organizador')
INSERT [dbo].[Perfiles] ([IdPerfil], [Nombre]) VALUES (3, N'Comprador')
SET IDENTITY_INSERT [dbo].[Perfiles] OFF
/****** Object:  Table [dbo].[FormasDePago]    Script Date: 11/24/2014 16:16:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FormasDePago](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
 CONSTRAINT [PK_FormasDepa] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[FormasDePago] ON
INSERT [dbo].[FormasDePago] ([Id], [Nombre]) VALUES (1, N'Efectivo')
INSERT [dbo].[FormasDePago] ([Id], [Nombre]) VALUES (2, N'Tarjeta de credito')
SET IDENTITY_INSERT [dbo].[FormasDePago] OFF
/****** Object:  Table [dbo].[Establecimientos]    Script Date: 11/24/2014 16:16:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Establecimientos](
	[IdEstablecimiento] [bigint] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[PrecioAlquiler] [numeric](18, 2) NOT NULL,
	[CapacidadTotal] [bigint] NOT NULL,
	[Imagen] [image] NULL,
 CONSTRAINT [PK_Establecimientos] PRIMARY KEY CLUSTERED 
(
	[IdEstablecimiento] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Establecimientos] ON
INSERT [dbo].[Establecimientos] ([IdEstablecimiento], [Nombre], [PrecioAlquiler], [CapacidadTotal], [Imagen]) VALUES (1, N'Bunker', CAST(5000.00 AS Decimal(18, 2)), 600, NULL)
INSERT [dbo].[Establecimientos] ([IdEstablecimiento], [Nombre], [PrecioAlquiler], [CapacidadTotal], [Imagen]) VALUES (2, N'nu', CAST(4.00 AS Decimal(18, 2)), 6, NULL)
INSERT [dbo].[Establecimientos] ([IdEstablecimiento], [Nombre], [PrecioAlquiler], [CapacidadTotal], [Imagen]) VALUES (3, N'P', CAST(33.00 AS Decimal(18, 2)), 66, NULL)
INSERT [dbo].[Establecimientos] ([IdEstablecimiento], [Nombre], [PrecioAlquiler], [CapacidadTotal], [Imagen]) VALUES (4, N'hhh', CAST(54.00 AS Decimal(18, 2)), 65, NULL)
INSERT [dbo].[Establecimientos] ([IdEstablecimiento], [Nombre], [PrecioAlquiler], [CapacidadTotal], [Imagen]) VALUES (26, N'Z1', CAST(222.00 AS Decimal(18, 2)), 23, 0xFFD8FFE000104A46494600010101006000600000FFDB004300080606070605080707070909080A0C140D0C0B0B0C1912130F141D1A1F1E1D1A1C1C20242E2720222C231C1C2837292C30313434341F27393D38323C2E333432FFDB0043010909090C0B0C180D0D1832211C213232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232FFC0001108005100AC03012200021101031101FFC4001F0000010501010101010100000000000000000102030405060708090A0BFFC400B5100002010303020403050504040000017D01020300041105122131410613516107227114328191A1082342B1C11552D1F02433627282090A161718191A25262728292A3435363738393A434445464748494A535455565758595A636465666768696A737475767778797A838485868788898A92939495969798999AA2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE1E2E3E4E5E6E7E8E9EAF1F2F3F4F5F6F7F8F9FAFFC4001F0100030101010101010101010000000000000102030405060708090A0BFFC400B51100020102040403040705040400010277000102031104052131061241510761711322328108144291A1B1C109233352F0156272D10A162434E125F11718191A262728292A35363738393A434445464748494A535455565758595A636465666768696A737475767778797A82838485868788898A92939495969798999AA2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE2E3E4E5E6E7E8E9EAF2F3F4F5F6F7F8F9FAFFDA000C03010002110311003F00F58D5AEAE23D4E648E795546DC057200F94552FB75DFFCFD4FFF007F0D58D67FE42D3FFC07FF00411542BF34C7D7AAB1755293F8A5D7CD9EFD1847D9C74E889FEDD77FF3F53FFDFC347DBAEFFE7EA7FF00BF86A0A2B93EB15BF99FDECD3923D89FEDD77FF3F53FFDFC347DBAEFFE7EA7FF00BF86A0A28FAC56FE67F7B0E48F627FB75DFF00CFD4FF00F7F0D1F6EBBFF9FA9FFEFE1A828A3EB15BF99FDEC3923D89FEDD77FF003F53FF00DFC347DBAEFF00E7EA7FFBF86A0A28FAC56FE67F7B0E48F635349BAB8935385249E5653BB219C907E5354BEDD77FF3F53FFDFC356346FF0090B41FF02FFD04D50AEB9D7ABF5483E67F14BAF940CD423ED1E9D17EA4FF006EBBFF009FA9FF00EFE1A3EDD77FF3F53FFDFC350515C9F58ADFCCFEF669C91EC4FF006EBBFF009FA9FF00EFE1A3EDD77FF3F53FFDFC3505147D62B7F33FBD87247B13FDBAEFFE7EA7FF00BF868FB75DFF00CFD4FF00F7F0D41451F58ADFCCFEF61C91EC4FF6EBBFF9FA9FFEFE1A3EDD77FF003F53FF00DFC3505147D62B7F33FBD87247B13FDBAEFF00E7EA7FFBF868FB75DFFCFD4FFF007F0D41451F58ADFCCFEF61C91EC5FD67FE42D3FF00C07FF411542AFEB3FF002169FF00E03FFA08AA15B661FEF757FC52FCD9347F851F44145145719A05145140051451400514514017F46FF90B41FF0002FF00D04D50ABFA37FC85A0FF00817FE826A85764FF00DD21FE297E50335FC57E8BF50A28A2B8CD028A28A0028A28A0028A28A0028A28A00BFACFFC85A7FF0080FF00E822A855FD67FE42D3FF00C07FF411542BB330FF007BABFE297E6CCE8FF0A3E8828A28AE3340A28A2800A28A2800A28A2802FE8DFF0021683FE05FFA09AA157F46FF0090B41FF02FFD04D50AEC9FFBA43FC52FCA066BF8AFD17EA145145719A0514514005145140051451400514514017F59FF0090B4FF00F01FFD04550ABFACFF00C85A7FF80FFE822A8576661FEF757FC52FCD99D1FE147D10514515C6681451450014514500145145005FD1BFE42D07FC0BFF00413542AFE8DFF21683FE05FF00A09AA15D93FF007487F8A5F940CD7F15FA2FD428A28AE3340A28A2800A28A2800A28A2800A28A2802FEB3FF2169FFE03FF00A08AA157F59FF90B4FFF0001FF00D04550AECCC3FDEEAFF8A5F9B33A3FC28FA20A28A2B8CD028A28A0028A28A0028A28A00BFA37FC85A0FF00817FE826A855FD1BFE42D07FC0BFF413542BB27FEE90FF0014BF2819AFE2BF45FA8514515C668145145001451450014514500145145005FD67FE42D3FF00C07FF4115428A2BB330FF7BABFE297E6CCE8FF000A3E8828A28AE3340A28A2800A28A2800A28A2802FE8DFF21683FE05FF00A09AA14515D93FF7487F8A5F940CD7F15FA2FD428A28AE3340A28A2800A28A2800A28A2800A28A2803FFD9)
INSERT [dbo].[Establecimientos] ([IdEstablecimiento], [Nombre], [PrecioAlquiler], [CapacidadTotal], [Imagen]) VALUES (27, N'z4', CAST(23.00 AS Decimal(18, 2)), 444, 0xFFD8FFE000104A46494600010101006000600000FFDB004300080606070605080707070909080A0C140D0C0B0B0C1912130F141D1A1F1E1D1A1C1C20242E2720222C231C1C2837292C30313434341F27393D38323C2E333432FFDB0043010909090C0B0C180D0D1832211C213232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232323232FFC0001108005100AC03012200021101031101FFC4001F0000010501010101010100000000000000000102030405060708090A0BFFC400B5100002010303020403050504040000017D01020300041105122131410613516107227114328191A1082342B1C11552D1F02433627282090A161718191A25262728292A3435363738393A434445464748494A535455565758595A636465666768696A737475767778797A838485868788898A92939495969798999AA2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE1E2E3E4E5E6E7E8E9EAF1F2F3F4F5F6F7F8F9FAFFC4001F0100030101010101010101010000000000000102030405060708090A0BFFC400B51100020102040403040705040400010277000102031104052131061241510761711322328108144291A1B1C109233352F0156272D10A162434E125F11718191A262728292A35363738393A434445464748494A535455565758595A636465666768696A737475767778797A82838485868788898A92939495969798999AA2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE2E3E4E5E6E7E8E9EAF2F3F4F5F6F7F8F9FAFFDA000C03010002110311003F00F58D5AEAE23D4E648E795546DC057200F94552FB75DFFCFD4FFF007F0D58D67FE42D3FFC07FF00411542BF34C7D7AAB1755293F8A5D7CD9EFD1847D9C74E889FEDD77FF3F53FFDFC347DBAEFFE7EA7FF00BF86A0A2B93EB15BF99FDECD3923D89FEDD77FF3F53FFDFC347DBAEFFE7EA7FF00BF86A0A28FAC56FE67F7B0E48F627FB75DFF00CFD4FF00F7F0D1F6EBBFF9FA9FFEFE1A828A3EB15BF99FDEC3923D89FEDD77FF003F53FF00DFC347DBAEFF00E7EA7FFBF86A0A28FAC56FE67F7B0E48F635349BAB8935385249E5653BB219C907E5354BEDD77FF3F53FFDFC356346FF0090B41FF02FFD04D50AEB9D7ABF5483E67F14BAF940CD423ED1E9D17EA4FF006EBBFF009FA9FF00EFE1A3EDD77FF3F53FFDFC350515C9F58ADFCCFEF669C91EC4FF006EBBFF009FA9FF00EFE1A3EDD77FF3F53FFDFC3505147D62B7F33FBD87247B13FDBAEFFE7EA7FF00BF868FB75DFF00CFD4FF00F7F0D41451F58ADFCCFEF61C91EC4FF6EBBFF9FA9FFEFE1A3EDD77FF003F53FF00DFC3505147D62B7F33FBD87247B13FDBAEFF00E7EA7FFBF868FB75DFFCFD4FFF007F0D41451F58ADFCCFEF61C91EC5FD67FE42D3FF00C07FF411542AFEB3FF002169FF00E03FFA08AA15B661FEF757FC52FCD9347F851F44145145719A05145140051451400514514017F46FF90B41FF0002FF00D04D50ABFA37FC85A0FF00817FE826A85764FF00DD21FE297E50335FC57E8BF50A28A2B8CD028A28A0028A28A0028A28A0028A28A00BFACFFC85A7FF0080FF00E822A855FD67FE42D3FF00C07FF411542BB330FF007BABFE297E6CCE8FF0A3E8828A28AE3340A28A2800A28A2800A28A2802FE8DFF0021683FE05FFA09AA157F46FF0090B41FF02FFD04D50AEC9FFBA43FC52FCA066BF8AFD17EA145145719A0514514005145140051451400514514017F59FF0090B4FF00F01FFD04550ABFACFF00C85A7FF80FFE822A8576661FEF757FC52FCD99D1FE147D10514515C6681451450014514500145145005FD1BFE42D07FC0BFF00413542AFE8DFF21683FE05FF00A09AA15D93FF007487F8A5F940CD7F15FA2FD428A28AE3340A28A2800A28A2800A28A2800A28A2802FEB3FF2169FFE03FF00A08AA157F59FF90B4FFF0001FF00D04550AECCC3FDEEAFF8A5F9B33A3FC28FA20A28A2B8CD028A28A0028A28A0028A28A00BFA37FC85A0FF00817FE826A855FD1BFE42D07FC0BFF413542BB27FEE90FF0014BF2819AFE2BF45FA8514515C668145145001451450014514500145145005FD67FE42D3FF00C07FF4115428A2BB330FF7BABFE297E6CCE8FF000A3E8828A28AE3340A28A2800A28A2800A28A2802FE8DFF21683FE05FF00A09AA14515D93FF7487F8A5F940CD7F15FA2FD428A28AE3340A28A2800A28A2800A28A2800A28A2803FFD9)
SET IDENTITY_INSERT [dbo].[Establecimientos] OFF
/****** Object:  Table [dbo].[ZonasEnvios]    Script Date: 11/24/2014 16:16:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ZonasEnvios](
	[IdZona] [bigint] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Precio] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_ZonasEnvios] PRIMARY KEY CLUSTERED 
(
	[IdZona] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[ZonasEnvios] ON
INSERT [dbo].[ZonasEnvios] ([IdZona], [Nombre], [Precio]) VALUES (1, N'nnn', CAST(2323.00 AS Decimal(18, 2)))
SET IDENTITY_INSERT [dbo].[ZonasEnvios] OFF
/****** Object:  Table [dbo].[Sectores]    Script Date: 11/24/2014 16:16:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sectores](
	[IdSector] [bigint] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[capacidad] [int] NOT NULL,
	[idEstablecimiento] [bigint] NOT NULL,
	[Precio] [numeric](18, 2) NOT NULL,
 CONSTRAINT [PK_Sectores] PRIMARY KEY CLUSTERED 
(
	[IdSector] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Sectores] ON
INSERT [dbo].[Sectores] ([IdSector], [Descripcion], [capacidad], [idEstablecimiento], [Precio]) VALUES (1, N'Campo', 300, 1, CAST(100.00 AS Numeric(18, 2)))
INSERT [dbo].[Sectores] ([IdSector], [Descripcion], [capacidad], [idEstablecimiento], [Precio]) VALUES (2, N'Platea', 300, 1, CAST(200.00 AS Numeric(18, 2)))
INSERT [dbo].[Sectores] ([IdSector], [Descripcion], [capacidad], [idEstablecimiento], [Precio]) VALUES (3, N'gf', 7, 3, CAST(12.00 AS Numeric(18, 2)))
INSERT [dbo].[Sectores] ([IdSector], [Descripcion], [capacidad], [idEstablecimiento], [Precio]) VALUES (4, N'cc', 3, 3, CAST(8.00 AS Numeric(18, 2)))
INSERT [dbo].[Sectores] ([IdSector], [Descripcion], [capacidad], [idEstablecimiento], [Precio]) VALUES (5, N'g', 4, 4, CAST(7.00 AS Numeric(18, 2)))
INSERT [dbo].[Sectores] ([IdSector], [Descripcion], [capacidad], [idEstablecimiento], [Precio]) VALUES (7, N'm', 23, 1, CAST(322.00 AS Numeric(18, 2)))
INSERT [dbo].[Sectores] ([IdSector], [Descripcion], [capacidad], [idEstablecimiento], [Precio]) VALUES (8, N'd', 2, 1, CAST(2.00 AS Numeric(18, 2)))
INSERT [dbo].[Sectores] ([IdSector], [Descripcion], [capacidad], [idEstablecimiento], [Precio]) VALUES (54, N'sec1', 3, 26, CAST(3.00 AS Numeric(18, 2)))
INSERT [dbo].[Sectores] ([IdSector], [Descripcion], [capacidad], [idEstablecimiento], [Precio]) VALUES (55, N'sec2', 4, 26, CAST(4.00 AS Numeric(18, 2)))
INSERT [dbo].[Sectores] ([IdSector], [Descripcion], [capacidad], [idEstablecimiento], [Precio]) VALUES (56, N'sec1', 23, 27, CAST(132.00 AS Numeric(18, 2)))
INSERT [dbo].[Sectores] ([IdSector], [Descripcion], [capacidad], [idEstablecimiento], [Precio]) VALUES (57, N'sec2', 33, 27, CAST(23.00 AS Numeric(18, 2)))
SET IDENTITY_INSERT [dbo].[Sectores] OFF
/****** Object:  Table [dbo].[Usuarios]    Script Date: 11/24/2014 16:16:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Usuarios](
	[IdUsuario] [bigint] IDENTITY(1,1) NOT NULL,
	[IdPerfil] [bigint] NOT NULL,
	[NombreUsuario] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[Email] [nvarchar](100) NOT NULL,
	[Nombre] [nvarchar](100) NOT NULL,
	[Apellido] [nvarchar](100) NOT NULL,
	[DNI] [nvarchar](13) NOT NULL,
	[Calle] [nvarchar](255) NULL,
	[IdProvincia] [bigint] NULL,
	[CP] [bigint] NULL,
	[NroTarjeta] [nvarchar](30) NULL,
	[FechaVencimientoTarjeta] [smalldatetime] NULL,
	[IsActivo] [bit] NOT NULL,
	[NombreLegal] [nvarchar](100) NULL,
	[NumeroCalle] [bigint] NULL,
	[Localidad] [varchar](255) NULL,
	[IdZonaEnvio] [bigint] NULL,
	[CodigoPostal] [int] NULL,
	[Piso] [int] NULL,
	[Departamento] [varchar](50) NULL,
	[CantidadCancelaciones] [int] NULL,
 CONSTRAINT [PK_Usuarios] PRIMARY KEY CLUSTERED 
(
	[IdUsuario] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Usuarios] ON
INSERT [dbo].[Usuarios] ([IdUsuario], [IdPerfil], [NombreUsuario], [Password], [Email], [Nombre], [Apellido], [DNI], [Calle], [IdProvincia], [CP], [NroTarjeta], [FechaVencimientoTarjeta], [IsActivo], [NombreLegal], [NumeroCalle], [Localidad], [IdZonaEnvio], [CodigoPostal], [Piso], [Departamento], [CantidadCancelaciones]) VALUES (2, 3, N'pau', N'123456', N'prueba@yahoo.com', N'Pau', N'P', N'454564565', NULL, 1, 1846, N'94541545443', CAST(0xA9860000 AS SmallDateTime), 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Usuarios] ([IdUsuario], [IdPerfil], [NombreUsuario], [Password], [Email], [Nombre], [Apellido], [DNI], [Calle], [IdProvincia], [CP], [NroTarjeta], [FechaVencimientoTarjeta], [IsActivo], [NombreLegal], [NumeroCalle], [Localidad], [IdZonaEnvio], [CodigoPostal], [Piso], [Departamento], [CantidadCancelaciones]) VALUES (3, 1, N'nn', N'nnnnnn', N'jaja@hotmail.com', N'Pau', N'P', N'554545459', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Usuarios] ([IdUsuario], [IdPerfil], [NombreUsuario], [Password], [Email], [Nombre], [Apellido], [DNI], [Calle], [IdProvincia], [CP], [NroTarjeta], [FechaVencimientoTarjeta], [IsActivo], [NombreLegal], [NumeroCalle], [Localidad], [IdZonaEnvio], [CodigoPostal], [Piso], [Departamento], [CantidadCancelaciones]) VALUES (10, 3, N'u', N'uuuuuu', N'u', N'u', N'u', N'332232232', N'h', 1, 23, N'1232', CAST(0xA4780000 AS SmallDateTime), 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Usuarios] ([IdUsuario], [IdPerfil], [NombreUsuario], [Password], [Email], [Nombre], [Apellido], [DNI], [Calle], [IdProvincia], [CP], [NroTarjeta], [FechaVencimientoTarjeta], [IsActivo], [NombreLegal], [NumeroCalle], [Localidad], [IdZonaEnvio], [CodigoPostal], [Piso], [Departamento], [CantidadCancelaciones]) VALUES (11, 1, N'adm', N'uuuuuu', N'n', N'n', N'n', N'323233223', NULL, NULL, NULL, NULL, NULL, 1, N'n', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Usuarios] ([IdUsuario], [IdPerfil], [NombreUsuario], [Password], [Email], [Nombre], [Apellido], [DNI], [Calle], [IdProvincia], [CP], [NroTarjeta], [FechaVencimientoTarjeta], [IsActivo], [NombreLegal], [NumeroCalle], [Localidad], [IdZonaEnvio], [CodigoPostal], [Piso], [Departamento], [CantidadCancelaciones]) VALUES (12, 3, N'a', N'uuuuuu', N'a', N'a', N'a', N'23232323232', N'a', 1, 1222, N'2324343', CAST(0xA4780000 AS SmallDateTime), 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Usuarios] ([IdUsuario], [IdPerfil], [NombreUsuario], [Password], [Email], [Nombre], [Apellido], [DNI], [Calle], [IdProvincia], [CP], [NroTarjeta], [FechaVencimientoTarjeta], [IsActivo], [NombreLegal], [NumeroCalle], [Localidad], [IdZonaEnvio], [CodigoPostal], [Piso], [Departamento], [CantidadCancelaciones]) VALUES (13, 3, N'q', N'uuuuuu', N'q', N'q', N'q', N'1121212121', N'q', 1, 1231, N'23232323', CAST(0xA5AB0000 AS SmallDateTime), 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[Usuarios] ([IdUsuario], [IdPerfil], [NombreUsuario], [Password], [Email], [Nombre], [Apellido], [DNI], [Calle], [IdProvincia], [CP], [NroTarjeta], [FechaVencimientoTarjeta], [IsActivo], [NombreLegal], [NumeroCalle], [Localidad], [IdZonaEnvio], [CodigoPostal], [Piso], [Departamento], [CantidadCancelaciones]) VALUES (19, 3, N'p', N'uuuuuu', N'u', N'u', N'u', N'767687', NULL, 1, 7676, N'12345', CAST(0xA7570000 AS SmallDateTime), 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Usuarios] ([IdUsuario], [IdPerfil], [NombreUsuario], [Password], [Email], [Nombre], [Apellido], [DNI], [Calle], [IdProvincia], [CP], [NroTarjeta], [FechaVencimientoTarjeta], [IsActivo], [NombreLegal], [NumeroCalle], [Localidad], [IdZonaEnvio], [CodigoPostal], [Piso], [Departamento], [CantidadCancelaciones]) VALUES (20, 3, N'juan', N'123456', N'juan@alfo.com', N'juan', N'perz', N'123343', NULL, 1, 1832, N'12324', CAST(0xA30B0000 AS SmallDateTime), 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Usuarios] OFF
/****** Object:  Table [dbo].[Tarjetas]    Script Date: 11/24/2014 16:16:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tarjetas](
	[IdTarjeta] [bigint] IDENTITY(1,1) NOT NULL,
	[NroTarjeta] [varchar](50) NOT NULL,
	[IdUsuario] [bigint] NOT NULL,
	[FechaVencimiento] [smalldatetime] NOT NULL,
	[IsPrincipal] [bit] NULL,
	[FechaAlta] [datetime] NULL,
 CONSTRAINT [PK_Tarjetas] PRIMARY KEY CLUSTERED 
(
	[IdTarjeta] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Tarjetas] ON
INSERT [dbo].[Tarjetas] ([IdTarjeta], [NroTarjeta], [IdUsuario], [FechaVencimiento], [IsPrincipal], [FechaAlta]) VALUES (0, N'23232323', 13, CAST(0xA5AB0000 AS SmallDateTime), NULL, CAST(0x0000A3EB00D38FCC AS DateTime))
INSERT [dbo].[Tarjetas] ([IdTarjeta], [NroTarjeta], [IdUsuario], [FechaVencimiento], [IsPrincipal], [FechaAlta]) VALUES (2, N'12345', 19, CAST(0xA7570000 AS SmallDateTime), 1, NULL)
INSERT [dbo].[Tarjetas] ([IdTarjeta], [NroTarjeta], [IdUsuario], [FechaVencimiento], [IsPrincipal], [FechaAlta]) VALUES (3, N'12324', 20, CAST(0xA30B0000 AS SmallDateTime), 1, CAST(0x0000A3E400B17C76 AS DateTime))
INSERT [dbo].[Tarjetas] ([IdTarjeta], [NroTarjeta], [IdUsuario], [FechaVencimiento], [IsPrincipal], [FechaAlta]) VALUES (4, N'768768', 13, CAST(0xA5C70000 AS SmallDateTime), NULL, CAST(0x0000A3EB00CF7E2B AS DateTime))
SET IDENTITY_INSERT [dbo].[Tarjetas] OFF
/****** Object:  Table [dbo].[Shows]    Script Date: 11/24/2014 16:16:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Shows](
	[IdShow] [bigint] IDENTITY(1,1) NOT NULL,
	[IdEstablecimiento] [bigint] NOT NULL,
	[IdUsuario] [bigint] NULL,
	[Artista] [varchar](50) NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[FechaHora] [smalldatetime] NOT NULL,
	[IsAprobado] [bit] NOT NULL,
	[Imagen] [varchar](500) NULL,
	[IsCarrusel] [bit] NOT NULL,
	[IsCancelado] [bit] NULL,
 CONSTRAINT [PK_Shows] PRIMARY KEY CLUSTERED 
(
	[IdShow] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Shows] ON
INSERT [dbo].[Shows] ([IdShow], [IdEstablecimiento], [IdUsuario], [Artista], [Descripcion], [FechaHora], [IsAprobado], [Imagen], [IsCarrusel], [IsCancelado]) VALUES (4, 27, 3, N'Eruca Sativa', N'Rock', CAST(0xA40704B0 AS SmallDateTime), 1, N'..\..\Content\Images\eruca.jpg', 1, NULL)
INSERT [dbo].[Shows] ([IdShow], [IdEstablecimiento], [IdUsuario], [Artista], [Descripcion], [FechaHora], [IsAprobado], [Imagen], [IsCarrusel], [IsCancelado]) VALUES (5, 1, 3, N'Lisandro', N'Rock Pop', CAST(0xA40104EC AS SmallDateTime), 1, N'C:\Users\Martin\Downloads\ProyectoSupervisada\practicasupervisada\MvcEntradas\MvcEntradas\Content\Images\lisandro.jpg', 1, NULL)
INSERT [dbo].[Shows] ([IdShow], [IdEstablecimiento], [IdUsuario], [Artista], [Descripcion], [FechaHora], [IsAprobado], [Imagen], [IsCarrusel], [IsCancelado]) VALUES (6, 1, 3, N'Lisandro Aristimuño', N'Rock, Pop', CAST(0xA40104EC AS SmallDateTime), 1, N'C:\Users\PC\UTN\Sexto Cuatri\Practica Supervisada II\practicasupervisada\MvcEntradas\MvcEntradas\Content\Images\lisandro.jpg', 1, NULL)
INSERT [dbo].[Shows] ([IdShow], [IdEstablecimiento], [IdUsuario], [Artista], [Descripcion], [FechaHora], [IsAprobado], [Imagen], [IsCarrusel], [IsCancelado]) VALUES (7, 1, 3, N'Muse', N'Rock', CAST(0xA41304EC AS SmallDateTime), 1, N'..\..\Content\Images\Muse.jpg', 1, NULL)
SET IDENTITY_INSERT [dbo].[Shows] OFF
/****** Object:  Table [dbo].[Domicilio]    Script Date: 11/24/2014 16:16:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Domicilio](
	[IdDomicilio] [bigint] IDENTITY(1,1) NOT NULL,
	[Calle] [varchar](255) NOT NULL,
	[Numero] [bigint] NOT NULL,
	[Localidad] [varchar](255) NOT NULL,
	[IdZonaEnvio] [bigint] NOT NULL,
	[IdUsuario] [bigint] NOT NULL,
	[CodigoPostal] [int] NOT NULL,
	[Piso] [int] NULL,
	[Departamento] [varchar](50) NULL,
	[IsPrincipal] [bit] NULL,
	[FechaAlta] [datetime] NULL,
 CONSTRAINT [PK_Domicilio] PRIMARY KEY CLUSTERED 
(
	[IdDomicilio] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Domicilio] ON
INSERT [dbo].[Domicilio] ([IdDomicilio], [Calle], [Numero], [Localidad], [IdZonaEnvio], [IdUsuario], [CodigoPostal], [Piso], [Departamento], [IsPrincipal], [FechaAlta]) VALUES (1, N'Algo', 5345, N'Ba', 1, 2, 565, NULL, NULL, NULL, NULL)
INSERT [dbo].[Domicilio] ([IdDomicilio], [Calle], [Numero], [Localidad], [IdZonaEnvio], [IdUsuario], [CodigoPostal], [Piso], [Departamento], [IsPrincipal], [FechaAlta]) VALUES (2, N'h', 5, N'hg', 1, 2, 5, NULL, NULL, NULL, NULL)
INSERT [dbo].[Domicilio] ([IdDomicilio], [Calle], [Numero], [Localidad], [IdZonaEnvio], [IdUsuario], [CodigoPostal], [Piso], [Departamento], [IsPrincipal], [FechaAlta]) VALUES (3, N'o', 212, N'oo', 1, 13, 1211, 12, N'12', NULL, NULL)
INSERT [dbo].[Domicilio] ([IdDomicilio], [Calle], [Numero], [Localidad], [IdZonaEnvio], [IdUsuario], [CodigoPostal], [Piso], [Departamento], [IsPrincipal], [FechaAlta]) VALUES (4, N'dd', 212, N'jjjj', 1, 13, 1211, 12, N'12', NULL, CAST(0x0000A3EB00D30DC2 AS DateTime))
INSERT [dbo].[Domicilio] ([IdDomicilio], [Calle], [Numero], [Localidad], [IdZonaEnvio], [IdUsuario], [CodigoPostal], [Piso], [Departamento], [IsPrincipal], [FechaAlta]) VALUES (5, N'h', 656, N'jhjj', 1, 13, 878, 7, N'78', NULL, NULL)
INSERT [dbo].[Domicilio] ([IdDomicilio], [Calle], [Numero], [Localidad], [IdZonaEnvio], [IdUsuario], [CodigoPostal], [Piso], [Departamento], [IsPrincipal], [FechaAlta]) VALUES (6, N'n', 34, N'mm', 1, 13, 87, 7, N'8', NULL, NULL)
SET IDENTITY_INSERT [dbo].[Domicilio] OFF
/****** Object:  Table [dbo].[Compras]    Script Date: 11/24/2014 16:16:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Compras](
	[IdCompra] [bigint] IDENTITY(1,1) NOT NULL,
	[IdUsuario] [bigint] NOT NULL,
	[IdShow] [bigint] NOT NULL,
	[FormaDePago] [varchar](50) NOT NULL,
	[IsEnvio] [bit] NOT NULL,
	[CantidadEnvios] [int] NOT NULL,
	[IdZonaEnvio] [bigint] NULL,
	[Cantidad] [int] NOT NULL,
	[IdEstado] [bigint] NOT NULL,
	[FechaHora] [smalldatetime] NOT NULL,
	[Importe] [decimal](18, 2) NOT NULL,
	[IsEntregado] [bit] NOT NULL,
	[IdSector] [bigint] NOT NULL,
	[Tarjeta] [varchar](50) NULL,
	[IdDomicilio] [bigint] NULL,
	[FechaEnvio] [smalldatetime] NULL,
	[CambioFechaEnvio] [bit] NULL,
 CONSTRAINT [PK_Compras] PRIMARY KEY CLUSTERED 
(
	[IdCompra] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Compras] ON
INSERT [dbo].[Compras] ([IdCompra], [IdUsuario], [IdShow], [FormaDePago], [IsEnvio], [CantidadEnvios], [IdZonaEnvio], [Cantidad], [IdEstado], [FechaHora], [Importe], [IsEntregado], [IdSector], [Tarjeta], [IdDomicilio], [FechaEnvio]) VALUES (3, 13, 5, N'Efectivo', 1, 0, 1, 123, 1, CAST(0xA3E4049C AS SmallDateTime), CAST(39606.00 AS Decimal(18, 2)), 0, 7, NULL, NULL, NULL)
INSERT [dbo].[Compras] ([IdCompra], [IdUsuario], [IdShow], [FormaDePago], [IsEnvio], [CantidadEnvios], [IdZonaEnvio], [Cantidad], [IdEstado], [FechaHora], [Importe], [IsEntregado], [IdSector], [Tarjeta], [IdDomicilio], [FechaEnvio]) VALUES (6, 13, 4, N'Efectivo', 0, 0, 1, 1, 1, CAST(0xA3E404A2 AS SmallDateTime), CAST(322.00 AS Decimal(18, 2)), 0, 7, NULL, NULL, NULL)
INSERT [dbo].[Compras] ([IdCompra], [IdUsuario], [IdShow], [FormaDePago], [IsEnvio], [CantidadEnvios], [IdZonaEnvio], [Cantidad], [IdEstado], [FechaHora], [Importe], [IsEntregado], [IdSector], [Tarjeta], [IdDomicilio], [FechaEnvio]) VALUES (7, 13, 4, N'Efectivo', 0, 0, 1, 1, 1, CAST(0xA3E404B8 AS SmallDateTime), CAST(322.00 AS Decimal(18, 2)), 0, 7, NULL, NULL, NULL)
INSERT [dbo].[Compras] ([IdCompra], [IdUsuario], [IdShow], [FormaDePago], [IsEnvio], [CantidadEnvios], [IdZonaEnvio], [Cantidad], [IdEstado], [FechaHora], [Importe], [IsEntregado], [IdSector], [Tarjeta], [IdDomicilio], [FechaEnvio]) VALUES (8, 13, 5, N'Efectivo', 1, 0, 1, 123, 1, CAST(0xA3E404C4 AS SmallDateTime), CAST(39606.00 AS Decimal(18, 2)), 0, 7, NULL, NULL, NULL)
INSERT [dbo].[Compras] ([IdCompra], [IdUsuario], [IdShow], [FormaDePago], [IsEnvio], [CantidadEnvios], [IdZonaEnvio], [Cantidad], [IdEstado], [FechaHora], [Importe], [IsEntregado], [IdSector], [Tarjeta], [IdDomicilio], [FechaEnvio]) VALUES (9, 13, 5, N'Efectivo', 0, 0, 1, 87, 0, CAST(0xA3E501CB AS SmallDateTime), CAST(28014.00 AS Decimal(18, 2)), 0, 7, N'23232323', NULL, NULL)
INSERT [dbo].[Compras] ([IdCompra], [IdUsuario], [IdShow], [FormaDePago], [IsEnvio], [CantidadEnvios], [IdZonaEnvio], [Cantidad], [IdEstado], [FechaHora], [Importe], [IsEntregado], [IdSector], [Tarjeta], [IdDomicilio], [FechaEnvio]) VALUES (10, 13, 5, N'Efectivo', 1, 0, 1, 87, 1, CAST(0xA3E501CC AS SmallDateTime), CAST(28014.00 AS Decimal(18, 2)), 0, 7, N'23232323', NULL, NULL)
INSERT [dbo].[Compras] ([IdCompra], [IdUsuario], [IdShow], [FormaDePago], [IsEnvio], [CantidadEnvios], [IdZonaEnvio], [Cantidad], [IdEstado], [FechaHora], [Importe], [IsEntregado], [IdSector], [Tarjeta], [IdDomicilio], [FechaEnvio]) VALUES (11, 13, 4, N'Efectivo', 1, 0, 1, 12, 1, CAST(0xA3E501EE AS SmallDateTime), CAST(3864.00 AS Decimal(18, 2)), 0, 7, N'23232323', NULL, NULL)
INSERT [dbo].[Compras] ([IdCompra], [IdUsuario], [IdShow], [FormaDePago], [IsEnvio], [CantidadEnvios], [IdZonaEnvio], [Cantidad], [IdEstado], [FechaHora], [Importe], [IsEntregado], [IdSector], [Tarjeta], [IdDomicilio], [FechaEnvio]) VALUES (12, 13, 4, N'Tarjeta de credito', 1, 0, 1, 2, 1, CAST(0xA3EB023B AS SmallDateTime), CAST(200.00 AS Decimal(18, 2)), 0, 1, N'768768', 4, NULL)
INSERT [dbo].[Compras] ([IdCompra], [IdUsuario], [IdShow], [FormaDePago], [IsEnvio], [CantidadEnvios], [IdZonaEnvio], [Cantidad], [IdEstado], [FechaHora], [Importe], [IsEntregado], [IdSector], [Tarjeta], [IdDomicilio], [FechaEnvio]) VALUES (16, 13, 4, N'Efectivo', 1, 0, 1, 1, 1, CAST(0xA3EB028F AS SmallDateTime), CAST(200.00 AS Decimal(18, 2)), 0, 2, N'768768', 4, CAST(0xA3F80000 AS SmallDateTime))
INSERT [dbo].[Compras] ([IdCompra], [IdUsuario], [IdShow], [FormaDePago], [IsEnvio], [CantidadEnvios], [IdZonaEnvio], [Cantidad], [IdEstado], [FechaHora], [Importe], [IsEntregado], [IdSector], [Tarjeta], [IdDomicilio], [FechaEnvio]) VALUES (17, 13, 7, N'Tarjeta de credito', 1, 0, 1, 6, 3, CAST(0xA3EB0300 AS SmallDateTime), CAST(1200.00 AS Decimal(18, 2)), 0, 2, N'23232323', 4, CAST(0xAC010000 AS SmallDateTime))
SET IDENTITY_INSERT [dbo].[Compras] OFF
/****** Object:  ForeignKey [FK_Sectores_Establecimientos1]    Script Date: 11/24/2014 16:16:26 ******/
ALTER TABLE [dbo].[Sectores]  WITH CHECK ADD  CONSTRAINT [FK_Sectores_Establecimientos1] FOREIGN KEY([idEstablecimiento])
REFERENCES [dbo].[Establecimientos] ([IdEstablecimiento])
GO
ALTER TABLE [dbo].[Sectores] CHECK CONSTRAINT [FK_Sectores_Establecimientos1]
GO
/****** Object:  ForeignKey [FK_Usuario_Perfil]    Script Date: 11/24/2014 16:16:26 ******/
ALTER TABLE [dbo].[Usuarios]  WITH CHECK ADD  CONSTRAINT [FK_Usuario_Perfil] FOREIGN KEY([IdPerfil])
REFERENCES [dbo].[Perfiles] ([IdPerfil])
GO
ALTER TABLE [dbo].[Usuarios] CHECK CONSTRAINT [FK_Usuario_Perfil]
GO
/****** Object:  ForeignKey [FK_Usuario_Provincia]    Script Date: 11/24/2014 16:16:26 ******/
ALTER TABLE [dbo].[Usuarios]  WITH CHECK ADD  CONSTRAINT [FK_Usuario_Provincia] FOREIGN KEY([IdProvincia])
REFERENCES [dbo].[Provincias] ([IdProvincia])
GO
ALTER TABLE [dbo].[Usuarios] CHECK CONSTRAINT [FK_Usuario_Provincia]
GO
/****** Object:  ForeignKey [FK_Usuarios_ZonasEnvios]    Script Date: 11/24/2014 16:16:26 ******/
ALTER TABLE [dbo].[Usuarios]  WITH CHECK ADD  CONSTRAINT [FK_Usuarios_ZonasEnvios] FOREIGN KEY([IdZonaEnvio])
REFERENCES [dbo].[ZonasEnvios] ([IdZona])
GO
ALTER TABLE [dbo].[Usuarios] CHECK CONSTRAINT [FK_Usuarios_ZonasEnvios]
GO
/****** Object:  ForeignKey [FK_Tarjetas_Usuarios]    Script Date: 11/24/2014 16:16:26 ******/
ALTER TABLE [dbo].[Tarjetas]  WITH CHECK ADD  CONSTRAINT [FK_Tarjetas_Usuarios] FOREIGN KEY([IdUsuario])
REFERENCES [dbo].[Usuarios] ([IdUsuario])
GO
ALTER TABLE [dbo].[Tarjetas] CHECK CONSTRAINT [FK_Tarjetas_Usuarios]
GO
/****** Object:  ForeignKey [FK_Show_Establecimiento]    Script Date: 11/24/2014 16:16:26 ******/
ALTER TABLE [dbo].[Shows]  WITH CHECK ADD  CONSTRAINT [FK_Show_Establecimiento] FOREIGN KEY([IdEstablecimiento])
REFERENCES [dbo].[Establecimientos] ([IdEstablecimiento])
GO
ALTER TABLE [dbo].[Shows] CHECK CONSTRAINT [FK_Show_Establecimiento]
GO
/****** Object:  ForeignKey [FK_Show_Usuario]    Script Date: 11/24/2014 16:16:26 ******/
ALTER TABLE [dbo].[Shows]  WITH CHECK ADD  CONSTRAINT [FK_Show_Usuario] FOREIGN KEY([IdUsuario])
REFERENCES [dbo].[Usuarios] ([IdUsuario])
GO
ALTER TABLE [dbo].[Shows] CHECK CONSTRAINT [FK_Show_Usuario]
GO
/****** Object:  ForeignKey [FK_Domicilio_Usuarios]    Script Date: 11/24/2014 16:16:26 ******/
ALTER TABLE [dbo].[Domicilio]  WITH CHECK ADD  CONSTRAINT [FK_Domicilio_Usuarios] FOREIGN KEY([IdUsuario])
REFERENCES [dbo].[Usuarios] ([IdUsuario])
GO
ALTER TABLE [dbo].[Domicilio] CHECK CONSTRAINT [FK_Domicilio_Usuarios]
GO
/****** Object:  ForeignKey [FK_Domicilio_ZonasEnvios]    Script Date: 11/24/2014 16:16:26 ******/
ALTER TABLE [dbo].[Domicilio]  WITH CHECK ADD  CONSTRAINT [FK_Domicilio_ZonasEnvios] FOREIGN KEY([IdZonaEnvio])
REFERENCES [dbo].[ZonasEnvios] ([IdZona])
GO
ALTER TABLE [dbo].[Domicilio] CHECK CONSTRAINT [FK_Domicilio_ZonasEnvios]
GO
/****** Object:  ForeignKey [FK_Compras_Domicilio]    Script Date: 11/24/2014 16:16:26 ******/
ALTER TABLE [dbo].[Compras]  WITH CHECK ADD  CONSTRAINT [FK_Compras_Domicilio] FOREIGN KEY([IdDomicilio])
REFERENCES [dbo].[Domicilio] ([IdDomicilio])
GO
ALTER TABLE [dbo].[Compras] CHECK CONSTRAINT [FK_Compras_Domicilio]
GO
/****** Object:  ForeignKey [FK_Compras_Sectores]    Script Date: 11/24/2014 16:16:26 ******/
ALTER TABLE [dbo].[Compras]  WITH CHECK ADD  CONSTRAINT [FK_Compras_Sectores] FOREIGN KEY([IdSector])
REFERENCES [dbo].[Sectores] ([IdSector])
GO
ALTER TABLE [dbo].[Compras] CHECK CONSTRAINT [FK_Compras_Sectores]
GO
/****** Object:  ForeignKey [FK_Compras_Show]    Script Date: 11/24/2014 16:16:26 ******/
ALTER TABLE [dbo].[Compras]  WITH CHECK ADD  CONSTRAINT [FK_Compras_Show] FOREIGN KEY([IdShow])
REFERENCES [dbo].[Shows] ([IdShow])
GO
ALTER TABLE [dbo].[Compras] CHECK CONSTRAINT [FK_Compras_Show]
GO
/****** Object:  ForeignKey [FK_Compras_Usuario]    Script Date: 11/24/2014 16:16:26 ******/
ALTER TABLE [dbo].[Compras]  WITH CHECK ADD  CONSTRAINT [FK_Compras_Usuario] FOREIGN KEY([IdUsuario])
REFERENCES [dbo].[Usuarios] ([IdUsuario])
GO
ALTER TABLE [dbo].[Compras] CHECK CONSTRAINT [FK_Compras_Usuario]
GO
/****** Object:  ForeignKey [FK_Compras_ZonaEnvio]    Script Date: 11/24/2014 16:16:26 ******/
ALTER TABLE [dbo].[Compras]  WITH CHECK ADD  CONSTRAINT [FK_Compras_ZonaEnvio] FOREIGN KEY([IdZonaEnvio])
REFERENCES [dbo].[ZonasEnvios] ([IdZona])
GO
ALTER TABLE [dbo].[Compras] CHECK CONSTRAINT [FK_Compras_ZonaEnvio]
GO
