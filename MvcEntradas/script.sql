USE [VentaEntradas]
GO
/****** Object:  Table [dbo].[Establecimientos]    Script Date: 10/29/2014 23:25:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Establecimientos](
	[IdEstablecimiento] [bigint] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[PrecioAlquiler] [decimal](18, 2) NOT NULL,
	[CapacidadTotal] [bigint] NOT NULL,
 CONSTRAINT [PK_Establecimientos] PRIMARY KEY CLUSTERED 
(
	[IdEstablecimiento] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Establecimientos] ON
INSERT [dbo].[Establecimientos] ([IdEstablecimiento], [Nombre], [PrecioAlquiler], [CapacidadTotal]) VALUES (1, N'Primero', CAST(233.00 AS Decimal(18, 2)), 122)
SET IDENTITY_INSERT [dbo].[Establecimientos] OFF
/****** Object:  Table [dbo].[ZonasEnvios]    Script Date: 10/29/2014 23:25:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ZonasEnvios](
	[IdZona] [bigint] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Precio] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_ZonasEnvios] PRIMARY KEY CLUSTERED 
(
	[IdZona] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sectores]    Script Date: 10/29/2014 23:25:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sectores](
	[IdSector] [bigint] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Sectores] PRIMARY KEY CLUSTERED 
(
	[IdSector] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Provincias]    Script Date: 10/29/2014 23:25:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Provincias](
	[IdProvincia] [bigint] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Provincias] PRIMARY KEY CLUSTERED 
(
	[IdProvincia] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Provincias] ON
INSERT [dbo].[Provincias] ([IdProvincia], [Descripcion]) VALUES (1, N'Bs')
SET IDENTITY_INSERT [dbo].[Provincias] OFF
/****** Object:  Table [dbo].[Perfiles]    Script Date: 10/29/2014 23:25:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Perfiles](
	[IdPerfil] [bigint] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Perfiles] PRIMARY KEY CLUSTERED 
(
	[IdPerfil] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Perfiles] ON
INSERT [dbo].[Perfiles] ([IdPerfil], [Nombre]) VALUES (1, N'Administrador')
INSERT [dbo].[Perfiles] ([IdPerfil], [Nombre]) VALUES (2, N'Organizador')
INSERT [dbo].[Perfiles] ([IdPerfil], [Nombre]) VALUES (3, N'Comprador')
SET IDENTITY_INSERT [dbo].[Perfiles] OFF
/****** Object:  Table [dbo].[Estados]    Script Date: 10/29/2014 23:25:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Estados](
	[IdEstado] [bigint] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Estados] PRIMARY KEY CLUSTERED 
(
	[IdEstado] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EstablecimientosSectores]    Script Date: 10/29/2014 23:25:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EstablecimientosSectores](
	[IdEstablecimientoSector] [bigint] IDENTITY(1,1) NOT NULL,
	[IdEstablecimiento] [bigint] NOT NULL,
	[IdSector] [bigint] NOT NULL,
	[Cantidad] [int] NOT NULL,
	[Precio] [numeric](18, 2) NOT NULL,
 CONSTRAINT [PK_EstablecimientosSectores] PRIMARY KEY CLUSTERED 
(
	[IdEstablecimientoSector] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Usuarios]    Script Date: 10/29/2014 23:25:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuarios](
	[IdUsuario] [bigint] IDENTITY(1,1) NOT NULL,
	[IdPerfil] [bigint] NOT NULL,
	[NombreUsuario] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[Email] [nvarchar](100) NOT NULL,
	[Nombre] [nvarchar](100) NOT NULL,
	[Apellido] [nvarchar](100) NOT NULL,
	[DNI] [nvarchar](13) NOT NULL,
	[IdProvincia] [bigint] NULL,
	[CP] [bigint] NULL,
	[FechaVencimientoTarjeta] [smalldatetime] NULL,
	[IsActivo] [bit] NOT NULL,
	[NombreLegal] [nvarchar](100) NULL,
 CONSTRAINT [PK_Usuarios] PRIMARY KEY CLUSTERED 
(
	[IdUsuario] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Usuarios] ON
INSERT [dbo].[Usuarios] ([IdUsuario], [IdPerfil], [NombreUsuario], [Password], [Email], [Nombre], [Apellido], [DNI], [IdProvincia], [CP], [FechaVencimientoTarjeta], [IsActivo], [NombreLegal]) VALUES (6, 1, N'nn', N'nnnnnn', N'nn', N'nn', N'nn', N'232', 1, 2123, CAST(0x7DD40000 AS SmallDateTime), 1, NULL)
INSERT [dbo].[Usuarios] ([IdUsuario], [IdPerfil], [NombreUsuario], [Password], [Email], [Nombre], [Apellido], [DNI], [IdProvincia], [CP], [FechaVencimientoTarjeta], [IsActivo], [NombreLegal]) VALUES (7, 3, N'us', N'uuuuuu', N'usmail', N'us', N'us', N'234324', 1, 2333, CAST(0xA2B20000 AS SmallDateTime), 1, NULL)
SET IDENTITY_INSERT [dbo].[Usuarios] OFF
/****** Object:  Table [dbo].[Tarjetas]    Script Date: 10/29/2014 23:25:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tarjetas](
	[IdTarjeta] [bigint] NOT NULL,
	[NroTarjeta] [bigint] NOT NULL,
	[IdUsuario] [bigint] NOT NULL,
	[FechaVencimiento] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_Tarjetas] PRIMARY KEY CLUSTERED 
(
	[IdTarjeta] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Shows]    Script Date: 10/29/2014 23:25:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Shows](
	[IdShow] [bigint] IDENTITY(1,1) NOT NULL,
	[IdEstablecimiento] [bigint] NOT NULL,
	[IdUsuario] [bigint] NULL,
	[Artista] [varchar](50) NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[FechaHora] [smalldatetime] NOT NULL,
	[IsAprobado] [bit] NOT NULL,
	[Imagen] [varchar](500) NULL,
	[IsCarrusel] [bit] NOT NULL,
 CONSTRAINT [PK_Shows] PRIMARY KEY CLUSTERED 
(
	[IdShow] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Shows] ON
INSERT [dbo].[Shows] ([IdShow], [IdEstablecimiento], [IdUsuario], [Artista], [Descripcion], [FechaHora], [IsAprobado], [Imagen], [IsCarrusel]) VALUES (1, 1, NULL, N'las pelotas', N'rock', CAST(0xA3DF02D0 AS SmallDateTime), 1, NULL, 0)
INSERT [dbo].[Shows] ([IdShow], [IdEstablecimiento], [IdUsuario], [Artista], [Descripcion], [FechaHora], [IsAprobado], [Imagen], [IsCarrusel]) VALUES (4, 1, NULL, N'La vela puerca', N'rock', CAST(0xA3FF0000 AS SmallDateTime), 1, NULL, 1)
SET IDENTITY_INSERT [dbo].[Shows] OFF
/****** Object:  Table [dbo].[Domicilio]    Script Date: 10/29/2014 23:25:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Domicilio](
	[IdDomicilio] [bigint] NOT NULL,
	[Calle] [varchar](255) NOT NULL,
	[Numero] [bigint] NOT NULL,
	[Localidad] [varchar](255) NOT NULL,
	[IdZonaEnvio] [bigint] NOT NULL,
	[IdUsuario] [bigint] NOT NULL,
	[CodigoPostal] [int] NOT NULL,
	[Piso] [int] NULL,
	[Departamento] [varchar](50) NULL,
 CONSTRAINT [PK_Domicilio] PRIMARY KEY CLUSTERED 
(
	[IdDomicilio] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Compras]    Script Date: 10/29/2014 23:25:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Compras](
	[IdCompra] [bigint] IDENTITY(1,1) NOT NULL,
	[IdUsuario] [bigint] NOT NULL,
	[IdShow] [bigint] NOT NULL,
	[FormaDePago] [varchar](50) NOT NULL,
	[IsEnvio] [bit] NOT NULL,
	[CantidadEnvios] [int] NOT NULL,
	[IdZonaEnvio] [bigint] NOT NULL,
	[Cantidad] [int] NOT NULL,
	[IdEstado] [bigint] NOT NULL,
	[FechaHora] [smalldatetime] NOT NULL,
	[Importe] [decimal](18, 2) NOT NULL,
	[IsEntregado] [bit] NOT NULL,
	[IdSector] [bigint] NOT NULL,
 CONSTRAINT [PK_Compras] PRIMARY KEY CLUSTERED 
(
	[IdCompra] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  ForeignKey [FK_EstablecimientoSector_Establecimiento]    Script Date: 10/29/2014 23:25:25 ******/
ALTER TABLE [dbo].[EstablecimientosSectores]  WITH CHECK ADD  CONSTRAINT [FK_EstablecimientoSector_Establecimiento] FOREIGN KEY([IdEstablecimiento])
REFERENCES [dbo].[Establecimientos] ([IdEstablecimiento])
GO
ALTER TABLE [dbo].[EstablecimientosSectores] CHECK CONSTRAINT [FK_EstablecimientoSector_Establecimiento]
GO
/****** Object:  ForeignKey [FK_EstablecimientoSector_Sector]    Script Date: 10/29/2014 23:25:25 ******/
ALTER TABLE [dbo].[EstablecimientosSectores]  WITH CHECK ADD  CONSTRAINT [FK_EstablecimientoSector_Sector] FOREIGN KEY([IdSector])
REFERENCES [dbo].[Sectores] ([IdSector])
GO
ALTER TABLE [dbo].[EstablecimientosSectores] CHECK CONSTRAINT [FK_EstablecimientoSector_Sector]
GO
/****** Object:  ForeignKey [FK_Usuario_Perfil]    Script Date: 10/29/2014 23:25:25 ******/
ALTER TABLE [dbo].[Usuarios]  WITH CHECK ADD  CONSTRAINT [FK_Usuario_Perfil] FOREIGN KEY([IdPerfil])
REFERENCES [dbo].[Perfiles] ([IdPerfil])
GO
ALTER TABLE [dbo].[Usuarios] CHECK CONSTRAINT [FK_Usuario_Perfil]
GO
/****** Object:  ForeignKey [FK_Usuario_Provincia]    Script Date: 10/29/2014 23:25:25 ******/
ALTER TABLE [dbo].[Usuarios]  WITH CHECK ADD  CONSTRAINT [FK_Usuario_Provincia] FOREIGN KEY([IdProvincia])
REFERENCES [dbo].[Provincias] ([IdProvincia])
GO
ALTER TABLE [dbo].[Usuarios] CHECK CONSTRAINT [FK_Usuario_Provincia]
GO
/****** Object:  ForeignKey [FK_Tarjetas_Usuarios]    Script Date: 10/29/2014 23:25:25 ******/
ALTER TABLE [dbo].[Tarjetas]  WITH CHECK ADD  CONSTRAINT [FK_Tarjetas_Usuarios] FOREIGN KEY([IdUsuario])
REFERENCES [dbo].[Usuarios] ([IdUsuario])
GO
ALTER TABLE [dbo].[Tarjetas] CHECK CONSTRAINT [FK_Tarjetas_Usuarios]
GO
/****** Object:  ForeignKey [FK_Show_Establecimiento]    Script Date: 10/29/2014 23:25:25 ******/
ALTER TABLE [dbo].[Shows]  WITH CHECK ADD  CONSTRAINT [FK_Show_Establecimiento] FOREIGN KEY([IdEstablecimiento])
REFERENCES [dbo].[Establecimientos] ([IdEstablecimiento])
GO
ALTER TABLE [dbo].[Shows] CHECK CONSTRAINT [FK_Show_Establecimiento]
GO
/****** Object:  ForeignKey [FK_Show_Usuario]    Script Date: 10/29/2014 23:25:25 ******/
ALTER TABLE [dbo].[Shows]  WITH CHECK ADD  CONSTRAINT [FK_Show_Usuario] FOREIGN KEY([IdUsuario])
REFERENCES [dbo].[Usuarios] ([IdUsuario])
GO
ALTER TABLE [dbo].[Shows] CHECK CONSTRAINT [FK_Show_Usuario]
GO
/****** Object:  ForeignKey [FK_Domicilio_Usuarios]    Script Date: 10/29/2014 23:25:25 ******/
ALTER TABLE [dbo].[Domicilio]  WITH CHECK ADD  CONSTRAINT [FK_Domicilio_Usuarios] FOREIGN KEY([IdUsuario])
REFERENCES [dbo].[Usuarios] ([IdUsuario])
GO
ALTER TABLE [dbo].[Domicilio] CHECK CONSTRAINT [FK_Domicilio_Usuarios]
GO
/****** Object:  ForeignKey [FK_Domicilio_ZonasEnvios]    Script Date: 10/29/2014 23:25:25 ******/
ALTER TABLE [dbo].[Domicilio]  WITH CHECK ADD  CONSTRAINT [FK_Domicilio_ZonasEnvios] FOREIGN KEY([IdZonaEnvio])
REFERENCES [dbo].[ZonasEnvios] ([IdZona])
GO
ALTER TABLE [dbo].[Domicilio] CHECK CONSTRAINT [FK_Domicilio_ZonasEnvios]
GO
/****** Object:  ForeignKey [FK_Compras_Estado]    Script Date: 10/29/2014 23:25:25 ******/
ALTER TABLE [dbo].[Compras]  WITH CHECK ADD  CONSTRAINT [FK_Compras_Estado] FOREIGN KEY([IdEstado])
REFERENCES [dbo].[Estados] ([IdEstado])
GO
ALTER TABLE [dbo].[Compras] CHECK CONSTRAINT [FK_Compras_Estado]
GO
/****** Object:  ForeignKey [FK_Compras_Sectores]    Script Date: 10/29/2014 23:25:25 ******/
ALTER TABLE [dbo].[Compras]  WITH CHECK ADD  CONSTRAINT [FK_Compras_Sectores] FOREIGN KEY([IdSector])
REFERENCES [dbo].[Sectores] ([IdSector])
GO
ALTER TABLE [dbo].[Compras] CHECK CONSTRAINT [FK_Compras_Sectores]
GO
/****** Object:  ForeignKey [FK_Compras_Show]    Script Date: 10/29/2014 23:25:25 ******/
ALTER TABLE [dbo].[Compras]  WITH CHECK ADD  CONSTRAINT [FK_Compras_Show] FOREIGN KEY([IdShow])
REFERENCES [dbo].[Shows] ([IdShow])
GO
ALTER TABLE [dbo].[Compras] CHECK CONSTRAINT [FK_Compras_Show]
GO
/****** Object:  ForeignKey [FK_Compras_Usuario]    Script Date: 10/29/2014 23:25:25 ******/
ALTER TABLE [dbo].[Compras]  WITH CHECK ADD  CONSTRAINT [FK_Compras_Usuario] FOREIGN KEY([IdUsuario])
REFERENCES [dbo].[Usuarios] ([IdUsuario])
GO
ALTER TABLE [dbo].[Compras] CHECK CONSTRAINT [FK_Compras_Usuario]
GO
/****** Object:  ForeignKey [FK_Compras_ZonaEnvio]    Script Date: 10/29/2014 23:25:25 ******/
ALTER TABLE [dbo].[Compras]  WITH CHECK ADD  CONSTRAINT [FK_Compras_ZonaEnvio] FOREIGN KEY([IdZonaEnvio])
REFERENCES [dbo].[ZonasEnvios] ([IdZona])
GO
ALTER TABLE [dbo].[Compras] CHECK CONSTRAINT [FK_Compras_ZonaEnvio]
GO
